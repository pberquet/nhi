const path = require('path');
const { execFile } = require('child_process');

const pathToAMLPackages = path.resolve('..\\..\\..\\Tests\\PackageMethods');
const pathToInnovatorClient = path.resolve('..\\..\\..\\Innovator\\Client');
const pathToJscs = path.resolve('node_modules\\.bin\\jscs.cmd');
const pathToJscsConfig =  path.resolve('..\\..\\..\\.jscsrc');
const pathToJshint = path.resolve('node_modules\\.bin\\jshint.cmd');

const mainProcess = process;
mainProcess.exitCode = 0;

function stdoutProcessCallback(processor) {
	return (error, stdout, stderr) => {
		const invalidFilePaths = [];
		const stdoutLines = stdout.split('\n');
		for (const line of stdoutLines) {
			const filePath = processor.parseFilePath(line);
			if (filePath && invalidFilePaths[invalidFilePaths.length - 1] !== filePath) {
				invalidFilePaths.push(filePath);
			}
		}
		processor.processInvalidFilePaths(invalidFilePaths);
		if (error) {
			mainProcess.exitCode += 1;
		}
	};
}

function createProcessor(validatorName, filePathStartIndex = 0) {
	return {
		parseFilePath: (line) => line.substring(filePathStartIndex, line.indexOf(':')),
		processInvalidFilePaths: (invalidFilePaths) => {
			if (invalidFilePaths.length > 0) {
				console.log(`${validatorName} validation failed for files:`);
				for (const filePath of invalidFilePaths) {
					console.log(`\t${filePath}`);
				}
			} else {
				console.log(`${validatorName} validation successfully passed.`);
			}
		}
	}
}

const stdoutLogCallback = (error, stdout, stderr) => { 
	console.log(stdout);
	if (error) {
		mainProcess.exitCode += 1;
	}
};

function jscsValidate(pathToTargetFodler, options = [], execFileCallback = stdoutLogCallback) {
	return execFile(pathToJscs, ['--config', pathToJscsConfig, '.', ...options], { cwd: pathToTargetFodler, maxBuffer: 10 * 1024 * 1024 }, execFileCallback);
}

function jshintValidate(pathToTargetFodler, options = [], execFileCallback = stdoutLogCallback) {
	return execFile(pathToJshint, ['.', ...options], { cwd: pathToTargetFodler, maxBuffer: 10 * 1024 * 1024 }, execFileCallback);
}

exports.jscs = (options, execFileCallback) => {
	jscsValidate(pathToAMLPackages, options, execFileCallback);
	jscsValidate(pathToInnovatorClient, options, execFileCallback);
};
exports.jshint = (options, execFileCallback) => {
	jshintValidate(pathToAMLPackages, options, execFileCallback);
	jshintValidate(pathToInnovatorClient, options, execFileCallback);
};
exports.lint = () => {
	exports.jscs();
	exports.jshint();
};
exports.lintMinimalOutput = () => {
	exports.jscs(['--reporter', 'inline'], stdoutProcessCallback(createProcessor('JSCS', 2)));
	exports.jshint([], stdoutProcessCallback(createProcessor('JSHint')));
};