$toolsDir = $PSScriptRoot
$pathToNugetExe = Join-Path $toolsDir ".nuget\NuGet.exe"

if (!(Test-Path $pathToNugetExe)) {
	$externalPathToNuget = "C:\Program Files (x86)\NuGet\NuGet.exe"
	if (Test-Path $externalPathToNuget) {
		Copy $externalPathToNuget $pathToNugetExe
	} else {
		if (($env:CRT_PATH_TO_NUGET_EXE -ne $null) -and (Test-Path $env:CRT_PATH_TO_NUGET_EXE)) {
			Copy $env:CRT_PATH_TO_NUGET_EXE $pathToNugetExe
		} else {
			$nugetUrl = "https://dist.nuget.org/win-x86-commandline/latest/nuget.exe"
			Invoke-WebRequest -Method Get -Uri $nugetUrl -OutFile $pathToNugetExe
		}
	}
}

& $pathToNugetExe install "NAnt" -OutputDirectory $toolsDir -ExcludeVersion