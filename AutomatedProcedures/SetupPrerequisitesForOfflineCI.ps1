﻿<#
.SYNOPSIS
The script installes prerequisites for running CI build of CustomRepositoryTemplate or related forks.

.DESCRIPTION
Use-case: You are going to run CustomerRepositoryTemplate\AutomatedProcedures\ContinuousIntegration.bat on a machine without Internet access.
How-to-use: 
    - ensure that you have access to Internet before running the script;
    - [optional] backup your global NuGet.config file (C:\Users\[username]\AppData\Roaming\NuGet\NuGet.config)
    - open powershell console, switch to directory CustomerRepositoryTemplate\AutomatedProcedures, execute .\SetupPrerequisitesForOfflineCI.ps1;
    - when execution of the script is finished, you can run ContinuousIntegration.bat without access to Internet.

.NOTES
Brief description of the script's steps:
    - Creates folder for a number of nupkg files, modifies global NuGet.config file to work ONLY with this folder. Nuget.org package feed is excluded.
      You need to delete this file or restore it from backup if you need access to Nuget.org package feed again.
    - Downloads nupkg files required for ContinuousIntegration.bat to local storage.
    - Downloads Node.JS installer, installs it.
    - Downloads npm packages to folder CustomerRepositoryTemplate\AutomatedProcedures\tools\NodeJS. This operation also puts these packages into cache.
#>
Try {
    Do {
        $pathToCRTgitRepo = Read-Host "Specify local path to git repository CustomRepositoryTemplate (i.e. C:\CustomerRepositoryTemplate)" 
    }
    Until (Test-Path $pathToCRTgitRepo)

    # 1. Check file $env:APPDATA\NuGet\NuGet.config. If it doesn't exist, create it. Set packageSource to local folder
    $nugetConfigPath = Join-Path -Path $env:APPDATA -ChildPath "NuGet\NuGet.config"
    if (Test-Path ($nugetConfigPath)) {
        Write "File Nuget.config already exists."
    }
    else {
        Write "File Nuget.config doesn't exist."
        $nugetFolderPath = Join-Path -Path $env:APPDATA -ChildPath "NuGet"
        Write "Creating directory $nugetFolderPath..."
        New-Item $nugetFolderPath -ItemType Directory
        Write "Creating file NuGet.config..."
        New-Item $nugetConfigPath -ItemType File
    }


    # 2. Set local nuget repository in Nuget.Config
    $localNugetRepoPath = Join-Path -Path $env:USERPROFILE -ChildPath "NuGet"
    Write "Updating $nugetConfigPath..."
    Set-Content $nugetConfigPath "<?xml version=""1.0"" encoding=""utf-8""?>
    <configuration>
	    <packageSources>
		    <add key=""NuGet local source"" value=""$localNugetRepoPath"" />
	    </packageSources>
    </configuration>
    "

    # 3. Creating directory for local nuget repository if it doesn't exist
    if (Test-Path ($localNugetRepoPath)) {
        Write "Directory $localNugetRepoPath already exists, cleaning it..."
        Remove-Item (Join-Path -Path $localNugetRepoPath -ChildPath "*")
    }
    else {
        Write "Creating directory $localNugetRepoPath..."
        New-Item $localNugetRepoPath -ItemType Directory
    }

    # 4. Download Microsoft.experimental.io.1.0.0.nupkg
    Write "Downloading nupkg files..."
    Invoke-WebRequest -Uri "https://www.nuget.org/api/v2/package/Microsoft.Experimental.IO/1.0.0" -OutFile (Join-Path -Path $localNugetRepoPath -ChildPath "microsoft.experimental.io.1.0.0.nupkg")
    Invoke-WebRequest -Uri "https://www.nuget.org/api/v2/package/NUnit/2.6.4" -OutFile (Join-Path -Path $localNugetRepoPath -ChildPath "NUnit.2.6.4.nupkg")
    Invoke-WebRequest -Uri "https://www.nuget.org/api/v2/package/NUnit.Runners/2.6.4" -OutFile (Join-Path -Path $localNugetRepoPath -ChildPath "NUnit.Runners.2.6.4.nupkg")
    Invoke-WebRequest -Uri "https://www.nuget.org/api/v2/package/NSubstitute/1.9.2" -OutFile (Join-Path -Path $localNugetRepoPath -ChildPath "NSubstitute.1.9.2.nupkg")
    Invoke-WebRequest -Uri "https://www.nuget.org/api/v2/package/StyleCop.Analyzers/1.0.0" -OutFile (Join-Path -Path $localNugetRepoPath -ChildPath "StyleCop.Analyzers.1.0.0.nupkg")
    Invoke-WebRequest -Uri "https://www.nuget.org/api/v2/package/System.Data.HashFunction.Core/1.8.2.2" -OutFile (Join-Path -Path $localNugetRepoPath -ChildPath "System.Data.HashFunction.Core.1.8.2.2.nupkg")
    Invoke-WebRequest -Uri "https://www.nuget.org/api/v2/package/System.Data.HashFunction.Interfaces/1.0.0.2" -OutFile (Join-Path -Path $localNugetRepoPath -ChildPath "System.Data.HashFunction.Interfaces.1.0.0.2.nupkg")
    Invoke-WebRequest -Uri "https://www.nuget.org/api/v2/package/System.Data.HashFunction.xxHash/1.8.2.2" -OutFile (Join-Path -Path $localNugetRepoPath -ChildPath "System.Data.HashFunction.xxHash.1.8.2.2.nupkg")

    # 5. Download NPM and install it if not installed
    if ($ENV:PATH -like "*nodejs*") {
        Write "Node.JS is installed"
    }
    else {
        Write "Node.JS is NOT installed, downloading..."
        $pathToNPMmsi = (Join-Path -Path $env:USERPROFILE -ChildPath "node-v6.11.0-x64.msi")
        if (!(Test-Path $pathToNPMmsi)){
            Invoke-WebRequest -Uri "https://nodejs.org/dist/v6.11.0/node-v6.11.0-x64.msi" -OutFile $pathToNPMmsi
        }
        Write "Installing node-v6.11.0-x64.ms..."
        Start-Process msiexec.exe -Wait -ArgumentList "/I $pathToNPMmsi /quiet"
    }

    # 6. Download NPM packages
    Write "Downloading NPM packages..."
    Set-Location (Join-Path -Path $pathToCRTgitRepo -ChildPath "\AutomatedProcedures\tools\NodeJS")
	Start-Process "C:\Program Files\nodejs\npm" -Wait -ArgumentList "cache clear"
    Start-Process "C:\Program Files\nodejs\npm" -Wait -ArgumentList "install jscs"
    Start-Process "C:\Program Files\nodejs\npm" -Wait -ArgumentList "install jshint"
    Start-Process "C:\Program Files\nodejs\npm" -Wait -ArgumentList "install xml2js"

    
}
Catch {
    $errorMessage = $_.Exception.Message
    $failedItem = $_.Exception.ItemName
    Write "Something went wrong.`n Error message is: $errorMessage"
    Break
}

Finally {
    if ($_.Exception.Message) {
        Write-Host "Installation of prerequisites failed!" -ForegroundColor Red
    }
    else {
        Write-Host "Prerequisites for running CI build without internet were successfully installed!" -ForegroundColor Green
    }
}