﻿using DeploymentProcedure.Utility.Base;
using Microsoft.Experimental.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DeploymentProcedure.Utility
{
	internal partial class FileSystemFactory
	{
		private class LocalFileSystem : FileSystem
		{
			public override bool IsLocal { get; }
			public override string ServerName { get; }

			public LocalFileSystem(string serverName)
			{
				if (!IsLocalMachine(serverName))
				{
					throw new ArgumentException(string.Format("Specified server '{0}' is not local", serverName));
				}

				ServerName = serverName;
				IsLocal = true;
			}

			public override void CopyFile(string sourceFilePath, IFileSystem targetFileSystem, string targetFilePath, bool overwrite)
			{
				if (targetFileSystem.IsLocal)
				{
					LongPathFile.Copy(sourceFilePath, targetFilePath, overwrite);
				}
				else
				{
					File.Copy(GetFullPath(sourceFilePath), targetFileSystem.GetFullPath(targetFilePath), overwrite);
				}
			}

			public override void CreateDirectory(string directoryPath)
			{
				LongPathDirectory.Create(directoryPath);
			}

			public override void CreateFile(string filePath)
			{
				using (FileStream fileStream = LongPathFile.Open(filePath, FileMode.Create, FileAccess.Write))
				{
					// due to the absence of LongPathFile.Create method we had to invent bycycle
					// this logic creates file stream, writes nothing there then saves and closes it
				}
			}

			public override void DeleteDirectory(string directoryPath)
			{
				foreach (string subdirectoryPathToDelete in EnumerateDirectories(directoryPath))
				{
					DeleteDirectory(subdirectoryPathToDelete);
				}

				foreach (string filePathToDelete in EnumerateFiles(directoryPath))
				{
					DeleteFile(filePathToDelete);
				}

				LongPathDirectory.Delete(directoryPath);
			}

			public override void DeleteFile(string filePath)
			{
				LongPathFile.Delete(filePath);
			}

			public override bool DirectoryExists(string directoryPath)
			{
				return LongPathDirectory.Exists(directoryPath);
			}

			public override IEnumerable<string> EnumerateDirectories(string path)
			{
				return LongPathDirectory.EnumerateDirectories(path);
			}

			public override IEnumerable<string> EnumerateFiles(string path)
			{
				return LongPathDirectory.EnumerateFiles(path);
			}

			public override bool FileExists(string filePath)
			{
				return LongPathFile.Exists(filePath);
			}

			public override string GetFullPath(string path)
			{
				return Path.GetFullPath(path);
			}

			public override FileStream OpenFile(string filePath)
			{
				return LongPathFile.Open(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
			}

			public override void WriteAllTextToFile(string filePath, string content)
			{
				using (FileStream fileStream = LongPathFile.Open(filePath, FileMode.Create, FileAccess.Write))
				{
					fileStream.Write(Encoding.Default.GetBytes(content), 0, content.Length);
				}
			}
		}
	}
}
