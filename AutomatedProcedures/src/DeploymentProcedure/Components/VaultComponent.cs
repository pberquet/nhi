﻿using DeploymentProcedure.Utility;
using System.IO;
using System.Xml.Serialization;

namespace DeploymentProcedure.Components
{
	[XmlType("vault")]
	public class VaultComponent : WebComponent
	{
		public string Name { get; set; } = "Default";
		public string PathToVaultFolder { get; set; }
		public string VaultServerAspxUrl => Url.TrimEnd('/') + "/VaultServer.aspx";

		#region Overriding CodeTreeComponent properties
		private string _pathToConfig;
		public override string PathToConfig
		{
			get { return _pathToConfig ?? Path.Combine(InstallationPath, "..\\VaultServerConfig.xml"); }
			set { _pathToConfig = value; }
		}
		public override string PathToCodeTreeTemplates => Path.Combine(Settings.Default.PathToTemplates, "VaultServer");
		public override string PathToConfigTemplate => Path.Combine(Settings.Default.PathToTemplates, "VaultServerConfig.xml");
		public override string PathToBasicConfig => Path.Combine(InstallationPath, "VaultServer.xml");
		public override string BaselineSourcePath { get; set; } = Path.Combine(Settings.Default.PathToCodeTree, "VaultServer");
		public override string DeploymentPackageDirectoryName { get; set; } = "VaultServer";
		#endregion

		#region Implementing Setup logic
		public override void Setup()
		{
			base.Setup();

			SetupVaultServerConfig();
			ConfigureInnovatorIISWebApplication(SiteName + VirtualDirectoryPath);
		}

		private void SetupVaultServerConfig()
		{
			Directory.CreateDirectory(PathToVaultFolder);
			TargetFileSystem.XmlHelper.XmlPoke(PathToConfig, "/VaultServer/LocalPath", PathToVaultFolder);
			ProcessWrapper.Execute("icacls", "{0} /grant *S-1-5-11:(OI)(CI)(M)", TargetFileSystem.GetFullPath(PathToVaultFolder));
		}
		#endregion

		#region Implementing Clenaup logic
		public override void Remove()
		{
			base.Remove();

			if (TargetFileSystem.DirectoryExists(PathToVaultFolder))
			{
				TargetFileSystem.DeleteDirectory(PathToVaultFolder);
			}
		}
		#endregion
	}
}
