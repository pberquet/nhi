﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace DeploymentProcedure.Components
{
	[XmlType("agent")]
	public class AgentComponent : WindowsServiceComponent
	{
		public string ConversionManagerUser { get; set; } = "admin";
		public string ConversionManagerPassword { get; set; } = "innovator";
		public string Url { get; set; }
		public string PathToConversionConfig => Path.Combine(InstallationPath, "conversion.config");
		public string PathToConversionConfigTemplate => Path.Combine(Settings.Default.PathToTemplates, "AgentService\\conversion.config");

		#region Overriding CodeTreeComponent properties
		public override string PathToExecutable => Path.Combine(InstallationPath, "Aras.Server.Agent.Service.exe");
		public override string PathToCodeTreeTemplates => Path.Combine(Settings.Default.PathToTemplates, "AgentService");
		public override string PathToConfig => Path.Combine(InstallationPath, "Aras.Server.Agent.Service.exe.config");
		public override string PathToConfigTemplate => Path.Combine(Settings.Default.PathToTemplates, "Aras.Server.Agent.Service.exe.config");
		public override string BaselineSourcePath { get; set; } = Path.Combine(Settings.Default.PathToCodeTree, "AgentService");
		public override string DeploymentPackageDirectoryName { get; set; } = "AgentService";
		#endregion

		#region Implementing Setup logic
		public override void Setup()
		{
			base.Setup();

			SetupAgentServiceConfig();
			SetupConfigFromTemplate(PathToConversionConfig, PathToConversionConfigTemplate);
		}
		private void SetupAgentServiceConfig()
		{
			Url = EvaluateAgentServiceBindingEndpoint(Url);

			TargetFileSystem.XmlHelper.XmlPoke(PathToConfig, "/configuration/system.serviceModel/services/service[@name='Aras.Server.Agent.Service.InternalAgent']/host/baseAddresses/add/@baseAddress", Url);
		}

		private static string EvaluateAgentServiceBindingEndpoint(string url)
		{
			UriBuilder agentServiceUrl = new UriBuilder(url);
			agentServiceUrl.Port = url.StartsWith("https://") ? 8735 : 8734;
			return agentServiceUrl.Uri.ToString();
		}
		#endregion
	}
}
