﻿using DeploymentProcedure.Utility;
using System.IO;
using System.Xml.Serialization;

namespace DeploymentProcedure.Components
{
	[XmlType("oauth")]
	public class OAuthComponent : WebComponent
	{
		public override string BaselineSourcePath { get; set; } = Path.Combine(Settings.Default.PathToCodeTree, "OAuthServer");
		public override string DeploymentPackageDirectoryName { get; set; } = "OAuthServer";
		public string InnovatorUrl { get; set; }
		public override string PathToCodeTreeTemplates => Path.Combine(Settings.Default.PathToTemplates, "OAuthServer");
		public override string PathToConfig => Path.Combine(InstallationPath, "OAuthServer.config");

		public OAuthComponent()
		{
			ManagedRuntimeVersion = string.Empty;
		}

		#region Implementing ISetupable
		public override void Setup()
		{
			base.Setup();

			SetupNtfsPermissionsToFolder(Path.Combine(InstallationPath, "App_Data"));
			SetupWinAuth(SiteName + VirtualDirectoryPath + "/signin-windows");
			ConfigureInnovatorIISWebApplication(SiteName + VirtualDirectoryPath);
			SetupConfigurationFiles();
		}

		private void SetupConfigurationFiles()
		{
			TargetFileSystem.XmlHelper.XmlPoke(PathToConfig, "/configuration/appSettings/add[@key = 'InnovatorServerUrl']/@value", InnovatorUrl);
		}
		#endregion

		#region Implementing IRemovable
		public override void Remove()
		{
			RemoveLocationFromIISApplicationHostConfigByPath(SiteName + VirtualDirectoryPath + "/signin-windows");

			base.Remove();
		}
		#endregion

		#region Overriding ApplyPackage
		public override void HealthCheck()
		{
			ValidationHelper.CheckHostAvailability(ServerName);
		}
		#endregion
	}
}
