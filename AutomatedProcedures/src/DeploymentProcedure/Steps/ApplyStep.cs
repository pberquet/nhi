﻿using DeploymentProcedure.Packages;
using DeploymentProcedure.Steps.Base;
using System.Collections.Generic;
using System.Xml.Serialization;
using DeploymentProcedure.Components.Base;
using DeploymentProcedure.Logging;

namespace DeploymentProcedure.Steps
{
	[XmlType("apply")]
	public class ApplyStep : BaseStep
	{
		[XmlElement("package", typeof(Package))]
		public List<Package> Packages { get; set; }

		public override void Execute(IReadOnlyCollection<Component> instanceComponents)
		{
			Logger.Instance.Log(LogLevel.Info, "Running the health check of all components...\n");

			foreach (Component component in instanceComponents)
			{
				component.HealthCheck();
			}

			foreach (Package package in Packages)
			{
				package.Import(instanceComponents);
			}
		}
	}
}
