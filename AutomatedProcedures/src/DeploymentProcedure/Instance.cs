﻿using DeploymentProcedure.Components.Base;
using DeploymentProcedure.Logging;
using DeploymentProcedure.Serialization;
using DeploymentProcedure.Steps.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace DeploymentProcedure
{
	public class Instance
	{
		private List<Component> _sortedByDependencyComponents;
		
		public List<Component> Components { get; set; }

		[XmlIgnore]
		public IReadOnlyCollection<Component> ComponentsInDependencyOrder
		{
			get
			{
				if (_sortedByDependencyComponents == null)
				{
					_sortedByDependencyComponents = new List<Component>();

					HashSet<string> visited = new HashSet<string>();
					foreach (Component component in Components)
					{
						TopologicalSort(component, visited, _sortedByDependencyComponents);
					}
				}

				return _sortedByDependencyComponents;
			}
		}

		public List<BaseStep> Steps { get; set; }

		#region Implementing Deploy logic
		public void Deploy()
		{
			foreach (BaseStep step in Steps)
			{
				step.Execute(ComponentsInDependencyOrder);
			}
		}
		#endregion

		#region Implementing Cleanup logic
		public void Remove()
		{
			foreach (Component component in ComponentsInDependencyOrder)
			{
				try
				{
					component.Remove();
				}
				catch
				{
					Logger.Instance.Log(LogLevel.Warning, "Cannot remove {0} component because it doesn't respond to health check.", component.Id);
				}
			}
		}
		#endregion

		private void TopologicalSort(Component component, HashSet<string> visited, List<Component> sortedByDependencyComponents)
		{
			if (visited.Add(component.Id))
			{
				string[] dependencyNames = component.DependsOn.Split(new char[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries);
				foreach (string dependencyName in dependencyNames)
				{
					TopologicalSort(Components.Single(c => c.Id == dependencyName), visited, sortedByDependencyComponents);
				}

				sortedByDependencyComponents.Add(component);
			}
		}

		public static Instance FromXml(string pathToConfig)
		{
			using (XmlReader xmlReader = XmlReader.Create(pathToConfig))
			{
				InstanceXmlSerializer serializer = new InstanceXmlSerializer(typeof(Instance));
				return serializer.Deserialize(xmlReader) as Instance;
			}
		}
	}
}
