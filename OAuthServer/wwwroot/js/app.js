﻿const backgroundImageElement = document.getElementById('backgroundImage');
document.body.style.backgroundImage = 'url(' + backgroundImageElement.src + ')';
const logoElement = document.getElementById('logo');
const logoContainerElement = logoElement.parentElement;
logoContainerElement.style.backgroundImage = 'url(' + logoElement.src + ')';

let links = document.querySelectorAll('link[rel="preload"]');
[].forEach.call(links, function(link) {
	link.onload = function(event) {
		event.target.rel = 'stylesheet';
		event.target.onload = null;
	};
});

// Fixup favicon loading for IE11 (I-002409 "R: Favicon is absent on login page on browser tab")
// It does not work because of implementation sandbox X-Content-Security-Policy in IE11.
// At the same time IE11 does not support another X-Content-Security-Policy.
function fixupFaviconForIe() {
	const iconLinkElement = document.querySelector('link[rel="icon"]');
	iconLinkElement.href = iconLinkElement.href;
}
fixupFaviconForIe();
if (window.loginPage) {
	window.loginPage.initialize();
}
