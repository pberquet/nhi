﻿const loginPage = {
	initialize: function() {
		this._initPageSettings();
		this._initAuthenticationTypeField();
		this._initLocalAuthenticationFields();
		this._setInitialFocus();
	},

	_initPageSettings: function() {
		const self = this;
		if (document.msCapsLockWarningOff === false) {
			document.msCapsLockWarningOff = true;
		}

		document.oncontextmenu = function() {
			return false;
		};

		document.onkeypress = function(e) {
			if (!e) {
				e = window.event;
			}

			if (e.keyCode === 13) {
				e.preventDefault();
				if (self._isLocalAuthenticationType()) {
					const username = self._getUsername();
					const password = self._getPassword();
					const element = e.srcElement || e.target;

					if (element.id === 'Username' && username && !password) {
						const passwordElement = this._getPasswordElement();
						passwordElement.focus();
					}

					if (element.id === 'Password' && !username && password) {
						const usernameElement = this._getUsernameElement();
						usernameElement.focus();
					}

					if (password !== '' && username !== '') {
						const loginButtonElement = document.getElementById('Login');
						loginButtonElement.click();
					}
				} else {
					const continueButtonElement = document.getElementById('Continue');
					continueButtonElement.click();
				}
			}
		};
	},

	_initAuthenticationTypeField: function() {
		const self = this;
		const authenticationTypesElement = this._getAuthenticationTypeElement();
		self._handleAuthenticationTypeChange();
		authenticationTypesElement.addEventListener('change', function() {
			self._handleAuthenticationTypeChange();
		});
	},

	_handleAuthenticationTypeChange: function() {
		const self = this;
		const externalAuthenticationElement = document.getElementById('ExternalAuthentication');
		const localAuthenticationElement = document.getElementById('LocalAuthentication');
		const passwordInput = localAuthenticationElement.querySelector('input[type=password]');
		const loginInput = localAuthenticationElement.querySelector('input[name=Username]');
		if (self._isLocalAuthenticationType()) {
			passwordInput.setAttribute('required', '');
			loginInput.setAttribute('required', '');

			externalAuthenticationElement.setAttribute('hidden', '');
			localAuthenticationElement.removeAttribute('hidden');
		} else {
			passwordInput.removeAttribute('required');
			loginInput.removeAttribute('required');

			localAuthenticationElement.setAttribute('hidden', '');
			externalAuthenticationElement.removeAttribute('hidden');
		}
	},

	_initLocalAuthenticationFields: function() {
		this._initUsernameElement();
		this._initPasswordElement();
		this._setHandlersForInvalidEvent();

		const self = this;
		const loginFormElement = document.getElementById('LoginForm');
		loginFormElement.addEventListener('submit', function(event) {
			if (!self._validateAuthenticationCredentials()) {
				event.preventDefault();
			}

			self._setEncryptedPassword();
		});
	},

	_setHandlersForInvalidEvent: function () {
		const usernameElement = this._getUsernameElement();
		const passwordElement = this._getPasswordElement();
		usernameElement.addEventListener('invalid', loginPage._handlerForInvalidEvent);
		passwordElement.addEventListener('invalid', loginPage._handlerForInvalidEvent);
	},

	_handlerForInvalidEvent: function (event) {
		if (loginPage._isLocalAuthenticationType()) {
			const element = event.target;
			element.classList.add('wrong');
			element.focus();
		}
	},

	_getUsernameElement: function() {
		return document.getElementById('Username');
	},

	_getPasswordElement: function() {
		return document.getElementById('Password');
	},

	_getAuthenticationTypeElement: function() {
		return document.getElementById('AuthenticationType');
	},

	_getDatabaseElement: function () {
		return document.getElementById('Database');
	},

	_setEncryptedPassword: function() {
		const passwordElement = this._getPasswordElement();
		const encryptedPasswordElement = document.getElementById('EncryptedPassword');
		const publicKeyElement = document.getElementById('PublicKey');

		const crypt = new JSEncrypt();
		crypt.setPublicKey(publicKeyElement.textContent);

		// JSEncrypt contains an instability in rsa.js module
		// https://github.com/travist/jsencrypt/issues/100
		// The 'encrypt' function should return 'false' if incorrect RSA key was provided
		// or return the valid base64 string.
		// The workaround is to re-run the encryption if result is different from described above.
		let encryptedPassword;
		let attemptsCount = 0;
		do {
			attemptsCount++;
			encryptedPassword = crypt.encrypt(passwordElement.value);
		}
		while (
			encryptedPassword !== 'false'&&
			!this._isValidBase64String(encryptedPassword) &&
			attemptsCount <= 100)

		encryptedPasswordElement.value = encryptedPassword;
	},

	_isValidBase64String: function(inputStr) {
		function stringEndsWith(inputStr, search) {
			if (inputStr.endsWith) {
				return inputStr.endsWith(search);
			} else {
				return inputStr.indexOf(search, inputStr.length - search.length) > -1;
			}
		}

		return inputStr.length === 344 && stringEndsWith(inputStr, '==');
	},

	_initUsernameElement: function() {
		const usernameElement = this._getUsernameElement();
		usernameElement.onkeydown = function() {
			this.classList.remove('wrong');
		};
	},

	_initPasswordElement: function() {
		const self = this;
		const passwordElement = this._getPasswordElement();

		passwordElement.addEventListener('keypress', function(e) {
			const ctrlKey = e.ctrlKey;
			const shiftKey = e.shiftKey;
			const keyCode = e.keyCode || e.which;

			if (!ctrlKey && (((keyCode >= 65 && keyCode <= 90) && !shiftKey) || ((keyCode >= 97 && keyCode <= 122) && shiftKey))) {
				self._showTooltip(true);
			} else {
				if ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122)) {
					self._showTooltip(false);
				}
			}
		});

		passwordElement.addEventListener('keydown', function(e) {
			const keyCode = e.keyCode || e.which;
			if (20 === keyCode) {
				self._showTooltip(false);
			}
			passwordElement.classList.remove('wrong');
		});

		passwordElement.addEventListener('blur', function() {
			self._showTooltip(false);
		});
	},

	_showTooltip: function(show) {
		const tooltipElement = this._getPasswordElement().parentNode;

		if (tooltipElement) {
			tooltipElement.setAttribute('data-tooltip-show', show);
		}
	},

	_setInitialFocus: function() {
		if (this._isLocalAuthenticationType()) {
			const usernameElement = this._getUsernameElement();
			const passwordElement = this._getPasswordElement();

			if (this._getUsername() === '') {
				usernameElement.focus();
			} else {
				passwordElement.focus();
			}
		} else {
			const continueButtonElement = document.getElementById('Continue');
			continueButtonElement.focus();
		}
	},

	_isLocalAuthenticationType: function() {
		return this._getSelectedAuthenticationType() === 'local';
	},

	_getSelectedDatabase: function() {
		const databaseElement = this._getDatabaseElement();
		const selectedDatabaseElement = databaseElement.options[databaseElement.selectedIndex];
		if (selectedDatabaseElement) {
			return selectedDatabaseElement.value;
		}
		return '';
	},

	_getSelectedAuthenticationType: function() {
		const authenticationTypesElement = this._getAuthenticationTypeElement();
		const selectedAuthenticationTypeElement = authenticationTypesElement.options[authenticationTypesElement.selectedIndex];
		if (selectedAuthenticationTypeElement) {
			return selectedAuthenticationTypeElement.value;
		}
		return '';
	},

	_getUsername: function() {
		return this._getUsernameElement().value.trim();
	},

	_getPassword: function() {
		const passwordElement = this._getPasswordElement();
		return passwordElement.value;
	},

	_validateAuthenticationCredentials: function() {
		const database = this._getSelectedDatabase();
		const autenticationType = this._getSelectedAuthenticationType();
		const username = this._getUsername();
		const password = this._getPassword();
		let stat = 'ok';
		const errorElement = document.querySelector('.login-area__error');
		errorElement.classList.remove('login-area__error_visible');
		let errorType;

		if (database === '') {
			stat = this._getLocalizedMessage('login.missing_db_wrn');
			errorType = 'Database';
		} else if (autenticationType === '') {
			stat = this._getLocalizedMessage('login.missing_authentication_type_wrn');
			errorType = 'AuthenticationType';
		} else if (this._isLocalAuthenticationType() && username === '') {
			stat = this._getLocalizedMessage('login.missing_login_name_wrn');
			errorType = 'Username';
		} else if (this._isLocalAuthenticationType() && password === '') {
			stat = this._getLocalizedMessage('login.missing_pwd_wrn');
			errorType = 'Password';
		}

		if (stat === 'ok') {
			return true;
		} else {
			this._showError(stat, errorType);
			return false;
		}
	},

	_showError: function(text, type) {
		const errorElement = document.querySelector('.login-area__error');
		errorElement.classList.add('login-area__error_visible');
		errorElement.firstElementChild.textContent = text;
		if (type) {
			const element = document.getElementById(type);
			if (element) {
				element.classList.add('wrong');
				element.focus();
			}
		}
	},

	_getLocalizedMessage: function(messageId) {
		const messageElement = document.getElementById(messageId);
		if (messageElement) {
			return messageElement.innerText;
		}
		return messageId;
	}
};

window.loginPage = loginPage;
