﻿using Aras.IOM;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpMethods.UnitTests
{
	internal static class ItemHelper
	{
		internal static Item CreateItem(string type, string action)
		{
			IServerConnection connection = Substitute.For<IServerConnection>();
			return Item.CreateItem(connection, type, action, "full");
		}

		internal static Innovator CreateInnovator()
		{
			IServerConnection connection = Substitute.For<IServerConnection>();
			return new Innovator(connection);
		}

		internal static Innovator CreateInnovator(IServerConnection connection)
		{
			return new Innovator(connection);
		}
	}
}