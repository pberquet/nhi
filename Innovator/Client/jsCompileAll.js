﻿/* eslint-env node */
const rollup = require('rollup');
const rollupConfig = require('./nodejs/rollup.config');
const jsCompileHelper = require('./nodejs/jsCompileHelper');

const usingXxHashLikeFileHash = true;

const regularPlugins = rollupConfig.plugins.concat([{
	generateBundle: jsCompileHelper.getGenerateHookHandler(usingXxHashLikeFileHash)
}]);

const uncompressedPipelinePlugins = regularPlugins.filter((plugin) => plugin.name !== 'uglify');

const bundleList = jsCompileHelper.getBundleList();

const uncompressedBundleList = bundleList.map((bundleConfig) => {
	const uncompressedBundleConfig = {
		name: bundleConfig.name + '.uncompressed',
		file: bundleConfig.file.replace('.js', '.uncompressed.js'),
		plugins: uncompressedPipelinePlugins
	};

	return Object.assign({}, bundleConfig, uncompressedBundleConfig);
});

const listOfAllBundles = [...bundleList, ...uncompressedBundleList];

listOfAllBundles.reduce(async (compileStack, rollupJSModule) => {
	await compileStack;
	const bundleResult = await rollup.rollup({
		input: rollupJSModule.input,
		external: rollupConfig.external,
		plugins: rollupJSModule.plugins || regularPlugins,
	});
	return await bundleResult.write({
		file: rollupJSModule.file,
		format: rollupConfig.output[0].format,
		sourcemap: rollupJSModule.sourcemap || rollupConfig.output[0].sourcemap,
		name: rollupJSModule.name,
		globals: rollupConfig.output[0].globals,
	});
}, Promise.resolve());
