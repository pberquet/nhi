﻿/*
Used by MainGridFactory.js
*/

function ItemGrid() {
	ItemGrid.superclass.constructor();
}

inherit(ItemGrid, BaseItemTypeGrid);

ItemGrid.prototype.setMenuState = function ItemGrid_setMenuState(rowId, col) {
	if (currQryItem) {
		var queryItem = currQryItem.getResult(),
			itemNd = queryItem.selectSingleNode('Item[@id="' + rowId + '"]') || aras.getFromCache(rowId),
			brokenFlg = !Boolean(itemNd),
			itemIDs, itemIdsCount;

		var keys = Object.keys(popupMenuState);
		if (keys.length === 0) {
			popupMenuState['com.aras.innovator.cui_default.pmig_Save As'] = false;
			popupMenuState['com.aras.innovator.cui_default.pmig_View'] = false; //4
			popupMenuState['com.aras.innovator.cui_default.pmig_separator1'] = false;
			popupMenuState['com.aras.innovator.cui_default.pmig_Purge'] = false; //7
			popupMenuState['com.aras.innovator.cui_default.pmig_Delete'] = false; //8
			popupMenuState['com.aras.innovator.cui_default.pmig_separator2'] = false;
			popupMenuState['com.aras.innovator.cui_default.pmig_Lock'] = false; //10
			popupMenuState['com.aras.innovator.cui_default.pmig_Unlock'] = false; //11
			popupMenuState['com.aras.innovator.cui_default.pmig_Version'] = false;
			popupMenuState['com.aras.innovator.cui_default.pmig_Revisions'] = false;
			popupMenuState['com.aras.innovator.cui_default.pmig_Promote'] = false;
			popupMenuState['com.aras.innovator.cui_default.pmig_Where Used'] = false;
			popupMenuState['com.aras.innovator.cui_default.pmig_Structure Browser'] = false;
			popupMenuState['com.aras.innovator.cui_default.pmig_Properties'] = false;
			popupMenuState['com.aras.innovator.cui_default.pmig_Add to Desktop'] = false;
			popupMenuStateChanged = true;
		}

		if (!brokenFlg && currItemType) {
			itemIDs = grid.getSelectedItemIds();
			itemIdsCount = itemIDs.length;

			var itemID = rowId;
			var locked_by = aras.getItemProperty(itemNd, 'locked_by_id');
			var isTemp = aras.isTempEx(itemNd);
			var isDirty = aras.isDirtyEx(itemNd);

			var discoverOnlyFlg = (itemNd && itemNd.getAttribute('discover_only') == '1');
			var editFlg = ((isTemp || locked_by == userID) && !discoverOnlyFlg);
			var saveFlg = (editFlg && (aras.getFromCache(itemID) != null));
			var viewFlg = (itemIdsCount == 1 && !discoverOnlyFlg);
			var purgeFlg = (isTemp || (locked_by == ''));
			var lockFlg = aras.uiItemCanBeLockedByUser(itemNd, isRelationshipIT, use_src_accessIT);
			var unlockFlg = (locked_by == userID || (!isTemp && locked_by != '' && aras.isAdminUser()));
			var copyFlg = (itemIdsCount == 1 && !isTemp && can_addFlg);
			var singlePromoteFlg = (locked_by == '' && !isTemp) && (itemIdsCount == 1);
			var massPromoteFlg = itemIdsCount > 1 && !aras.isPolymorphic(currItemType);
			var add2desktopFlg = !isTemp;

			var copy2clipboardFlg = aras.getItemProperty(currItemType, 'is_relationship') == '1' && aras.getItemProperty(currItemType, 'is_dependent') != '1' && (!isFunctionDisabled(itemTypeName, 'Copy'));
			var pasteFlg = !aras.clipboard.isEmpty() && (isTemp || (locked_by == userID)) && (!isFunctionDisabled(itemTypeName, 'Paste'));
			var pasteSpecialFlg = !aras.clipboard.isEmpty() && (isTemp || (locked_by == userID)) && (!isFunctionDisabled(itemTypeName, 'Paste Special'));
			var showClipboardFlg = !aras.clipboard.isEmpty();
			var addItem2PackageFlg = ((itemID == '' || isTemp) ? false : true);

			if (itemIdsCount > 1) {
				var idsArray = [],
					itemNds, tmpItemNd,
					i;

				for (i = 0; i < itemIdsCount; i++) {
					idsArray.push('@id=\'' + itemIDs[i] + '\'');
				}

				itemNds = queryItem.selectNodes('Item[' + idsArray.join(' or ') + ']');
				for (i = 0; i < itemNds.length; i++) {
					tmpItemNd = itemNds[i];
					itemID = aras.getItemProperty(tmpItemNd, 'id');

					if (!tmpItemNd) {
						brokenFlg = true;
						break;
					}

					locked_by = aras.getItemProperty(tmpItemNd, 'locked_by_id');
					isTemp = aras.isTempEx(tmpItemNd);
					isDirty = aras.isDirtyEx(tmpItemNd);

					editFlg = editFlg && (isTemp || (locked_by == userID));
					saveFlg = saveFlg && (editFlg && aras.getFromCache(itemID));
					purgeFlg = purgeFlg && (isTemp || (locked_by == ''));
					lockFlg = lockFlg && aras.uiItemCanBeLockedByUser(tmpItemNd, isRelationshipIT, use_src_accessIT);

					unlockFlg = unlockFlg && (locked_by == userID || (!isTemp && locked_by != '' && aras.isAdminUser()));
					add2desktopFlg = add2desktopFlg & !isTemp;
					pasteFlg = pasteFlg && !aras.clipboard.isEmpty() && (isTemp || (locked_by == userID));
					pasteSpecialFlg = pasteSpecialFlg && !aras.clipboard.isEmpty() && (isTemp || (locked_by == userID));
					addItem2PackageFlg = ((itemID == '' || isTemp) ? false : true);
				}

				if (pasteFlg) {
					pasteFlg = aras.isLCNCompatibleWithIT(itemTypeID);
				}
			}
		}

		if (!brokenFlg) {
			var newPopupMenuState = [];
			newPopupMenuState['com.aras.innovator.cui_default.pmig_Save As'] = (!(isFunctionDisabled(itemTypeName, 'Save As')) && copyFlg);
			newPopupMenuState['com.aras.innovator.cui_default.pmig_View'] = viewFlg && !isFunctionDisabled(itemTypeName, 'View');
			newPopupMenuState['com.aras.innovator.cui_default.pmig_Purge'] = (purgeFlg && isVersionableIT);
			newPopupMenuState['com.aras.innovator.cui_default.pmig_Delete'] = purgeFlg && !isFunctionDisabled(itemTypeName, 'Delete'); //delete is always available, but purge is only for versioanble
			newPopupMenuState['com.aras.innovator.cui_default.pmig_Lock'] = lockFlg && !isFunctionDisabled(itemTypeName, 'Lock');
			newPopupMenuState['com.aras.innovator.cui_default.pmig_Unlock'] = unlockFlg && !isFunctionDisabled(itemTypeName, 'Unlock');
			newPopupMenuState['com.aras.innovator.cui_default.pmig_Version'] = (!(isFunctionDisabled(itemTypeName, 'Version')) && isManualyVersionableIT && itemIDs && itemIdsCount == 1 && !isTemp && (locked_by == userID || locked_by == ''));
			newPopupMenuState['com.aras.innovator.cui_default.pmig_Revisions'] = (isVersionableIT && !isTemp && itemIDs && itemIdsCount == 1);
			newPopupMenuState['com.aras.innovator.cui_default.pmig_Promote'] = ((singlePromoteFlg || massPromoteFlg) && !(isFunctionDisabled(itemTypeName, 'Promote')));
			newPopupMenuState['com.aras.innovator.cui_default.pmig_Where Used'] = (itemIDs && itemIdsCount == 1);
			newPopupMenuState['com.aras.innovator.cui_default.pmig_Structure Browser'] = (itemIDs && itemIdsCount == 1);
			newPopupMenuState['com.aras.innovator.cui_default.pmig_Properties'] = (itemIDs && itemIdsCount == 1);
			newPopupMenuState['com.aras.innovator.cui_default.pmig_Add to Desktop'] = add2desktopFlg;
			newPopupMenuState['com.aras.innovator.cui_default.pmig_separator1'] = true;
			newPopupMenuState['com.aras.innovator.cui_default.pmig_separator2'] = true;

			const keys = Object.keys(newPopupMenuState);
			keys.forEach(function(key) {
				const value = newPopupMenuState[key];
				if (popupMenuState[key] !== value) {
					popupMenuStateChanged = true;
					popupMenuState[key] = value;
				}
			});
		}
	}
};

ItemGrid.prototype.onLockCommand = function ItemGrid_onLockCommand(ignorePolymophicWarning, itemIDs) {
	if (!ignorePolymophicWarning && aras.isPolymorphic(currItemType)) {
		aras.AlertError(aras.getResource('', 'itemsgrid.poly_item_cannot_be_locked_from_location'));
		return false;
	}
	return ItemGrid.superclass.onLockCommand();
};

ItemGrid.prototype.onUnlockCommand = function ItemGrid_onUnlockCommand(ignorePolymophicWarning, itemIDs) {
	if (!ignorePolymophicWarning && aras.isPolymorphic(currItemType)) {
		aras.AlertError(aras.getResource('', 'itemsgrid.poly_item_cannot_be_unlocked_from_location'));
		return false;
	}
	return ItemGrid.superclass.onUnlockCommand();
};

ItemGrid.prototype.onEditCommand = function ItemGrid_onEditCommand(itemId) {
	if (aras.isPolymorphic(currItemType)) {
		aras.AlertError(aras.getResource('', 'itemsgrid.polyitem_cannot_be_edited_from_the_location'));
		return false;
	}

	if (!itemId) {
		aras.AlertError(aras.getResource('', 'itemsgrid.select_item_type_first', itemTypeLabel));
		return false;
	}

	if (execInTearOffWin(itemId, 'edit')) {
		return true;
	}

	if (itemTypeName === 'SelfServiceReport') {
		var existingWnd = aras.uiFindAndSetFocusWindowEx(aras.SsrEditorWindowId);
		if (existingWnd) {
			return existingWnd.showMultipleReportsError(itemId);
		}
	}

	var itemNode = aras.getItemById(itemTypeName, itemId, 0, undefined, '*'),
		notLocked;

	if (!itemNode) {
		if (itemTypeName == 'Form') {
			itemNode = aras.getItemFromServer(itemTypeName, itemId, 'locked_by_id').node;
		}

		if (!itemNode) {
			aras.AlertError(aras.getResource('', 'itemsgrid.failed2get_itemtype', itemTypeLabel));
			return false;
		}
	}

	notLocked = (!aras.isTempEx(itemNode) && aras.getItemProperty(itemNode, 'locked_by_id') == '');
	if (notLocked) {
		if (!aras.lockItemEx(itemNode)) {
			return false;
		}

		itemNode = aras.getItemById(itemTypeName, itemId, 0);
		if (updateRowSearchGrids(itemNode) !== false) {
			onSelectItem(itemId);
		}
	}

	aras.uiShowItemEx(itemNode, aras.getPreferenceItemProperty('Core_GlobalLayout', null, 'core_view_mode'));
};

ItemGrid.prototype.onDoubleClick = function ItemGrid_onDoubleClick(itemId, altMode) {
	if (popupMenuState['com.aras.innovator.cui_default.pmig_View'] || !isFunctionDisabled(itemTypeName, 'DoubleClick')) {
		aras.uiShowItem(itemTypeName, itemId, null, altMode);
	}
};

ItemGrid.prototype.onClick = function ItemGrid_onClick(itemId) {
	previewPane.showFormByItemId(itemId, itemTypeName);
};

ItemGrid.prototype.onMenuClicked = function ItemGrid_onMenuClicked(commandId, rowId, col) {
	switch (commandId) {
		case 'locked_criteria:clear':
			grid.setCellValue('input_row', 0, '<img src=\'\'>');
			break;
		case 'locked_criteria:by_me':
			grid.setCellValue('input_row', 0, '<img src=\'../images/ClaimOn.svg\'>');
			break;
		case 'locked_criteria:by_others':
			grid.setCellValue('input_row', 0, '<img src=\'../images/ClaimOther.svg\'>');
			break;
		case 'locked_criteria:by_anyone':
			grid.setCellValue('input_row', 0, '<img src=\'../images/ClaimAnyone.svg\'>');
			break;
		default:
			break;
	}

	return commandId;
};

ItemGrid.prototype.onMassPromote = function ItemGrid_onMassPromote(itemIds) {
	if (typeof itemIds === 'undefined' || itemIds === null) {
		return;
	}

	var newItemNd = aras.newItem('mpo_MassPromotion');
	aras.itemsCache.addItem(newItemNd);

	aras.setItemProperty(newItemNd, 'promote_type', window.itemTypeName);
	var relationships = newItemNd.ownerDocument.createElement('Relationships');
	var queryItems = currQryItem.getResult();

	for (var i = 0; i < itemIds.length; i++)
	{
		var selectedItem = aras.getFromCache(itemIds[i]);
		if (!selectedItem) {
			var itemId = itemIds[i];
			var query = 'Item[@id="' + itemId + '"]';
			selectedItem = queryItems.selectSingleNode(query);
		}
		if (selectedItem) {
			var cloned = selectedItem.cloneNode(true);
			relationships.appendChild(cloned);
		}
	}
	newItemNd.appendChild(relationships);

	aras.uiShowItemEx(newItemNd, 'new');
};

