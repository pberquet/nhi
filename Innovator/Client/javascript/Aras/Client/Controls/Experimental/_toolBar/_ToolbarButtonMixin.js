﻿/*jslint sloppy: true, nomen: true*/
/*global dojo, define, document, window*/
define(['dojo/_base/declare', 'dojo/dom-construct'],
	function(declare, Dom) {

		return declare('Aras.Client.Controls.Experimental._toolBar._ToolbarButtonMixin', null, {
			constructor: function(args) {
				this._imageSrc = args.imageSrc;
				this._imageClass = args.imageClass;
				this._imageStyle = args.imageStyle;
			},

			postCreate: function(args) {
				this.inherited(arguments);
				var imageNode;
				this.iconNode.style.display = 'inline-block';
				function isSvg(url) {
					return (url.toLowerCase().substring(url.length - 3, url.length) == 'svg');
				}
				function svgSerialize(url) {
					var response = dojo.xhr.get({ url: url, sync: false });
					return response.promise;
				}

				if (isSvg(this._imageSrc) && dojoConfig.arasContext.browser.isIe &&
					dojoConfig.arasContext.browser.majorVersion >= 10) {
					imageNode = Dom.create('div', {'class': this._imageClass}, this.iconNode);
					var SVGElement = document.createElement('div');
					var imageStyle = this._imageStyle;
					svgSerialize(this._imageSrc).then(function(res) {
						SVGElement.innerHTML = res;
						var svgNode = SVGElement.querySelector('svg');
						svgNode.setAttribute('height', imageStyle.maxHeight);
						svgNode.setAttribute('width', imageStyle.width);
						imageNode.appendChild(svgNode);
						setStyles(imageNode, imageStyle);
					});
				} else {
					imageNode = Dom.create('img', { 'class': this._imageClass, 'src': this._imageSrc }, this.iconNode);
					setStyles(imageNode, this._imageStyle);
				}

				function setStyles(imageNode, imageStyle) {
					// removeAttribute() needed for IE, because it's add unnecessary attributes 'width' and 'height', when image loaded from cache.
					imageNode.removeAttribute('width');
					imageNode.removeAttribute('height');
					if (imageStyle.width != 'auto') {
						imageNode.style.width = imageStyle.width;
					} else {
						imageNode.style.maxWidth = imageStyle.maxWidth;
					}
					if (imageStyle.height != 'auto') {
						imageNode.style.height = imageStyle.height;
					} else {
						imageNode.style.maxHeight = imageStyle.maxHeight;
					}
					if (imageStyle.display == 'none') {
						imageNode.style.display = imageStyle.display;
					}
				}

				this.containerNode.style.verticalAlign = 'middle';
			}
		});
	});
