﻿define(['dojo/_base/declare'],

	function(declare) {

		return declare('Aras.Client.Frames.Relationships', null, {

			relationshipsControl: null,
			getFirstEnabledTabID: null,

			constructor: function(args) {
				this.relationshipsControl = args.relationshipsControl;
				this.getFirstEnabledTabID = this.relationshipsControl.getFirstEnabledTabID;
				this.contentWindow = this;
				this.frameElement = this;
				this.document = window.document;
				window.relationships = this;
				this.contentDijit = this.relationshipsControl.relTabbar;
				this.relTabbar = this.relationshipsControl.relTabbar;

				if (!document.frames) {
					document.frames = [];
				}
				document.frames.relationships = this;

				Object.defineProperty(this, 'domNode', {
					get: function() {
						return this.relationshipsControl.relTabbar.domNode;
					}
				});

				Object.defineProperty(this, 'item', {
					get: function() {
						return this.relationshipsControl.item;
					}
				});

				Object.defineProperty(this, 'relTabbarReady', {
					get: function() {
						return this.relationshipsControl.relTabbarReady;
					}
				});

				Object.defineProperty(this, 'itemTypeName', {
					get: function() {
						return this.relationshipsControl.itemTypeName;
					}
				});

				Object.defineProperty(this, 'isEditMode', {
					get: function() {
						return this.relationshipsControl.isEditMode;
					}
				});

				Object.defineProperty(this, 'contentDijit', {
					get: function() {
						return this.relationshipsControl.relTabbar;
					}
				});

				Object.defineProperty(this, 'iframesCollection', {
					get: function() {
						return this.relationshipsControl.iframesCollection;
					}
				});

				Object.defineProperty(this, 'currTabID', {
					get: function() {
						return this.relationshipsControl.currTabID;
					}
				});
			},

			setViewMode: function(updatedItem) {
				return this.relationshipsControl.setViewMode(updatedItem);
			},

			setEditMode: function(updatedItem) {
				return this.relationshipsControl.setEditMode(updatedItem);
			},

			updateTabbarState: function(callerID) {
				return this.relationshipsControl.updateTabbarState(callerID);
			},

			startup: function() {
				return this.relationshipsControl.startup();
			},

			reload: function(queryStringData) {
				return this.relationshipsControl.reload(queryStringData);
			}
		});
	});
