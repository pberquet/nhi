﻿function TimeZonesInformation() {
	this.cacheOlsonTzName = {};
}

/***
 * @param winTzName Name of windows time zone
 * @returns {*} Olson time zone name by windows time zone
 */
TimeZonesInformation.prototype.getOlsonTimeZoneName = function GetOlsonTimeZoneName(winTzName) {
	if (this.cacheOlsonTzName[winTzName]) {
		return this.cacheOlsonTzName[winTzName];
	}

	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open('GET', aras.getBaseURL() + '/TimeZone/GetOlsonTimeZoneName?windowsTimeZoneName=' + winTzName, false);
	xmlHttp.send();
	if (xmlHttp.status == 404) {
		aras.AlertError('Could not found timeZone: ' + winTzName);
		return;
	}

	this.cacheOlsonTzName[winTzName] = xmlHttp.responseText;
	return this.cacheOlsonTzName[winTzName];
};

/***
 * @param date Date with respect to which is calculated offset
 * @param winTzName Name of windows time zone
 * @returns {*} Time zone offset using olson time zone database
 */
TimeZonesInformation.prototype.getTimeZoneOffset = function TimeZonesInformationGetTimeZoneOffset(date, winTzName) {
	// Get olson time zone name by windows time zone name
	var olsonTzName = this.getOlsonTimeZoneName(winTzName);
	dojo.require('Aras.Client.Controls.Experimental.TimeZoneInfo');
	var tzInfo = Aras.Client.Controls.Experimental.TimeZoneInfo.getTzInfo(date, olsonTzName);
	return tzInfo.tzOffset;
};

/***
 * @returns {*} Timezone name in windows format
 */
TimeZonesInformation.prototype.getTimeZoneLabel = function() {
	var getZoneNameFromDateString = function(str) {
		if (!str || (str && (str.indexOf('(') === -1 || str.indexOf(')') === -1))) {
			return;
		}
		var regexp1 = /.*?\((.*).*/;
		str = regexp1.exec(str)[1];
		if (!str) {
			return;
		}
		var regexp2 = /(.*)\)/;
		str = regexp2.exec(str)[1];

		if (str) {
			return str;
		}
	};
	var timeZoneName;
	if (aras.Browser.isIe() && aras.Browser.OSName === 'Windows') {
		var dateString = (new Date()).toString();
		timeZoneName = getZoneNameFromDateString(dateString);
	}

	if (!timeZoneName) {
		timeZoneName = jstz.determine().name();
	}

	return timeZoneName || '';
};

TimeZonesInformation.prototype.getWindowsTimeZoneNames = function(tzLabel) {
	var inputType = tzLabel.indexOf('/') > 0 ? 'iana' : 'windows';
	var localTime = (new Date()).toISOString();
	var localTimeOffset = (-1 * (new Date()).getTimezoneOffset()).toString();

	var url = aras.getBaseURL() + '/TimeZone/GetTimezoneNames?tzlabel=' +
		encodeURIComponent(tzLabel) + '&inputType=' + encodeURIComponent(inputType) +
		'&localTime=' + encodeURIComponent(localTime) +
		'&offsetBetweenLocalTimeAndUTCTime=' + encodeURIComponent(localTimeOffset);
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open('GET', url, false);
	xmlHttp.send();
	if (xmlHttp.status !== 200) {
		aras.AlertError('Could not found timeZone: ' + tzLabel);
		return [];
	}

	return JSON.parse(xmlHttp.responseText).map(function(tz) {return tz.id;});
};

TimeZonesInformation.prototype.setTimeZoneNameInLocalStorage = function(tzLabel, tzName) {
	var tzOffset = -1 * (new Date()).getTimezoneOffset();
	var timeZones = {};
	timeZones[tzOffset + tzLabel] = tzName;
	localStorage.setItem('timeZone', JSON.stringify(timeZones));
};

TimeZonesInformation.prototype.getTimeZoneNameFromLocalStorage = function(tzLabel) {
	var tzOffset = -1 * (new Date()).getTimezoneOffset();
	var timeZones = JSON.parse(localStorage.getItem('timeZone'));
	if (timeZones && timeZones.hasOwnProperty(tzOffset + tzLabel)) {
		return timeZones[tzOffset + tzLabel];
	}
};
