﻿// © Copyright by Aras Corporation, 2004-2012.

function updateProvidedItemsGrid(itemsGrid, updatedItem) {
	if (!updatedItem || !window.isTearOff) {
		return false;
	}
	var updatedID = updatedItem.getAttribute('id');

	if (itemsGrid.isItemsGrid) {
		if (itemsGrid.itemTypeName == itemTypeName) {
			var grid = itemsGrid.grid;
			if (grid.getRowIndex(itemID) > -1) {
				var wasSelected = (grid.getSelectedItemIds().indexOf(itemID) > -1);

				if (updatedID != itemID) {
					itemsGrid.deleteRow(item);
				}
				itemsGrid.updateRow(updatedItem);

				if (wasSelected) {
					if (updatedID == itemID) {
						itemsGrid.onSelectItem(itemID);
					} else {
						var currSel = grid.getSelectedId();
						//if (currSel)
						itemsGrid.onSelectItem(currSel);
					}
				} //if (wasSelected)
			}
		}
	} //if (itemsGrid.isItemsGrid)
}

function deleteProvidedItemsGrid(itemsGrid) {
	if (itemsGrid.isItemsGrid && itemsGrid.itemTypeName === itemTypeName) {
		const grid = itemsGrid.grid;
		if (grid.getRowIndex(itemID) > -1) {
			grid.deleteRow(itemID);
		}
	}
}

window.aras = window.aras || window.parent.aras;
var updateItemsGrid = window.aras.decorateForMultipleGrids(updateProvidedItemsGrid);
var deleteRowFunc = window.aras.decorateForMultipleGrids(deleteProvidedItemsGrid);

function updateMenuState() {
	var editor = document.getElementById('editor');
	if (window.isTearOff && editor && editor.contentWindow.refreshMenuState) {
		editor.contentWindow.refreshMenuState();
	}
}

function onSaveCommand() {
	var editor = (window.isTearOff ? document.getElementById('editor').contentWindow : window);

	var res;
	editor.currLCNode.setAttribute('levels', 3);
	res = aras.saveItemEx(editor.currLCNode);
	if (res) {
		editor.setEditMode();
		if (window.isTearOff) {
			updateItemsGrid(res);
		}
	}
	window['system_res'] = res;
	return true;
}

function onUnlockCommand(silentMode) {
	var editor = (window.isTearOff ? document.getElementById('editor').contentWindow : window);
	var res = aras.unlockItemEx(editor.currLCNode, silentMode);
	if (res) {
		editor.setViewMode();
		if (window.isTearOff) {
			updateItemsGrid(res);
		}
	}

}

function onUndoCommand() {
	var editor = window.isTearOff ? document.getElementById('editor').contentWindow : window;
	if (!aras.isDirtyEx(editor.currLCNode)) {
		aras.AlertError(aras.getResource('', 'item_window.nothing_to_undo'));
		return true;
	}
	if (!aras.confirm(aras.getResource('', 'common.undo_discard_changes'))) {
		return true;
	}
	aras.removeFromCache(editor.currLCID);
	editor.setEditMode();
	return true;
}

function onLockCommand() {
	var editor = (window.isTearOff ? document.getElementById('editor').contentWindow : window);
	var res = aras.lockItemEx(editor.currLCNode);
	if (res) {
		editor.setEditMode();
		if (window.isTearOff) {
			updateItemsGrid(res);
		}
	}
}

function onEditCommand() {
	if (aras.isTempEx(item)) {
		return true;
	}

	var isLockedByUser = aras.isLockedByUser(item);

	if (!isLockedByUser && this.onLockCommand) {
		return this.onLockCommand();
	}

	return true;
}

function onPurgeCommand() {
	return onPurgeDeleteCommand('purge');
}

function onDeleteCommand(silentMode) {
	return onPurgeDeleteCommand('delete', silentMode);
}

function onPurgeDeleteCommand(command, silentMode) {
	var editor = (window.isTearOff ? document.getElementById('editor').contentWindow : window);
	var res = false;

	if (command == 'purge') {
		res = aras.purgeItem('Life Cycle Map', editor.currLCID, false);
	} else {
		res = aras.deleteItem('Life Cycle Map', editor.currLCID, silentMode);
	}

	if (res) {
		if (window.isTearOff) {
			deleteRowFunc();
			window.close();
		} else {
			var lcIT = aras.getItemFromServerByName('ItemType', 'Life Cycle Map', 'id');
			if (lcIT) {
				parent.work.location.replace('itemsGrid.html?itemtypeID=' + lcIT.getID());
			} else {
				parent.work.location.replace('../scripts/blank.html');
			}
		}
	}

	return {result: res ? 'Deleted' : 'Canceled'};
}

function onPrintCommand() {
	//no any code since pdf printing approach implementation
	return true;
}

function onExport2OfficeCommand(targetAppType) {
	var editor = (window.isTearOff ? document.getElementById('editor').contentWindow : window);
	var itm = editor.currLCNode;
	if (itm) {
		aras.export2Office(function() { return ''; }, targetAppType, itm);
	}
}

function onSaveUnlockAndExitCommand() {
	var editor = (window.isTearOff ? document.getElementById('editor').contentWindow : window);

	var res = null;
	var statusId;
	if (window.isTearOff) {
		statusId = aras.showStatusMessage('status', aras.getResource('', 'common.saving'), '../images/Progress.gif');
	}
	res = aras.saveItemEx(editor.currLCNode, false);
	if (window.isTearOff) {
		if (statusId) {
			aras.clearStatusMessage(statusId);
		}
	}
	if (!res) {
		return true;
	}

	if (window.isTearOff) {
		statusId = aras.showStatusMessage('status', aras.getResource('', 'common.unlocking'), '../images/Progress.gif');
	}
	res = aras.unlockItemEx(res);
	if (window.isTearOff) {
		if (statusId) {
			aras.clearStatusMessage(statusId);
		}
	}
	if (!res) {
		return true;
	}

	if (window.isTearOff) {
		updateItemsGrid(res);
		window.close();
	}

	return true;
}

function onShowAccess() {
}

function onShowWhereUsed() {
	var win = aras.getMostTopWindowWithAras(window);
	var url = 'whereUsed.html?id=' + itemID + '&type_name=' + itemTypeName + '&curLy=' +
		aras.getPreferenceItemProperty('Core_GlobalLayout', null, 'core_structure_layout') + '&toolbar=1&where=dialog';
	win.ArasModules.Dialog.show('iframe', {aras: aras, dialogWidth: 600, dialogHeight: 400, resizable: true, content: url});
}

function onShowWorkflow() {
	var editor = (window.isTearOff ? document.getElementById('editor').contentWindow : window);
	var editmode = editor.isEditMode ? 1 : 0;
	var relTypeID = aras.getItemFromServerByName('RelationshipType', 'Workflow', 'id').getID();
	var params = {
		LocationSearch: '?db=' + aras.getDatabase() + '&ITName=' + itemTypeName + '&itemID=' + itemID + '&relTypeID=' + relTypeID +
			'&editMode=' + editmode + '&tabbar=0&toolbar=1&where=dialog',
		aras: aras,
		item: item,
		dialogWidth: 750,
		dialogHeight: 500,
		resizable: true,
		content: 'relationships.html'
	};

	var win = aras.getMostTopWindowWithAras(window);
	win.ArasModules.Dialog.show('iframe', params);
}

function onShowLifeCycle() {
	var params = {
		aras: aras,
		title: aras.getResource('', 'lifecycledialog.lc', aras.getKeyedNameEx(item)),
		item: item,
		dialogWidth: 600,
		dialogHeight: 400,
		resizable: true,
		type: 'LifeCycleDialog.html'
	};

	var win = aras.getMostTopWindowWithAras(window);
	win.ArasModules.Dialog.show('iframe', params).promise.then(
		function(res) {
			if (typeof (res) == 'string' && res == 'null') {
				deleteRowFromItemsGrid(itemID);
				window.close();
				return;
			}
			if (!res) {
				return;
			}

			if (window.isTearOff) {
				updateItemsGrid(res);
			}

			isEditMode = false;
			aras.uiReShowItemEx(itemID, res, viewMode);
		}
	);
}
var windowReady = true;
