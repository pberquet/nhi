﻿// © Copyright by Aras Corporation, 2004-2007.

//constants
var fontSizeConst = '8';
var popupMenuFontConst = 'normal normal normal ' + fontSizeConst + 'pt normal';
var borderWidthConst = 2;
var leftRightMarginConst = 0;
var topBottomMarginConst = 1;
var labelHeightConst = 13;
var arasScrollBarConst = 15;
var separatorImgPathConst = '../imagesLegacy/100x4_divider.gif';

function setSeparatorImgPath(newPath) {
	separatorImgPathConst = newPath;
}

// ++++++++++   PopupMenu   ++++++++++ //
function PopupMenu(menuID, menuLabel, showLabel) {
	if (menuID === undefined) {
		menuID = 'id_';
	}
	if (menuLabel === undefined) {
		menuLabel = '';
	} else {
		menuLabel = menuLabel.toString();
	}
	if (showLabel === undefined) {
		showLabel = false;
	}

	this.isMenu = true;

	this.id = menuID;
	this.label = menuLabel;
	this.showLabel = showLabel;
	this.onClick = null;
	this.font = popupMenuFontConst;
	this.win = window;

	this.contentArr = [];
	this.contentMap = {};
	this.submenus = {};

	this.isShown = false;
	this.parentMenu = null;
	this.shownSubMenu = null;
	this.clickedID = '';
	this.enabled = true;

	this.oPopup = null;

	this.topVar = 0;
	this.left = 0;
	this.width = 0;
	this.height = 0;
}

PopupMenu.prototype.isOpen = function PopupMenuIsOpen() {
	if (this.oPopup === null) {
		return false;
	}
	return this.oPopup.isOpen;
};

PopupMenu.prototype.setLabel = function PopupMenuSetLabel(menuLabel) {
	if (menuLabel === undefined) {
		return;
	}
	this.label = menuLabel.toString();
};

PopupMenu.prototype.setShowLabel = function PopupMenuSetLabel(showLabel) {
	if (showLabel === undefined) {
		return;
	}
	this.showLabel = showLabel;
};

PopupMenu.prototype.setOnClick = function PopupMenuSetOnClick(onClickHandler) {
	if (!onClickHandler || typeof (onClickHandler) != 'function') {
		return;
	}

	this.onClick = onClickHandler;
};

PopupMenu.prototype.setFont = function PopupMenuSetFont(newFont) {
	if (!newFont) {
		return;
	}
	this.font = newFont;
};

PopupMenu.prototype.setParentWindow = function PopupMenuSetParentWindow(win) {
	if (!win) {
		return;
	}
	this.win = win;
};

PopupMenu.prototype.setEnabled = function PopupMenuSetEnabled(enabled) {
	this.enabled = enabled;
};

PopupMenu.prototype.getId = function PopupMenuGetId() {
	return this.id;
};

PopupMenu.prototype.getLabel = function PopupMenuGetLabel() {
	return this.label;
};

PopupMenu.prototype.getShowLabel = function PopupMenuGetLabel() {
	return this.showLabel;
};

PopupMenu.prototype.getOnClick = function PopupMenuGetOnClick() {
	return this.onClick;
};

PopupMenu.prototype.getFont = function PopupMenuGetFont() {
	return this.font;
};

PopupMenu.prototype.getParentWindow = function PopupMenuGetParentWindow() {
	return this.win;
};

PopupMenu.prototype.isEnabled = function PopupMenuIsEnabled() {
	return this.enabled;
};

PopupMenu.prototype.setClickedID = function PopupMenuSetClickedID(clickedID) {
	if (!clickedID) {
		return;
	}
	this.clickedID = clickedID;
	if (this.contentMap[clickedID] && this.contentMap[clickedID].isCheckable) {
		var prevState = this.contentMap[clickedID].isChecked();
		this.contentMap[clickedID].setChecked(!prevState);
	}
	if (!this.onClick && this.parentMenu) {
		return this.parentMenu.setClickedID(clickedID);
	}

	this.hideAll();

	if (this.onClick) {
		this.onClick(this.getItemById(clickedID));
	}
};

PopupMenu.prototype.getClickedID = function PopupMenuGetClickedID() {
	return this.clickedID;
};

PopupMenu.prototype.getSeparator = function() {
	return '<div style="border-top:solid #808080 1px; border-bottom:solid #ffffff 1px; margin-left:2px; margin-right:2px;"></div>';
};

PopupMenu.prototype.show = function PopupMenuShow(left, topVar) {
	if (!('createPopup' in this.win)) {
		return;
	}
	if (this.contentArr.length === 0) {
		return; //to not show empty menu
	}

	if (left === undefined) {
		left = 0;
	}
	if (topVar === undefined) {
		topVar = 0;
	}

	if (this.isShown) {
		return true;
	}

	this.win.oPopup = this.win.createPopup();
	this.oPopup = this.win.oPopup;
	var oPopup = this.oPopup;

	var i;
	var itm;
	var field;

	var doc = oPopup.document.open();
	var font = this.getFont();

	doc.writeln('<html>\n' +
		'<head><style type="text/css">\n' +
		'body {overflow-y: hidden; overflow-x: hidden; border-style:outset; border-color:grey; ' +
		'	cursor:default; background-color:buttonface;' +
		'	border-width:' + borderWidthConst + '; ' +
		'	margin-top:' + topBottomMarginConst + '; ' +
		'	margin-bottom:' + topBottomMarginConst + '; ' +
		'	margin-left:' + leftRightMarginConst + '; ' +
		'	margin-right:' + leftRightMarginConst + '; ' +
		'	font:' + (font ? font : '') + '; ' +
		'	font-family:Tahoma; ' +
		'	scrollbar-3dlight-color:buttonface;\n' +
		'}\n' +
		'table {font:' + (font ? font : '') + '; font-family:Tahoma; ' +
		'	width:100%; height:' + (fontSizeConst * 2.5) + 'px;' +
		'}\n' +
		'span {float:none; width:100%;}\n' +
		'.st {color:; background-color:;}\n' +
		'.sth {color:highlighttext; background-color:highlight;}\n' +
		'.labelTD {width:100%;}\n' +
		'.csignZone {font-family:Marlett; font-size:x-small; margin-right:0.1em; visibility:hidden; }\n' +//border:solid red 1px;
		'.labelZone {margin-right:2em;}\n' +//border:solid green 1px;
		'.smsignZone {font-family:Marlett; font-size:x-small; margin-right:0.1em;}\n' +//border:solid blue 1px;
		'</style></head>\n');

	doc.creatorOwner = this;

	//+++ define the very first and very last non separator rows +++
	var firstRowId = '';
	var lastRowId = '';
	var cntArr = this.contentArr;
	if (cntArr && cntArr.length > 0) {
		i = 0;
		do {
			firstRowId = (cntArr[i].getId) ? cntArr[i].getId() : '';
			i++;
		} while (!firstRowId && i < cntArr.length);
		i = cntArr.length - 1;
		do {
			lastRowId = (cntArr[i].getId) ? cntArr[i].getId() : '';
			i--;
		} while (!lastRowId && i >= 0);
	}
	//--- define the very first and very last non separator rows ---

	doc.writeln('<script>\n' +

	'function getSpanElement(elem) {\n' +
	' if (elem.tagName=="SPAN") return elem;\n' +
	' if (elem.tagName=="BODY") return null;\n' +
	' if (!elem.parentElement) return null;\n' +
	'	return getSpanElement(elem.parentElement);\n' +
	'}\n' +

	'var highlightedRow = null;\n' +
	'displayArasScrollBar = false;\n' +
	'var firstRowId = "' + firstRowId + '";\n' +
	'var lastRowId = "' + lastRowId + '";\n' +

	'var maxScrollTop = null;\n' +
	'function setHighLighted(row) {\n' +
	'	if (row === highlightedRow) return;\n' +
	'	if (highlightedRow && highlightedRow.id==firstRowId && row.id==lastRowId && ' + this.contentArr.length + '>2) return;\n' +
	'	if (highlightedRow && highlightedRow.id==lastRowId && row.id==firstRowId && ' + this.contentArr.length + '>2) return;\n' +
	'	if (highlightedRow) {\n' +
	'		if (document.creatorOwner.getItemById(highlightedRow.id).isEnabled()) highlightedRow.className = "st";\n' +
	'		else highlightedRow.className = "std";\n' +
	'	}\n' +
	'	if (document.creatorOwner.getItemById(row.id).isEnabled()) row.className = "sth";\n' +
	'	else row.className = "sthd";\n' +
	'	highlightedRow = row;\n' +
	'	if (row.id==firstRowId || row.id==lastRowId) bMousePressedDown=false;\n' +
	'	var docB = document.getElementById("mainDiv"); \n' +
	'	var iOffsetTop = row.offsetTop;\n' +
	'	var iClientHeight = row.clientHeight;\n' +
	'	var st=docB.scrollTop;\n' +
	'	if (iOffsetTop - st<=0) docB.scrollTop = iOffsetTop;\n' +
	'	if (iOffsetTop + iClientHeight - docB.clientHeight - st>5) docB.scrollTop = st+iClientHeight;\n' +
	'	if (row.id==lastRowId) maxScrollTop = docB.scrollTop;\n' +
	'	document.all("UpArrowButton").disabled = (docB.scrollTop === 0 ? true : false);\n' +
	'	document.all("DownArrowButton").disabled = ((maxScrollTop!==null && maxScrollTop<=docB.scrollTop) ? true : false);\n' +
	'}\n' +

	'var bMousePressedDown=false;\n' +
	'function upDownArrowButtonClick(isUp)' +
	'{\n' +
	'	var kCode = (isUp) ? 38 : 40;\n' +
	'	if (bMousePressedDown) onKeyPress({keyCode:kCode});\n' +
	'	if (bMousePressedDown) setTimeout("upDownArrowButtonClick("+isUp+")", 100);\n' +
	'}\n' +

	'function showSubMenu(row) {\n' +
	'	var docB = document.getElementById("mainDiv"); \n' +
	'	var left = document.creatorOwner.width;\n' +
	'	var topVar = row.offsetTop + ' + (borderWidthConst + topBottomMarginConst) + '+' + (this.showLabel ? labelHeightConst : '0') +
	'+(document.displayArasScrollBar?(' + arasScrollBarConst + '):0) - docB.scrollTop;\n' +
	'	document.creatorOwner.showSubMenu(row.id, left, topVar);\n' +
	'}\n' +

	'document.onselectionchange = function () {return false;}\n' +

	'function onMouseOverRow(row) {\n' +
	'	if (row === highlightedRow) return;\n' +
	'	setHighLighted(row);\n' +
	'	if (row.clss == "submenu") showSubMenu(row);\n' +
	'	else document.creatorOwner.highlightItem(row.id);\n' +
	'}\n' +

	'function onClick(row) {\n' +
	'	if (row.clss == "menuitem") {\n' +
	'		document.creatorOwner.setClickedID(row.id);\n' +
	'	}\n' +
	'	else if (row.clss == "submenu") {\n' +
	'		if (document.creatorOwner.shownSubMenu) document.creatorOwner.hideSubMenu();\n' +
	'		else showSubMenu(row);\n' +
	'	}\n' +
	'}\n' +

	'function onKeyPress(event) {\n' +
	'	var keyCode = event.keyCode;\n' +
	'	if (keyCode == 27) {\n' +
	'		document.creatorOwner.hideAll();\n' +
	'	}\n' +
	'	else if (keyCode == 13) {\n' +
	'		if (highlightedRow) {\n' +
	'			if (highlightedRow.clss == "menuitem") {\n' +
	'				document.creatorOwner.setClickedID(highlightedRow.id);\n' +
	'			}\n' +
	'			else if (highlightedRow.clss) {\n' +
	'				if (document.creatorOwner.shownSubMenu) document.creatorOwner.hideSubMenu();\n' +
	'				else showSubMenu(highlightedRow);\n' +
	'			}\n' +
	'		}\n' +
	'	}\n' +
	'	else if (keyCode == 40) {\n' +//down arrow
	'		var nextRow = null;\n' +
	'		if (highlightedRow) nextRow = highlightedRow.nextSibling;\n' +
	'		else {\n' +
	'			var tables = document.all("mainTable").getElementsByTagName("TABLE");\n' +
	'			if (tables && tables.length>0) nextRow = tables(0);\n' +
	'		}\n' +
	'		if (nextRow) for( ; nextRow.tagName!="TABLE" || nextRow.disabled==true; ) {\n' +
	'			nextRow = nextRow.nextSibling;\n' +
	'			if (!nextRow) break;\n' +
	'		}\n' +

	'		if (!nextRow || nextRow.tagName!="TABLE" || nextRow.disabled) { \n' +
	'			var tables = document.all("mainTable").getElementsByTagName("TABLE");\n' +
	'			if (tables && tables.length>0) nextRow = tables(0);\n' +
	'		}\n' +
	'		if (nextRow) for( ; nextRow.tagName!="TABLE" || nextRow.disabled==true; ) {\n' +
	'			nextRow = nextRow.nextSibling;\n' +
	'			if (!nextRow) break;\n' +
	'		}\n' +
	'		if (!nextRow || nextRow.tagName!="TABLE" || nextRow.disabled) nextRow = null;\n' +

	'		if (nextRow) setHighLighted(nextRow);\n' +
	'	}\n' +
	'	else if (keyCode == 39) {\n' +//right arrow
	'		if (highlightedRow && highlightedRow.clss == "submenu") showSubMenu(highlightedRow);\n' +
	'	}\n' +
	'	else if (keyCode == 38) {\n' +//up arrow
	'	var prevRow = null;\n' +
	'	if (highlightedRow) prevRow = highlightedRow.previousSibling;\n' +
	'	else {\n' +
	'		var tables = document.all("mainTable").getElementsByTagName("TABLE");\n' +
	'		if (tables && tables.length>0) prevRow = tables(tables.length-1);\n' +
	'	}\n' +
	'	if (prevRow) for( ; prevRow.tagName!="TABLE" || prevRow.disabled==true; ) {\n' +
	'		prevRow = prevRow.previousSibling;\n' +
	'		if (!prevRow) break;\n' +
	'	}\n' +

	'	if (!prevRow || prevRow.tagName!="TABLE" || prevRow.disabled) {\n' +
	'		var tables = document.all("mainTable").getElementsByTagName("TABLE");\n' +
	'		if (tables && tables.length>0) prevRow = tables(tables.length-1);\n' +
	'	}\n' +
	'	if (prevRow) for( ; prevRow.tagName!="TABLE" || prevRow.disabled==true; ) {\n' +
	'		prevRow = prevRow.previousSibling;\n' +
	'		if (!prevRow) break;\n' +
	'	}\n' +

	'	if (!prevRow || prevRow.tagName!="TABLE" || prevRow.disabled) prevRow = null;\n' +
	'		if (prevRow) setHighLighted(prevRow);\n' +
	'	}\n' +
	'	else if (keyCode == 37) {\n' +//left arrow
	'		if (document.creatorOwner.parentMenu) document.creatorOwner.hide();\n' +
	'	}\n' +
	'}\n' +
	'</script>\n' +

	'<body ' +
	'oncontextmenu="return false;" ondblclick="return false;" onselectstart="return false;"' +
	'onkeydown="onKeyPress(event); return false;" ' +
	'onunload="document.creatorOwner.hideAll();" ' +
	'>');

	if (this.showLabel) {
		var label2display = this.getLabel().replace(/ /g, '&nbsp;');
		doc.writeln('<center><span id="labelSpan" align="center">&nbsp;&nbsp;' + label2display + '&nbsp;&nbsp;</span></center>');
		doc.writeln(this.getSeparator());
	}

	var expressionFunctionCode = '';

	doc.writeln('<center><span id="UpArrow" class="smsignZone" style="cursor:pointer;" onmouseover="bMousePressedDown=true; upDownArrowButtonClick(true);"' +
				' onmouseout="bMousePressedDown=false;">' +
				'<button id="UpArrowButton" DISABLED class="smsignZone" style="background-color:buttonface; width:expression(document.body.clientWidth);' +
				' cursor:pointer; border:none;">&#53;</button></span></center>');

	expressionFunctionCode += '' +
		'var field = document.getElementById(\'UpArrow\');\r\n' +
		'field.style[\'display\'] = displayArasScrollBar ? \'block\' : \'none\';\r\n';

	doc.writeln('<div id="mainDiv" width="100%" style="overflow:hidden; ">' +
			'<table id="mainTable" cellspacing="0" cellpadding="0" border="0"><tr><td nowrap valign="middle">');

	expressionFunctionCode += '' +
			'var field = document.getElementById(\'mainDiv\');\r\n' +
			'field.style[\'height\'] = document.body.clientHeight-(displayArasScrollBar? (2*' + arasScrollBarConst + '):0)';
	if (this.showLabel) {
		expressionFunctionCode += '-document.all(\'labelSpan\').offsetHeight';
	}

	expressionFunctionCode += ';\r\n';

	var const1 = '<font style="visibility:hidden" face="Marlett" size="1">&#97;</font>';
	var const2 = '<font face="Marlett"  size="1">&#97;</font>';

	for (i = 0; i < this.contentArr.length; i++) {
		itm = this.contentArr[i];
		if (itm.isSeparator) {
			doc.write(this.getSeparator());
		} else {
			var str =
			'<table clss="' + (itm.isMenu ? 'submenu' : 'menuitem') + '" id="' + itm.getId().toString().replace(/"/g, '&quot;') +
			'" onmouseover="onMouseOverRow(this); return false;" ' +
			'onclick="onClick(this); return false;" cellpadding="0" cellspacing="0" border="0"><tr>' +
			'<td align="left" valign="middle"><span class="csignZone" ' +
			((!itm.isMenu && itm.isChecked()) ? 'style="visibility:visible;"' : '') + '>&#97;</span></td>' +
			'<td align="left" valign="middle" class="labelTD" nowrap><span class="labelZone">' + itm.getLabel() + '</span></td>' +
			(itm.isMenu ? '<td align="left" valign="middle"><span class="smsignZone">&#52;</span></td>' : '') +
			'</tr></table>';

			doc.write(str);
		}
	}

	doc.writeln('<script>\n var elem = null;');
	for (i = 0; i < this.contentArr.length; i++) {
		itm = this.contentArr[i];
		if (!itm.isSeparator && !itm.isEnabled()) {
			doc.writeln(
				'elem = document.getElementById("' + itm.getId() + '");\n' +
				'if (elem && !elem.length) elem.disabled = true;'
			);
		}
	}
	doc.writeln('</script>');

	doc.writeln('</td></tr></table></div>');
	doc.writeln('<center><span id="DownArrow" class="smsignZone" style="cursor:pointer;" onmouseover="bMousePressedDown=true; upDownArrowButtonClick(false);"' +
				'onmouseout="bMousePressedDown=false;"><button id="DownArrowButton" class="smsignZone" style="background-color:buttonface;' +
				' width:expression(document.body.clientWidth); cursor:pointer; border:none;">&#54;</button></span></center>');
	expressionFunctionCode += '' +
	'var field = document.getElementById(\'DownArrow\');\r\n' +
	'field.style[\'display\'] = displayArasScrollBar ? \'block\' : \'none\';\r\n';

	var content = '<script type="text/javascript">';
	content += 'function expression_PopupMenu_setExpression(displayArasScrollBar)\r\n{\r\n' + expressionFunctionCode + '}\r\n';
	content += 'expression_PopupMenu_setExpression(document.displayArasScrollBar);';
	content += '</script>';

	doc.writeln(content);

	doc.writeln('</body></html>');
	doc.close();

	this.isShown = true;
	oPopup.show(left, topVar, 1, 1, this.win.document.body);
	var w1 = doc.all('mainTable').offsetWidth;
	var w2 = (this.showLabel ? doc.all('labelSpan').offsetWidth : 0);
	var width = ((w1 - w2 < 0) ? w2 : w1) + 2 * borderWidthConst + 2 * leftRightMarginConst;
	var height = doc.all('mainTable').offsetHeight + 2 * borderWidthConst + 2 * topBottomMarginConst + (this.showLabel ? doc.all('labelSpan').offsetHeight : 0);

	var maxHeight = window.screen.availHeight;
	this.left = left;
	this.topVar = topVar;
	this.width = width;
	this.height = (height - maxHeight <= 0) ? height : maxHeight;
	document.displayArasScrollBar = (height - maxHeight <= 0) ? false : true;

	field = oPopup.document.getElementById('UpArrow');
	field.style.display = oPopup.document.displayArasScrollBar ? 'block' : 'none';

	field = oPopup.document.getElementById('mainDiv');
	var newHeight = document.body.clientHeight - (document.displayArasScrollBar ? (2 * arasScrollBarConst) : 0);

	if (this.showLabel) {
		newHeight -= oPopup.document.all('labelSpan').offsetHeight;
	}
	field.style.height = newHeight;

	field = oPopup.document.getElementById('DownArrow');
	field.style.display = oPopup.document.displayArasScrollBar ? 'block' : 'none';

	oPopup.show(this.left, this.topVar, this.width, this.height, this.win.document.body);
	return true;
};

PopupMenu.prototype.highlightItem = function PopupMenuHighlightItem(menuitemID) {
	var submenu = this.submenus[menuitemID];
	if (this.shownSubMenu && (!submenu || this.shownSubMenu !== submenu)) {
		this.hideSubMenu();
	}
};

PopupMenu.prototype.showSubMenu = function PopupMenuShowSubMenu(submenuID, left, topVar) {
	var submenu = this.submenus[submenuID];
	if (!submenu) {
		return false;
	}
	if (this.shownSubMenu === submenu) {
		return true;
	}

	if (this.shownSubMenu) {
		this.hideSubMenu();
	}

	submenu.setParentWindow(this.oPopup.document.parentWindow);
	submenu.show(left, topVar);
	this.shownSubMenu = submenu;
	return true;
};

PopupMenu.prototype.hideSubMenu = function PopupMenuHideSubMenu() {
	if (!this.shownSubMenu) {
		return false;
	}

	this.shownSubMenu.hide();
	this.shownSubMenu = null;
};

PopupMenu.prototype.hide = function PopupMenuHide() {
	if (!this.isShown) {
		return true;
	}

	if (this.shownSubMenu) {
		this.shownSubMenu.hide();
	}
	if (this.parentMenu && this.parentMenu.shownSubMenu === this) {
		this.parentMenu.shownSubMenu = null;
	}

	this.oPopup.document.body.onunload = '';
	this.oPopup.hide();

	this.isShown = false;
	return true;
};

PopupMenu.prototype.hideAll = function PopupMenuHideAll(event) {
	if (!this.isShown) {
		return true;
	}

	if (this.parentMenu) {
		this.parentMenu.hideAll();
	} else {
		this.hide();
	}
};

PopupMenu.prototype.getItemCount = function PopupMenuGetItemCount() {
	return this.contentArr.length;
};

PopupMenu.prototype.getItem = function PopupMenuGetItem(pos) {
	var res = this.contentArr[pos];
	if (!res) {
		res = null;
	}
	return res;
};

PopupMenu.prototype.getItemById = function PopupMenuGetItemById(id) {
	var res = this.contentMap[id];
	if (!res) {
		for (var submenuID in this.submenus) {
			res = this.submenus[submenuID].getItemById(id);
			if (res) {
				break;
			}
		}
		if (!res) {
			res = null;
		}
	}

	return res;
};

PopupMenu.prototype.add = function PopupMenuAdd(arg1, arg2, arg3, arg4) {
	return this.insert(this.contentArr.length, arg1, arg2, arg3, arg4);
};

PopupMenu.prototype.addSeparator = function PopupMenuAddSeparator() {
	return this.insertSeparator(this.contentArr.length);
};

PopupMenu.prototype.insert = function(pos, arg2, arg3, arg4, arg5) {
	if (pos === undefined) {
		return null;
	}
	pos = parseInt(pos);
	if (isNaN(pos)) {
		return null;
	}

	if (arguments.length >= 2 && typeof (arg2) == 'object' && arg2.isMenu) {
		return this.insertSubMenu(pos, arg2, arg3);
	} else {
		return this.insertMenuItem(pos, arg2, arg3, arg4, arg5);
	}
};

PopupMenu.prototype.insertSeparator = function PopupMenuInsertSeparator(pos) {
	if (pos === undefined) {
		return null;
	}
	pos = parseInt(pos);
	if (isNaN(pos)) {
		return null;
	}

	var newItm = new MenuSeparator();
	if (!newItm) {
		return null;
	}

	if (pos < 0) {
		pos = 0;
	} else if (pos > this.contentArr.length) {
		pos = this.contentArr.length;
	}

	if (pos == this.contentArr.length) {
		this.contentArr.push(newItm);
	} else if (pos === 0) {
		var tmpArr = new Array(newItm);
		this.contentArr = tmpArr.concat(this.contentArr);
	} else {
		var tmpArr1 = this.contentArr.slice(0, pos);
		var tmpArr2 = this.contentArr.slice(pos);
		tmpArr1.push(newItm);
		this.contentArr = tmpArr1.concat(tmpArr2);
	}

	return newItm;
};

PopupMenu.prototype.insertMenuItem = function PopupMenuMenuItem(pos, pointID, pointLabel, pointEnabled, isCheckable) {
	if (pointID === undefined) {
		pointID = 'id_';
	}
	if (pointLabel === undefined) {
		pointLabel = pointID;
	}
	if (pointEnabled === undefined) {
		pointEnabled = true;
	}
	if (isCheckable === undefined) {
		isCheckable = false;
	}

	var newItm = new MenuItem(pointID, pointLabel, pointEnabled, isCheckable);
	if (!newItm) {
		return null;
	}

	if (this.contentMap[newItm.getId()]) {
		return null;
	}

	if (pos < 0) {
		pos = 0;
	} else if (pos > this.contentArr.length) {
		pos = this.contentArr.length;
	}

	if (pos == this.contentArr.length) {
		this.contentArr.push(newItm);
	} else if (pos === 0) {
		var tmpArr = new Array(newItm);
		this.contentArr = tmpArr.concat(this.contentArr);
	} else {
		var tmpArr1 = this.contentArr.slice(0, pos);
		var tmpArr2 = this.contentArr.slice(pos);
		tmpArr1.push(newItm);
		this.contentArr = tmpArr1.concat(tmpArr2);
	}

	this.contentMap[newItm.getId()] = newItm;

	return newItm;
};

PopupMenu.prototype.insertSubMenu = function PopupMenuInsertSubMenu(pos, subMenu, enabled) {
	if (enabled === undefined) {
		enabled = true;
	}

	if (this.contentMap[subMenu.getId()]) {
		return null;
	}

	if (pos < 0) {
		pos = 0;
	} else if (pos > this.contentArr.length) {
		pos = this.contentArr.length;
	}

	if (pos == this.contentArr.length) {
		this.contentArr.push(subMenu);
	} else if (pos === 0) {
		var tmpArr = new Array(subMenu);
		this.contentArr = tmpArr.concat(this.contentArr);
	} else {
		var tmpArr1 = this.contentArr.slice(0, pos);
		var tmpArr2 = this.contentArr.slice(pos);
		tmpArr1.push(subMenu);
		this.contentArr = tmpArr1.concat(tmpArr2);
	}

	this.submenus[subMenu.getId()] = subMenu;
	this.contentMap[subMenu.getId()] = subMenu;
	subMenu.setEnabled(enabled);
	subMenu.parentMenu = this;
};

PopupMenu.prototype.remove = function PopupMenuRemove(pos) {
	if (pos < 0) {
		return null;
	}
	if (pos >= this.getItemCount()) {
		return null;
	}

	var arasObj = aras;
	if (!arasObj) {
		arasObj = (function getMostTopWindowWithArasForPopupMenu(windowObj) {
			var win = windowObj ? windowObj : window;
			var prevWin = win;
			var winWithAras;
			while (win !== win.parent) {
				try {
					// Try access to any property with permission denied.
					var t = win.parent.name;
				} catch (excep) {
					break;
				}
				prevWin = win;
				win = win.parent;
				winWithAras = typeof win.aras !== 'undefined' ? win : winWithAras;
			}
			return winWithAras ? winWithAras : prevWin; // for work Innovator working in iframe case
		}().aras);
	}

	var res = this.contentArr[pos];
	if (res.isSeparator) {
	} else if (res.isMenu) {
		arasObj.deletePropertyFromObject(this.submenus, res.getId());
	} else {
		arasObj.deletePropertyFromObject(this.contentMap, res.getId());
	}

	this.contentArr.splice(pos, 1);
	this.hide();

	return res;
};

PopupMenu.prototype.removeAll = function PopupMenuRemoveAll() {
	this.contentArr = [];
	this.submenus = {};
	this.contentMap = {};

	this.hide();
};
// ----------   PopupMenu   ---------- //

// ********************************************************************* //

// ++++++++++   MenuItem   ++++++++++ //
function MenuItem(id, label, enabled, isCheckable) {
	if (id === undefined) {
		id = 'id_';
	}
	if (label === undefined) {
		label = id;
	}
	if (enabled === undefined) {
		enabled = true;
	}
	if (isCheckable === undefined) {
		isCheckable = false;
	}

	this.isMenuItem = true;

	this.id = id;
	this.label = label.toString();
	this.enabled = enabled;
	this.isCheckable = isCheckable;
	this.checked = false;
}

MenuItem.prototype.setLabel = function MenuItemSetLabel(label) {
	if (label === undefined) {
		return;
	}
	this.label = label.toString();
};

MenuItem.prototype.setEnabled = function MenuItemSetEnabled(enabled) {
	if (enabled === undefined) {
		return;
	}
	this.enabled = enabled;
};

MenuItem.prototype.setChecked = function MenuItemSetChecked(checked) {
	if (checked === undefined) {
		return;
	}
	this.checked = checked;
};

MenuItem.prototype.getId = function MenuItemGetId() {
	return this.id;
};

MenuItem.prototype.getLabel = function MenuItemGetLabel() {
	return this.label;
};

MenuItem.prototype.isEnabled = function MenuItemIsEnabled() {
	return this.enabled;
};

MenuItem.prototype.isChecked = function MenuItemIsChecked() {
	return this.checked;
};
// ----------   MenuItem   ---------- //

// ********************************************************************* //

// ++++++++++   MenuSeparator   ++++++++++ //
function MenuSeparator() {
	this.isSeparator = true;
}
// ----------   MenuSeparator   ---------- //

//++++++ Unit tests ++++++
// To run tests just uncomment this section, include this js file to html page, browse the html page.
/*
  //+++ Commont Section +++
function populateMenu(menu, obj) {
    for (var propID in obj) {
        var point = obj[propID];
        if (point=="separator")
        {
          menu.addSeparator();
        } else if (typeof(point) == 'object') {
            var m = new PopupMenu(propID, propID+'\'s Properties', false);
            populateMenu(m, point);
            menu.add(m);
        }
        else menu.add(propID, point, true);
    }
}

function onPPMClick(menuItm)
{
  var menuID = menuItm.getId();
  alert("Selected menu id is " + menuID);
}
  //--- Commont Section ---

  //+++ Test 01 +++
function test01()
{
    var ppm = new PopupMenu('test01_ppm', '', false);
    setSeparatorImgPath('../imagesLegacy/100x4_divider.gif');
    populatePropsList01(ppm);
    ppm.setOnClick(onPPMClick);
    window.focus();
    ppm.show(300, 300);
}
function populatePropsList01(ppm) {
    var obj = new Object();
    for (var i=0; i<10; i++)
      obj["sep"+i] = "separator";
    for (var i=0; i<50; i++)
    {
        obj[i] = "test01 Menu Item Number " + (i+1);
        var r = i/5;
        if (Math.ceil(r)==Math.floor(r))
          obj["sep2"+i] = "separator";
        r = i/10;
        if (Math.ceil(r)==Math.floor(r))
          obj["submenu"+r] = {p1:"menuItem 1", p2:"menuItem 2", p3:"menuItem 3"};
        r = i/7;
        if (Math.ceil(r)==Math.floor(r))
          obj["submenu2"+r] = {p0:"separator", p1:"menuItem 1", p2:"menuItem 2", p3:"menuItem 3", p4:"separator"};

    }
    for (var i=0; i<10; i++)
      obj["sep3"+i] = "separator";
    populateMenu(ppm, obj);
}
test01();
  //--- Test 01 ---
*/
//------ Unit tests ------
