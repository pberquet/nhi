﻿// (c) Copyright by Aras Corporation, 2004-2009.

function updateProvidedItemsGrid(itemsGrid, updatedItem) {
	if (!updatedItem || !window.isTearOff) {
		return false;
	}
	var updatedID = updatedItem.getAttribute('id');

	if (itemsGrid.isItemsGrid) {
		if (itemsGrid.itemTypeName == itemTypeName) {
			var grid = itemsGrid.grid;
			if (grid.getRowIndex(itemID) > -1) {
				var wasSelected = (grid.getSelectedItemIds().indexOf(itemID) > -1);

				if (updatedID != itemID) {
					itemsGrid.deleteRow(item);
				}
				itemsGrid.updateRow(updatedItem);

				if (wasSelected) {
					if (updatedID == itemID) {
						itemsGrid.onSelectItem(itemID);
					} else {
						var currSel = grid.getSelectedId();
						//if (currSel)
						itemsGrid.onSelectItem(currSel);
					}
				} //if (wasSelected)
			}
		}
	} //if (itemsGrid.isItemsGrid)
}

function deleteProvidedItemsGrid(itemsGrid) {
	if (itemsGrid.isItemsGrid && itemsGrid.itemTypeName === itemTypeName) {
		const grid = itemsGrid.grid;
		if (grid.getRowIndex(itemID) > -1) {
			grid.deleteRow(itemID);
		}
	}
}

window.aras = window.aras || window.parent.aras;
var updateItemsGrid = window.aras.decorateForMultipleGrids(updateProvidedItemsGrid);
var deleteRowFunc = window.aras.decorateForMultipleGrids(deleteProvidedItemsGrid);

function reloadForm(formToUpdate) {
	var tmp = aras.getFormForDisplay(itemID);
	aras.mergeItemRelationships(formToUpdate, tmp.node);
}

function updateMenuState() {
	if (window.isTearOff && document.frames.editor && document.frames.editor.refreshMenuState) {
		document.frames.editor.refreshMenuState();
	}
}

function getEditorFrame() {
	return (window.isTearOff ? document.frames.editor : window);
}

function onSaveCommand() {
	var statusId = aras.showStatusMessage('status', aras.getResource('', 'common.saving'), '../images/Progress.gif');
	var res = aras.saveItemEx(item);
	aras.clearStatusMessage(statusId);
	if (!res) {
		return true;
	}

	reloadForm(res);

	if (window.isTearOff) {
		updateItemsGrid(res);
	}
	aras.uiReShowItemEx(itemID, res, viewMode);
	return true;
}

function onLockCommand() {
	var statusId = aras.showStatusMessage('status', aras.getResource('', 'formtool_window.locking'), '../images/Progress.gif');
	var res = aras.lockItemEx(item);
	aras.clearStatusMessage(statusId);
	if (!res) {
		return true;
	}

	reloadForm(res);

	if (window.isTearOff) {
		updateItemsGrid(res);
	}

	isEditMode = true;
	aras.uiReShowItemEx(itemID, res, viewMode);
	return true;
}

function onUnlockCommand(silentMode) {
	var statusId = aras.showStatusMessage('status', aras.getResource('', 'common.unlocking'), '../images/Progress.gif');
	var res = aras.unlockItemEx(item, silentMode);
	aras.clearStatusMessage(statusId);
	if (!res) {
		return false;
	}

	reloadForm(res);

	if (window.isTearOff) {
		updateItemsGrid(res);
	}

	isEditMode = false;
	aras.uiReShowItemEx(itemID, res, viewMode);

	return true;
}

function onEditCommand() {
	if (aras.isTempEx(item)) {
		return true;
	}

	var isLockedByUser = aras.isLockedByUser(item);

	if (!isLockedByUser && this.onLockCommand) {
		return this.onLockCommand();
	}

	return true;
}

function onUndoCommand() {
	if (!aras.isDirtyEx(item)) {
		aras.AlertError(aras.getResource('', 'common.nothing_undo'));
		return true;
	}

	if (!aras.confirm(aras.getResource('', 'common.undo_discard_changes'))) {
		return true;
	}

	if (!aras.isTempEx(item)) {
		aras.removeFromCache(itemID);
		var frm = aras.getItemById('Form', itemID, 0);
		reloadForm(frm);
		if (item.parentNode) {
			item.parentNode.replaceChild(frm, item);
		}

		if (window.isTearOff) {
			updateItemsGrid(frm);
		}
		aras.uiReShowItemEx(itemID, frm, viewMode);
	}
	return true;
}

function onPurgeCommand() {
	return onPurgeDeleteCommand('purge');
}

function onDeleteCommand(silentMode) {
	return onPurgeDeleteCommand('delete', silentMode);
}

function onPurgeDeleteCommand(command, silentMode) {
	var res = false;

	if (command == 'purge') {
		res = aras.purgeItem('Form', itemID, false);
	} else {
		res = aras.deleteItem('Form', itemID, silentMode);
		if (res && this.item.parentNode && 'related_id' == this.item.parentNode.nodeName) {
			var relNd = this.item.parentNode.parentNode;
			aras.deleteItemEx(relNd, true);
		}
	}

	if (res) {
		if (window.isTearOff) {
			deleteRowFunc();
			window.close();
		} else {
			var formIT = aras.getItemFromServerByName('ItemType', 'Form', 'id');
			if (formIT) {
				work.location.replace('itemsGrid.html?itemtypeID=' + formIT.getAttribute('id'));
			} else {
				work.location.replace('../scripts/blank.html');
			}
		}
	}

	return {result: res ? 'Deleted' : 'Canceled'};
}

function onPrintCommand() {
	//no any code since pdf printing approach implementation
	return true;
}

function onExport2OfficeCommand(targetAppType) {
	aras.export2Office(function() { return ''; }, targetAppType, item);
	return true;
}

function onSaveUnlockAndExitCommand() {
	var statusId;
	if (window.isTearOff) {
		statusId = aras.showStatusMessage('status', aras.getResource('', 'formtool_window.saving'), '../images/Progress.gif');
	}
	var res = aras.saveItemEx(item, false);
	if (window.isTearOff && statusId) {
		aras.clearStatusMessage(statusId);
	}
	if (!res) {
		return true;
	}

	if (window.isTearOff) {
		statusId = aras.showStatusMessage('status', aras.getResource('', 'formtool_window.unlocking'), '../images/Progress.gif');
	}
	res = aras.unlockItemEx(res);
	if (window.isTearOff && statusId) {
		aras.clearStatusMessage(statusId);
	}
	if (!res) {
		return true;
	}

	if (window.isTearOff) {
		reloadForm(res);
		updateItemsGrid(res);
		window.close();
	}

	return true;
}

var windowReady = true;
