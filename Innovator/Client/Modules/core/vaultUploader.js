﻿import xml from './Xml';
import { soap } from './Soap';
import vault from './vault';
/**
 * @typedef {File} ExtendedFile
 * @property {String} fileItemId - Id from AML Item of file
 *
 * Internal module type.
 */

/**
 * @typedef {String} FileState
 *
 * Internal module type.
 * Each file have states:
 * - "pending" - file not uploaded and has waiting for upload;
 * - "in progress" - some chunks of file currently are in upload;
 * - "uploaded" - file is uploaded.
 */

/**
 * @typedef {Object} Chunk
 * @property {Blob} data
 * @property {Number} from - Count of file bytes where starts the chunk range
 * @property {Number} to - Count of file bytes where ends the chunk range
 *
 * Internal module type.
 * Serves to allow a possibility to upload a chunk multiple times
 */

/**
 * Serves to store the states of file uploading
 *
 * @param {ExtendedFile} file
 * @param {Boolean} isAsync
 *
 * @property {String} fileName
 * @property {String} fileItemId
 * @property {Number} fileSize
 * @property {File} _file
 * @property {Boolean} _isAsync - Chunks of file can be uploaded synchronously or not
 * @property {FileState} _state
 * @property {Number} _sentBytes - Watch to
 * @property {Number} _readingPoint
 * @property {Number} chunkSize
 *
 * @constructor
 */
function FileWrapper(file, isAsync, worker) {
	this.fileName = file.name;
	this.fileItemId = file.fileItemId;
	this.fileSize = file.size;

	this._file = file;
	this._isAsync = isAsync;
	this._state = 'pending';

	this._sentBytes = 0;
	this._readingPoint = 0;

	if (this.fileSize && worker) {
		this._hashWaiting = {};
		this._hashes = {};
		this.initialize(worker);
	}
}

FileWrapper.prototype = {
	chunkSize: 16 * 1024 * 1024, // 16 Mb
	constructor: FileWrapper,
	/**
	 * Method needs to split file at chunks to upload.
	 * Chunks splits by the schema:
	 * ChunkSize = 16 Mb
	 *
	 * file: [________________36Mb________________]
	 * 1 ch: [0Mb + 16Mb]_________________________
	 * 2 ch: ____________[16Mb + 16Mb]____________
	 * 3 ch: _________________________[32Mb + 4Mb]
	 *
	 * @return {Chunk|undefined} undefined if file is empty
	 */
	getData: function() {
		this._state = 'in progress';

		if (this.fileSize === 0) {
			return Promise.resolve();
		}

		const offset = this._readingPoint;
		this._readingPoint = Math.min(this.fileSize, offset + this.chunkSize);

		const promise = new Promise(
			function(resolve, reject) {
				if (!this._hashes || this._hashes[offset]) {
					resolve(this._formatPacket(offset));
				} else {
					this._hashWaiting[offset] = resolve;
				}
			}.bind(this)
		);

		return promise;
	},
	/**
	 * Toggles state of file upload to "uploaded" or "pending"
	 */
	endUpload: function() {
		this._sentBytes = Math.min(this.fileSize, this._sentBytes + this.chunkSize);
		this._state = this._sentBytes === this.fileSize ? 'uploaded' : 'pending';
	},
	isAllowedUpload: function() {
		if (
			this._state === 'uploaded' ||
			(!this._isAsync && this._state === 'in progress')
		) {
			return false;
		} else if (this.fileSize === 0 && this._state === 'pending') {
			return true;
		}

		return this._readingPoint < this.fileSize;
	},
	initialize: function(worker) {
		let offset = 0;
		const receiveHash = e => {
			const receivedData = e.data;

			if (receivedData.fileId === this.fileItemId) {
				offset = Math.min(this.fileSize, offset + this.chunkSize);
				this._hashes[receivedData.offset] = receivedData.xxhash;

				if (this._hashWaiting[receivedData.offset]) {
					this._hashWaiting[receivedData.offset](
						this._formatPacket(receivedData.offset)
					);
				}
				if (offset < this.fileSize) {
					worker.postMessage(this._formatPacket(offset));
				}
			}
		};

		worker.addEventListener('message', receiveHash);
		worker.postMessage(this._formatPacket(0));
	},

	_formatPacket: function(offset) {
		const to = Math.min(this.fileSize, offset + this.chunkSize);
		return {
			fileId: this.fileItemId,
			from: offset,
			to: to,
			hash: this._hashes ? this._hashes[offset] : null,
			data: this._file.slice(offset, to)
		};
	}
};

/**
 * Class is needed for encapsulation of each upload transaction of files
 * to providing opportunity to send more than 1 set of files to upload and prevent conflicts.
 *
 * @param {FileList|File[]} files
 * @param {{credentials: Object, isAsync: Boolean, serverUrl: String}} options
 * @param {String} aml
 *
 * @property {Object} _credentials
 * @property {String} _serverUrl
 * @property {FileWrapper[]} _filesWrappers
 * @property {Number} attempts - The number of attempts of try to repeat upload chunk
 *
 * @constructor
 */
function FilesUploader(files, options, aml) {
	this._credentials = Object.assign({}, options.credentials);
	this._serverUrl = options.serverUrl;
	this._fileWrappers = [];
	this.attempts = 10;

	this._initFileWrappers(files, !!options.isAsync, aml);
}

FilesUploader.prototype = {
	constructor: FilesUploader,
	/**
	 * @param {FileList|File[]} files
	 * @param {Boolean} isAsync
	 * @param {String} aml
	 * @private
	 */
	_initFileWrappers: function(files, isAsync, aml) {
		const doc = xml.parseString(aml);
		let fileItems = xml.selectNodes(
			doc,
			"//Item[@type='File' and @action='add']"
		);
		let fileItem;
		let file;
		if (!this._serverUrl.startsWith('https')) {
			this._xxhWorker = new Worker('../Modules/core/xxhWorker.js');
		}

		fileItems = Array.prototype.slice.call(fileItems);

		// search item file id for upload transaction
		for (let i = 0, n = files.length; i < n; i++) {
			file = files[i];

			for (let j = 0, m = fileItems.length; j < m; j++) {
				fileItem = fileItems[j];
				if (
					file.name === fileItem.selectSingleNode('filename').text &&
					file.size === +fileItem.selectSingleNode('file_size').text
				) {
					file.fileItemId = fileItem.getAttribute('id');
					fileItems.splice(j, 1);
					break;
				}
			}

			this._fileWrappers.push(new FileWrapper(file, isAsync, this._xxhWorker));
		}
	},
	/**
	 * Sets the existing transaction id as request header with the other headers.
	 * Thus server can identify a context of uploaded files
	 *
	 * @param {XMLHttpRequest} xhr
	 * @param {Object} [headers]
	 * @private
	 */
	_setHeaders: function(xhr, headers) {
		const topWnd = TopWindowHelper.getMostTopWindowWithAras();
		const aras = topWnd.aras;

		headers = Object.assign({}, this._credentials, headers || {});
		headers = Object.assign(headers, aras.OAuthClient.getAuthorizationHeader());

		if (this._transactionId) {
			headers.transactionid = this._transactionId;
		}

		Object.keys(headers).forEach(function(header) {
			xhr.setRequestHeader(header, headers[header]);
		});
	},
	/**
	 * @param {String|Node} response
	 * @returns {Node|undefined}
	 * @private
	 */
	_selectResultNode: function(response) {
		if (response.nodeName) {
			return response;
		}

		const xmlModule = xml;
		const doc = xmlModule.parseString(response);

		return xmlModule.selectSingleNode(doc, '//Result');
	},
	/**
	 * Serves to beginning of upload transaction on the server and takes its transaction id
	 *
	 * @returns {Promise} Resolves without parameters
	 */
	beginTransaction: function() {
		const self = this;

		return soap('', {
			async: true,
			method: 'BeginTransaction',
			url: self._serverUrl,
			headers: Object.assign({}, self._credentials)
		})
			.then(function(response) {
				const node = self._selectResultNode(response);

				if (!node) {
					// will caught below to throw an error
					return Promise.reject({ status: 200, responseText: response });
				}

				self._transactionId = node.text;
			})
			.catch(function(xhr) {
				throw new Error(
					xhr.status +
						' occurred while beginning transaction with text: ' +
						xhr.responseText
				);
			});
	},
	/**
	 * Serves to rollback of upload transaction on the server which removes earlier uploaded files
	 *
	 * @returns {undefined | Promise} undefined returns if no sense in rollback | Promise resolves with xhr
	 */
	rollbackTransaction: function() {
		if (!this._transactionId) {
			return;
		}

		const headers = Object.assign(
			{
				transactionid: this._transactionId
			},
			this._credentials
		);

		return soap('', {
			async: true,
			method: 'RollbackTransaction',
			url: this._serverUrl,
			headers: headers
		});
	},
	/**
	 * Serves to commit of upload transaction on the server and applies aml as the context of uploaded files
	 *
	 * @param {String} aml
	 * @returns {Promise} Resolves with xml node
	 */
	commitTransaction: function(aml) {
		const self = this;

		return new Promise(function(resolve, reject) {
			const formData = new FormData();
			const xhr = new XMLHttpRequest();

			formData.append('XMLData', aml);

			xhr.open('POST', self._serverUrl, true);
			self._setHeaders(xhr, {
				SOAPAction: 'CommitTransaction'
			});
			xhr.addEventListener('error', function() {
				reject(xhr);
			});
			xhr.addEventListener('load', function() {
				if (xhr.status < 200 || xhr.status > 299) {
					reject(xhr);
				} else {
					const node = self._selectResultNode(xhr.responseText);

					if (!node) {
						reject(xhr);
					}

					resolve(node);
				}
			});
			xhr.send(formData);
		}).catch(function(xhr) {
			if (xhr.status === 200) {
				const responseXML = xml.parseString(xhr.responseText);
				throw responseXML;
			}

			throw new Error(
				xhr.status +
					' occurred while commit transaction with text: ' +
					xhr.responseText
			);
		});
	},
	/**
	 * Encodes file name for Content-Disposition
	 *
	 * @returns {String} Encoded string
	 * @private
	 */
	_encodeRFC5987ValueChars: function(str) {
		// noinspection JSDeprecatedSymbols
		return encodeURIComponent(str)
			.replace(/['()]/g, escape) // i.e., %27 %28 %29
			.replace(/\*/g, '%2A')
			.replace(/%(?:7C|60|5E)/g, unescape);
	},
	/**
	 * @returns {undefined | FileWrapper} A wrapper of file which is not uploaded fully
	 * @private
	 */
	_searchNotUploadedFile: function() {
		// simple cycle used to avoid unnecessary iterations
		for (let i = 0, n = this._fileWrappers.length; i < n; i++) {
			if (this._fileWrappers[i].isAllowedUpload()) {
				return this._fileWrappers[i];
			}
		}
	},
	/** @private */
	_upload: function(fileWrapper, dataObject) {
		const self = this;

		return new Promise(function(resolve, reject) {
			const XmlHttpRequest = fileWrapper._file.Xhr || XMLHttpRequest;
			const xhr = new XmlHttpRequest();
			const url =
				self._serverUrl +
				(self._serverUrl.indexOf('?') > -1 ? '&' : '?') +
				'fileId=' +
				fileWrapper.fileItemId;

			xhr.open('POST', url, true);
			xhr.addEventListener('error', function(event) {
				reject(event.target.statusText);
			});
			xhr.addEventListener('load', function(event) {
				if (event.target.status !== 200) {
					reject(event.target.statusText);
					return;
				}
				fileWrapper.endUpload();
				resolve();
			});

			const contentRange = !dataObject
				? '*/0'
				: dataObject.from +
				  '-' +
				  (dataObject.to - 1) +
				  '/' +
				  fileWrapper.fileSize;

			const headers = {
				'Content-Type': 'application/octet-stream',
				'Content-Range': 'bytes ' + contentRange,
				SOAPAction: 'UploadFile',
				'Content-Disposition':
					"attachment; filename*=utf-8''" +
					self._encodeRFC5987ValueChars(fileWrapper.fileName)
			};
			if (self._xxhWorker) {
				headers['Aras-Content-Range-Checksum'] = dataObject
					? dataObject.hash
					: '46947589';
				headers['Aras-Content-Range-Checksum-Type'] =
					'xxHashAsUInt32AsDecimalString';
			}
			self._setHeaders(xhr, headers);
			xhr.send(dataObject ? dataObject.data : null);
		}).catch(function(message) {
			if (self.attempts > 0) {
				self.attempts--;
				return self._upload(fileWrapper, dataObject);
			}
			self.attempts--;
			return Promise.reject(message);
		});
	},
	/**
	 * Serves to filter and send data to the server
	 *
	 * @returns {Promise} undefined returns if no data to send or file already loaded in sync mode
	 * Promise resolves without parameters
	 */
	send: function() {
		const fileWrapper = this._searchNotUploadedFile();
		if (!fileWrapper || this.attempts === -1) {
			return Promise.resolve();
		}
		return fileWrapper
			.getData()
			.then(
				function(data) {
					return this._upload(fileWrapper, data);
				}.bind(this)
			)
			.then(this.send.bind(this));
	},
	destroyWorker: function() {
		if (this._xxhWorker) {
			this._xxhWorker.terminate();
			this._xxhWorker = null;
		}
	}
};

/**
 * Serves to control upload queue of data.
 * For each call of send makes new instance of it to avoid redundant
 * complexity in cases when exists only one vault instance in application.
 *
 * @constructor
 */
function RequestsManager() {}

RequestsManager.prototype = {
	constructor: RequestsManager,
	parallelRequests: 3,
	/**
	 * Serves to start parallel requests
	 *
	 * @param {FilesUploader} filesUploader
	 * @returns {Promise} Resolves without parameters
	 * @private
	 */
	_upload: function(filesUploader) {
		const array = [];
		for (let i = this.parallelRequests; i--; ) {
			array.push(filesUploader.send());
		}

		return Promise.all(array);
	},
	/**
	 * Serves to control action sequence of upload
	 *
	 * @param {FileList|File[]} files
	 * @param {{credentials: Object, isAsync: Boolean, serverUrl: String}} options
	 * @param {String} aml
	 * @returns {Promise|undefined} Resolves with response after commit transaction
	 */
	send: function(files, options, aml) {
		const self = this;
		const filesUploader = new FilesUploader(files, options, aml);

		return filesUploader
			.beginTransaction()
			.then(function() {
				return self._upload(filesUploader);
			})
			.then(function() {
				filesUploader.destroyWorker();
				return filesUploader.commitTransaction(aml);
			})
			.catch(function(error) {
				filesUploader.destroyWorker();
				filesUploader.rollbackTransaction();
				return Promise.reject(error);
			});
	}
};

let vaultUploader = {
	options: {
		serverUrl: 'http://localhost/S11-0-SP8',
		credentials: {},
		isAsync: false
	},
	/**
	 * @param {FileList|File[]} files
	 * @param {String} aml
	 * @returns {Promise}
	 */
	send: function(files, aml) {
		return new RequestsManager().send(files, this.options, aml);
	}
};

vaultUploader = Object.assign(vault || {}, vaultUploader);
export default vaultUploader;
