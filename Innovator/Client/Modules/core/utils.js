﻿const utils = {};

utils.mixin = function(...arg) {
	const target = arg[0];

	const _len = arg.length;
	const sources = Array(_len > 1 ? _len - 1 : 0);

	for (let _key = 1; _key < _len; _key++) {
		sources[_key - 1] = arg[_key];
	}

	sources.forEach(function(source) {
		const descriptors = Object.keys(source).reduce(function(descriptors, key) {
			descriptors[key] = Object.getOwnPropertyDescriptor(source, key);
			return descriptors;
		}, {});
		// by default, Object.assign copies enumerable Symbols too
		Object.getOwnPropertyNames(source).forEach(function(sym) {
			const descriptor = Object.getOwnPropertyDescriptor(source, sym);
			if (descriptor.enumerable) {
				descriptors[sym] = descriptor;
			}
		});
		Object.defineProperties(target, descriptors);
	});
	return target;
};

utils.infernoFlags = {
	unknownChildren: 0,
	hasInvalidChildren: 1,
	hasVNodeChildren: 2,
	hasNonKeyedChildren: 4,
	hasKeyedChildren: 8,
	multipleChildren: 12,
	hasTextChildren: 16,
	componentFunction: 8
};

utils.templateToVNode = function(template) {
	if (template === null) {
		return template;
	}

	if (typeof template !== 'object' || !template.tag) {
		if (template.flags && template.type) {
			return template;
		}

		return Inferno.createTextVNode(template);
	}

	let children;
	if (template.children) {
		children = template.children.map(child => utils.templateToVNode(child));
	}

	const props = {
		style: template.style
	};
	Object.assign(props, template.attrs || {}, template.events || {});

	let childFlag;
	if (!children || (!Array.isArray(children) && typeof children !== 'object')) {
		childFlag = this.infernoFlags.hasInvalidChildren;
	} else if (!Array.isArray(children)) {
		childFlag = this.infernoFlags.hasVNodeChildren;
	} else if (children.every(child => !!child.key)) {
		childFlag = this.infernoFlags.hasKeyedChildren;
	} else {
		childFlag = this.infernoFlags.hasNonKeyedChildren;
	}
	return Inferno.createVNode(
		Inferno.getFlagsForElementVnode(template.tag),
		template.tag,
		template.className,
		children,
		childFlag,
		props,
		template.key,
		template.ref
	);
};

utils.hashFromString = function(string) {
	let hash = 0;
	if (string.length === 0) {
		return hash;
	}
	for (let i = 0; i < string.length; i++) {
		const char = string.charCodeAt(i);
		hash = (hash << 5) - hash + char;
		hash |= 0; // Convert to 32bit integer
	}
	return hash;
};

utils.extendHTMLElement = function(...arg) {
	return typeof Reflect === 'object'
		? Reflect.construct(HTMLElement, arg, this.constructor)
		: HTMLElement.apply(this, arg) || this;
};

export default utils;
