﻿importScripts('../../vendors/xxhash-asm.min.js');
const xxHash = function(arrayBuffer, seed) {
	seed = seed || 0;

	const module = xxhAsm();

	const xxh32CreateState = module.cwrap('XXH32_createState', 'number');
	const xxh32Reset = module.cwrap('XXH32_reset', 'number', [
		'number',
		'number'
	]);
	const xxh32Update = module.cwrap('XXH32_update', 'number', [
		'number',
		'array',
		'number'
	]);
	const xxh32Digest = module.cwrap('XXH32_digest', 'number', ['number']);

	const totalSize = 4 * 1024 * 1024;

	const state = xxh32CreateState();
	xxh32Reset(state, seed);

	const count = Math.ceil(arrayBuffer.byteLength / totalSize);
	for (let i = 0; i < count; i++) {
		const size = Math.min(totalSize, arrayBuffer.byteLength - i * totalSize);
		let array = new Uint8Array(arrayBuffer, i * totalSize, size);
		xxh32Update(state, array, array.byteLength);
		array = null;
	}

	const hash = (xxh32Digest(state) >>> 0).toString();
	xxh32Reset(state, seed);
	return hash;
};

onmessage = function(e) {
	const receivedData = e.data.data;
	const fileId = e.data.fileId;
	const offset = e.data.from;
	let reader = new FileReaderSync();
	let byteData = reader.readAsArrayBuffer(receivedData);
	const xxhash = xxHash(byteData);

	const result = {
		xxhash: xxhash,
		fileId: fileId,
		offset: offset
	};

	// we remove links to objects in memory to prevent keeping references and to stable garbage collector working
	reader = null;
	byteData = null;
	postMessage(result);
};
