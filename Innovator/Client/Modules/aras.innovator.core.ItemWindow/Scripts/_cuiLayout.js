﻿let confirmationDialogPromise;
function confirmExitEditMode() {
	if (!window.aras.isDirtyEx(window.item) || window.aras.isTempEx(window.item)) {
		return Promise.resolve(true);
	}

	cuiWindowFocusHandler();

	const confirmDialogParams = {
		additionalButton: {
			text: window.aras.getResource('', 'common.discard'),
			actionName: 'discard'
		},
		okButtonText: window.aras.getResource('', 'common.save'),
		title: window.aras.getResource('', 'item_methods_ex.unsaved_changes')
	};
	const confirmDialogMessage = window.aras.getResource('', 'item_methods_ex.changes_not_saved');
	return window.ArasModules.Dialog.confirm(confirmDialogMessage, confirmDialogParams).then(function(dialogResult) {
		if (!dialogResult || dialogResult === 'cancel') {
			return false;
		}

		window.ignorePageCloseHooks = true;
		if (dialogResult === 'ok') {
			return window.onDoneCommand();
		}

		return window.onUnlockCommand(false);
	});
}
function confirmExitWithUnsavedFiles() {
	cuiWindowFocusHandler();

	const dialogParams = {
		title: window.aras.getResource('', 'item_methods_ex.unsaved_changes')
	};
	const confirmDialogMessage = window.aras.getResource('', 'file_management.files_will_be_lost');
	return window.ArasModules.Dialog.confirm(confirmDialogMessage, dialogParams)
		.then(function(checkedBtn) {
			if (checkedBtn !== 'ok') {
				return false;
			}

			removeAllUnsavedFiles();
			return true;
		});
}
function cuiWindowBeforeUnloadHandler(e) {
	if (
		aras.getCommonPropertyValue('exitInProgress') === true ||
		window.ignorePageCloseHooks ||
		!window.aras.isDirtyEx(window.item) ||
		window.aras.isTempEx(window.item)
	) {
		return;
	}

	if (!confirmationDialogPromise) {
		confirmationDialogPromise = confirmExitEditMode().then(function(shouldCloseWindow) {
			if (shouldCloseWindow) {
				window.close();
			}

			confirmationDialogPromise = null;
		});
	}

	e.returnValue = window.aras.getResource('', 'item_methods_ex.unsaved_changes');
	return e.returnValue;
}
function cuiWindowUnloadHandler() {
	if (!window.aras) {
		return;
	}

	if (
		aras.getCommonPropertyValue('exitInProgress') !== true &&
		!window.ignorePageCloseHooks &&
		window.isEditMode &&
		window.aras.isLockedByUser(window.item)
	) {
		window.onUnlockCommand(false, {skipReshow: true});
	}

	if (window.itemID) {
		window.aras.uiUnregWindowEx(window.itemID);
	}
}
function cuiWindowFocusHandler() {
	if (arasTabsobj && arasTabsobj.selectedTab !== window.frameElement.id) {
		arasTabsobj.selectTab(window.frameElement.id);
	}
}
function cuiWindowCloseHandler(callback, ignorePageCloseHooks) {
	if (!callback || aras.getCommonPropertyValue('exitInProgress') === true) {
		arasTabsobj.removeTab(this.frameElement.id, ignorePageCloseHooks);
		return;
	}

	if (confirmationDialogPromise) {
		return;
	}

	if (
		ignorePageCloseHooks &&
		(!window.aras.Browser.isIe() ||
		!(window.getUnsavedFiles && window.getUnsavedFiles().length > 0) ||
		item.getAttribute('action') === 'skip')
	) {
		window.ignorePageCloseHooks = true;
		callback(true);
		return;
	}

	confirmationDialogPromise = ignorePageCloseHooks ? confirmExitWithUnsavedFiles() : confirmExitEditMode();
	confirmationDialogPromise.then(function(shouldClose) {
		if (shouldClose && ignorePageCloseHooks) {
			window.ignorePageCloseHooks = true;
		}

		confirmationDialogPromise = null;
		callback(shouldClose);
	});
}
function cuiCommandBarChangeHandler() {
	if (relationshipsControl) {
		relationshipsControl.resize();
	}
}
