﻿ModulesManager.define(
	['aras.innovator.core.ItemWindow/DefaultItemWindowView'],
	'aras.innovator.core.ItemWindow/FormItemWindowView',
	function(DefaultItemWindowView) {
		function FormItemWindowView(inDom, inArgs) {
			this.inDom = inDom;
			this.inArgs = inArgs;
		}

		FormItemWindowView.prototype = new DefaultItemWindowView();

		FormItemWindowView.prototype.getWindowProperties = function() {
			var result;
			var topWindow = window;

			var screenHeight = topWindow.screen.availHeight;
			var screenWidth = topWindow.screen.availWidth;
			var mainWindowHeight = topWindow.outerHeight;
			var mainWindowWidth = topWindow.outerWidth;

			var sizeTrue = mainWindowHeight > 800 && mainWindowWidth > 1200;
			var tempHeight = sizeTrue ? screenHeight : 800;	// 800*0.8= 640px
			var tempWidth = sizeTrue ? screenWidth : 1200;	// 1200*0.8= 960px	960*640 is default size if the main window smaller than 1200*800

			var FormWindowHeight = tempHeight * 0.8;
			var FormWindowWidth = tempWidth * 0.8;

			// workflowMap window will be center-aligned
			var FormWindowTop = (screenHeight - FormWindowHeight) / 2;
			var FormWindowLeft = (screenWidth - FormWindowWidth) / 2;

			result = {height: FormWindowHeight, width: FormWindowWidth, x: FormWindowLeft, y: FormWindowTop};
			return result;
		};

		FormItemWindowView.prototype.getWindowArguments = function() {
			var result = {};

			var formNd = this.inDom;
			var arasObj = aras;

			var formID = formNd.getAttribute('id');

			var url = 'FormTool/formtool.html?formID=' + formID;

			var isEditMode = (arasObj.isTempEx(formNd) || arasObj.isLockedByUser(formNd)) ? true : false;
			var typeID = formNd.getAttribute('typeId');
			var itemTypeNd = arasObj.getItemTypeDictionary(typeID ? arasObj.getItemTypeName(typeID) : 'Form').node;
			var itemTypeLabel = arasObj.getItemProperty(itemTypeNd, 'label');
			if (!itemTypeLabel) {
				itemTypeLabel = itemTypeName;
			}
			var tmpKey = (isEditMode ? 'ui_methods_ex.itemtype_label_item_keyed_name' : 'ui_methods_ex.itemtype_label_item_keyed_name_readonly');
			var keyedName = arasObj.getKeyedNameEx(formNd);

			var title = arasObj.getResource('', tmpKey, itemTypeLabel, keyedName);

			result.itemTypeLabel = itemTypeLabel;
			result.url2tool = url;
			result.title = title;
			result.item = formNd;
			result.itemID = formID;
			result.itemTypeName = 'Form';
			result.itemType = itemTypeNd;
			result.viewMode = '';
			result.isEditMode = isEditMode;
			result.aras = arasObj;

			return result;
		};

		FormItemWindowView.prototype.getWindowUrl = function() {
			var result;
			var arasObj = aras;

			result = arasObj.getBaseURL() + '/Modules/aras.innovator.core.ItemWindow/formView';
			return result;
		};

		return FormItemWindowView;
	});
