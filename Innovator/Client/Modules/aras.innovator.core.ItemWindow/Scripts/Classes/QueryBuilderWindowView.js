﻿ModulesManager.define(
	['aras.innovator.core.ItemWindow/DefaultItemWindowView'],
	'aras.innovator.core.ItemWindow/QueryBuilderWindowView',
	function(DefaultItemWindowView) {
		function QueryBuilderWindowView(inDom, inArgs) {
			this.inDom = inDom;
			this.inArgs = inArgs;
		}

		QueryBuilderWindowView.prototype = new DefaultItemWindowView();

		QueryBuilderWindowView.prototype.getViewUrl = function() {
			return '/Modules/aras.innovator.core.ItemWindow/queryBuilderView';
		};

		return QueryBuilderWindowView;
	});
