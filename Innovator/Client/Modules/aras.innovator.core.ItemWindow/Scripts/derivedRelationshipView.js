﻿(function() {
	const baseLock = window.onLockCommand;
	const baseUnlock = window.onUnlockCommand;
	const baseSave = window.onSaveCommand;
	const baseSaveUnlockExit = window.onSaveUnlockAndExitCommand;
	const baseRefresh = window.onRefresh;

	window.onSaveCommand = function() {
		const itemController = new ItemController();

		itemController.dRFItem = item;
		const validationResult = itemController.validate();

		if (!validationResult.isValid) {
			const res = aras.getResource('../Modules/aras.innovator.DomainAccessControl/', 'unused_derived_relationships_error_msg', validationResult.unMapped);
			return aras.AlertError(res);
		}

		itemController.onBeforeAction(true);
		return baseSave.apply(this, arguments).then(function(res) {
			itemController.dRFItem = item;
			itemController.onAfterAction();
			if (!aras.isTempEx(item)) {
				dijit.byId('sidebar').getChildren().find(function(btn) { return btn.id === 'dr_showGrid'; }).domNode.style.display = 'block';
				viewModel.render();
			}
		});
	};
	window.onSaveUnlockAndExitCommand = function() {
		const itemController = new ItemController();

		itemController.dRFItem = item;
		const validationResult = itemController.validate();

		if (!validationResult.isValid) {
			const res = aras.getResource('../Modules/aras.innovator.DomainAccessControl/', 'unused_derived_relationships_error_msg', validationResult.unMapped);
			return aras.AlertError(res);
		}

		itemController.onBeforeAction(true);
		return baseSaveUnlockExit.apply(this, arguments).then(function() {
			itemController.dRFItem = item;
			itemController.onAfterAction();
			if (!aras.isTempEx(item)) {
				viewModel.render();
			}
		});
	};
	window.onLockCommand = function() {
		const itemController = new ItemController();
		itemController.dRFItem = item;
		itemController.onBeforeAction(false);
		const val = baseLock.apply(this, arguments);

		itemController.dRFItem = item;
		itemController.onAfterAction();
		viewModel.render();
		return val;
	};
	window.onUnlockCommand = function(res) {
		const itemController = new ItemController();
		itemController.dRFItem = item;

		if (res) {
			const validationResult = itemController.validate();

			if (!validationResult.isValid) {
				const res = aras.getResource('../Modules/aras.innovator.DomainAccessControl/', 'unused_derived_relationships_error_msg', validationResult.unMapped);
				aras.AlertError(res);
				return;
			}
		}
		itemController.onBeforeAction(res);
		const val = baseUnlock(res);
		itemController.dRFItem = item;
		itemController.onAfterAction();
		viewModel.render();
		return val;
	};
	window.onRefresh = function() {
		const val = baseRefresh.apply(this, arguments);

		if (aras.isTempEx(item)) {
			new ItemController().setViewModeOnRelationships();
		} else {
			viewModel.render();
		}
		return val;
	};
	window.addEventListener('load', function() {
		if (aras.isTempEx(item)) {
			new ItemController().setViewModeOnRelationships();
		}
	});
	document.addEventListener('loadSideBar', function() {
		dijit.byId('sidebar').switchSidebarButton('dr_showForm', '../Images/ShowFormOn.svg', true);
	});
}());
