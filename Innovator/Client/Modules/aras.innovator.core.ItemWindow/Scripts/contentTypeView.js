﻿var item = window.opener[paramObjectName].item;
var currContentTypeNode = window.opener[paramObjectName].item;
var isEditMode = window.opener[paramObjectName].isEditMode;

////////// Need For item_window.js //////////
var itemType = window.opener[paramObjectName].itemType;
var isTearOff = true;
var itemTypeName = item.getAttribute('type');
var itemID = item.getAttribute('id');
var viewMode = '';
viewType = 'formtool';
var LocationSearches = {};
var menuFrame;
var isNew = window.opener[paramObjectName].isNew;
window.opener[paramObjectName] = undefined;
////////// End Need For item_window.js //////////

///////// Default Windows Functions /////////

function resizeHandler() {
	if (!containerWidget) {
		return;
	}

	var containerHeight = containerWidget.domNode.offsetHeight;
	containerWidget.previousHeight = containerWidget.previousHeight || 0;
	if (containerHeight != containerWidget.previousHeight) {
		tabsPaneWidget.domNode.style.height = Math.round(containerHeight * window.tabsHeightRatio) + 'px';

		containerWidget.previousHeight = containerHeight;
	}
}

function updateRootItem(itemNd) {
	if (!itemNd) {
		aras.AlertError('Failed to get the ' + itemTypeName, '', '');
		return false;
	}

	if (itemNd.getAttribute('type') != itemTypeName) {
		aras.AlertError('Invalid ItemType specified: (' + itemNd.getAttribute('type') + ').');
		return false;
	}

	var isLocked = (aras.isLockedByUser(itemNd) || aras.isTempEx(itemNd));
	item = itemNd;

	if (isEditMode !== isLocked) {
		if (isEditMode) {
			isEditMode = false;
		} else {
			isEditMode = true;
			setEditMode();
		}
	}
	refreshMenuState();
	return true;
}

function turnWindowReadyOn() {
	//that script will set windowReady = true and cause menuUpdate
	window.windowReady = true;
	if (window.updateMenuState) {
		window.updateMenuState = function() {};
	}

}

function setTitle(isEditMode) {
	var typeId = item.getAttribute('typeId');
	var itemTypeNd = aras.getItemTypeDictionary(aras.getItemTypeName(typeId)).node;
	var label = aras.getItemProperty(itemTypeNd, 'label');
	if (!label) {
		label = aras.getItemProperty(itemTypeNd, 'name');
	}
	var tmpKey = (isEditMode ? 'ui_methods_ex.itemtype_label_item_keyed_name' : 'ui_methods_ex.itemtype_label_item_keyed_name_readonly');
	var keyedName = aras.getKeyedNameEx(item);
	document.title = aras.getResource('', tmpKey, label, keyedName);
}

function refreshMenuState() {
	if (!menuFrame) {
		menuFrame = parent.tearOffMenuController;
	}

	var statusId = aras.showStatusMessage('status', aras.getResource('', 'common.updating_menu'), '');
	var isTemp = aras.isTempEx(item);
	menuFrame.setControlEnabled('new', aras.isNew(item));
	menuFrame.setControlEnabled('edit', false);
	menuFrame.setControlEnabled('view', false);
	menuFrame.setControlEnabled('print', false);
	menuFrame.setControlEnabled('lock', !isEditMode && !aras.isLocked(item));
	menuFrame.setControlEnabled('delete', !isEditMode || isTemp);
	menuFrame.setControlEnabled('save', isEditMode);
	menuFrame.setControlEnabled('save_unlock_close', isEditMode);
	menuFrame.setControlEnabled('unlock', (isEditMode || aras.getLoginName().search(/^admin$|^root$/) === 0 && aras.isLocked(item)) && !isTemp);
	menuFrame.setControlEnabled('undo', false);

	if (menuFrame.populateAccessMenuLazyStart) {
		menuFrame.populateAccessMenuLazyStart(isEditMode, currContentTypeNode);
	}

	aras.clearStatusMessage(statusId);
	var topWindow = aras.getMostTopWindowWithAras(window);
	if (topWindow.cui) {
		topWindow.cui.callInitHandlers('UpdateTearOffWindowState');
	}
}

document.addEventListener('loadWidgets', function() {
	var centerContainerWidget = dijit.byId('CenterBorderContainer');
	if (centerContainerWidget) {
		var formSplitter = centerContainerWidget.getSplitter('top');
		if (formSplitter) {
			window.borderContainerWidget = dijit.byId('BorderContainer');
			window.addEventListener('resize', resizeHandler, false);
		}
	}

	try {
		if (window.aras) {
			window.aras = new Aras(window.aras.findMainArasObject());
		}
	} catch (e) { }

	turnWindowReadyOn();
});

///////// END Default Windows Functions /////////

var containerWidget;
var formsPaneWidget;
var tabsPaneWidget;

onload = function onloadHandler() {
	window.aras = new Aras(window.aras);
	var centerDiv = document.getElementById('center');
	centerDiv.style.height = '100%';
	containerWidget = dijit.byId('view-container');
	formsPaneWidget = dijit.byId('forms');
	tabsPaneWidget = dijit.byId('relationshipsPane');
	document.addEventListener('loadWidgets', refreshMenuState);
	loadCmfAdminPanel(); // see cmfAdminPanel.js
	aras.browserHelper.toggleSpinner(document, false);
	return;
};
