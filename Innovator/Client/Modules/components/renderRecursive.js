﻿import SvgManager from '../core/SvgManager';
import trimSeparators from './trimSeparators';

const iconClass = 'aras-list-item__icon';

function calcSubmenuPosition(event) {
	const targetElement = event.target;
	const childList = targetElement.querySelector('ul.aras-list');
	childList.classList.remove('aras-list_left-sided');
	childList.style.right = '';
	childList.style.left = '';

	const docWidth = document.documentElement.clientWidth;
	const parentList = targetElement.parentNode;
	const parentListInfo = parentList.getBoundingClientRect();
	const childListWidth = childList.offsetWidth;
	const rightSideWidth = docWidth - parentListInfo.right;

	if (rightSideWidth > childListWidth) {
		return;
	} else if (parentListInfo.left > childListWidth) {
		childList.classList.add('aras-list_left-sided');
		return;
	}

	const isRightLargerLeft = parentListInfo.left < rightSideWidth;
	const childListInfo = isRightLargerLeft
		? {
				delta: childListWidth - rightSideWidth,
				calcSide: 'left',
				autoSide: 'right'
		  }
		: {
				delta: childListWidth - parentListInfo.left,
				calcSide: 'right',
				autoSide: 'left'
		  };

	childList.style[childListInfo.autoSide] = 'auto';
	childList.style[childListInfo.calcSide] =
		'calc(100% - ' + childListInfo.delta + 'px)';
}

const eventHandler = event => {
	const node = event.target;
	const targetNode = node.closest('li[data-index]');
	if (!targetNode) {
		event.stopPropagation();
		return;
	}

	const isDisabled = targetNode.classList.contains('aras-list-item_disabled');
	const isSeparator = targetNode.classList.contains('separator');
	if (isDisabled || isSeparator) {
		event.stopPropagation();
	}
};

function renderRecursive(data, itemIds) {
	const iconsRequired = itemIds.some(element => {
		const item = data.get(element);
		return Boolean(item.icon || item.image);
	});

	const checkedRequired = itemIds.some(element => {
		const item = data.get(element);
		return item.checked !== undefined || item.type === 'checkbox';
	});

	const arrowsRequired = itemIds.some(element => {
		const item = data.get(element);
		return item.children;
	});

	const shortcutsInfo = itemIds.reduce(
		(result, id) => {
			const currentShortcut = data.get(id).shortcut;
			if (!currentShortcut) {
				return result;
			}

			if (!result.shortcutsRequired) {
				result.shortcutsRequired = true;
			}

			if (currentShortcut.length > result.longestShortcut.length) {
				result.longestShortcut = currentShortcut;
			}

			return result;
		},
		{
			shortcutsRequired: false,
			longestShortcut: ''
		}
	);

	itemIds = trimSeparators(data, itemIds);
	const items = itemIds.reduce(function(acc, value) {
		const item = data.get(value);
		if (item.hidden) {
			return acc;
		}

		const isSeparator = item.type === 'separator';
		let classList = 'aras-list-item';
		let childList = !isSeparator
			? [
					<span className="aras-list-item__label">
						{item.label || aras.getResource('', 'common.no_label')}
					</span>
			  ]
			: [];
		if (item.disabled) {
			classList += ' aras-list-item_disabled';
		}
		if (isSeparator) {
			classList += ' separator';
		}
		if (item.children) {
			childList = childList.concat(renderRecursive(data, item.children));
			classList += ' aras-list__parent';
		}
		if (shortcutsInfo.shortcutsRequired && item.label) {
			childList = childList.concat(
				<span
					className={
						'aras-list-item__shortcut' +
						(item.shortcut ? '' : ' aras-list-item__shortcut_invisible')
					}
				>
					{item.shortcut || shortcutsInfo.longestShortcut}
				</span>
			);
		}
		if (arrowsRequired) {
			childList = childList.concat(
				<span
					className={
						'aras-list-item__arrow' +
						(item.children ? '' : ' aras-list-item__arrow_hidden')
					}
				/>
			);
			classList += ' aras-list-item_arrowed';
		}
		if (iconsRequired || checkedRequired) {
			classList += ' aras-list-item_iconed';
			let iconNode = SvgManager.createInfernoVNode(item.icon || item.image);
			if (!iconNode) {
				iconNode = <span />;
			}
			iconNode.className =
				iconClass + (item.checked ? ' aras-icon-checked' : '');
			childList.unshift(iconNode);
		}
		const props = {
			className: classList,
			'data-index': value,
			onMouseEnter: item.children && !item.disabled ? calcSubmenuPosition : null
		};
		if (item.cssStyle) {
			props.style = item.cssStyle;
		}

		acc.push(<li {...props}>{childList}</li>);
		return acc;
	}, []);

	if (!items.length) {
		return null;
	}

	return (
		<ul className="aras-list" onclick={eventHandler} onmousedown={eventHandler}>
			{items}
		</ul>
	);
}

export default renderRecursive;
