﻿import utils from '../../core/utils';

const infernoFlags = utils.infernoFlags;

export default {
	statusNode: data => {
		const contentNodes = [];
		const item = data.item;

		const pageSize = data.toolbar._prevPageSize || data.toolbar.pageSize;

		if (item.totalResults || item.totalResults === 0) {
			const totalResults = parseInt(item.totalResults) || 0;
			const totalPagesNumberNode = Inferno.createVNode(
				Inferno.getFlagsForElementVnode('span'),
				'span',
				'aras-pagination__total-pages',
				Inferno.createTextVNode(
					`/ ${
						pageSize && totalResults ? Math.ceil(totalResults / pageSize) : 1
					}`
				),
				infernoFlags.hasVNodeChildren
			);
			const totalResultsNumberNode = Inferno.createVNode(
				Inferno.getFlagsForElementVnode('span'),
				'span',
				'aras-pagination__total-results',
				Inferno.createTextVNode(
					aras.getResource('', 'pagination.results', item.totalResults)
				),
				infernoFlags.hasVNodeChildren
			);
			contentNodes.push(totalPagesNumberNode);
			contentNodes.push(totalResultsNumberNode);
		}

		contentNodes.unshift(
			Inferno.createTextVNode(
				aras.getResource('', 'pagination.page', item.currentPageNumber)
			)
		);

		const classNode =
			'aras-pagination__status' +
			(item.disabled ? ' aras-pagination__status_disabled' : '');

		return Inferno.createVNode(
			Inferno.getFlagsForElementVnode('span'),
			'span',
			classNode,
			contentNodes,
			infernoFlags.hasNonKeyedChildren,
			{
				'data-id': item.id,
				tabindex: '0'
			}
		);
	}
};
