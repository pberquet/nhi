﻿import intl from '../core/intl';
import SvgManager from '../core/SvgManager';

import renderRecursive from './renderRecursive';

const TOOLBAR_CLASS = 'aras-toolbar';
const TOOLBAR_ITEM_CLASS = 'aras-toolbar__item';
const DROPDOWN_CLASS = 'aras-toolbar__dropdown';
const DROPDOWN_BUTTON_CLASS =
	'aras-toolbar__dropdown-button aras-icon-arrow aras-icon-arrow_down';
const DROPDOWN_LIST_CONTAINER_CLASS = 'aras-dropdown';
const DROPDOWN_LIST_CLASS = 'aras-list';
const DIVIDER_CLASS = 'aras-toolbar__divider';

const BUTTON_CLASS = 'aras-button';
const SELECT_CLASS = 'aras-select';
const SELECT_CONTAINER_CLASS = 'aras-select__container';
const SEPARATOR_CLASS = 'aras-toolbar__separator';
const FORM_CLASS = 'aras-toolbar__form aras-form';
const SINGULAR_CLASS = 'aras-singular';
const SINGULAR_CONTAINER_CLASS = SINGULAR_CLASS + '__container';
const SINGULAR_BUTTON_CLASS = SINGULAR_CLASS + '__button';
const DISABLED_MODIFIER = '_disabled';
const CORPORATE_TIME_CLASS = 'aras-input aras-toolbar__corporate-time';
const TEXT_CLASS = 'aras-toolbar__text';
const IMAGE_CLASS = 'aras-toolbar__image';
const INPUT_CLASS = 'aras-input';
const INPUT_INVALID_CLASS = INPUT_CLASS + '_invalid';

const ItemConstructorWrapper = function(props) {
	const node = props.formatter(props);
	const propsItem = props.item;
	const itemCssStyle = propsItem.cssStyle || '';
	node.props = {
		...propsItem.attributes,
		...node.props
	};
	node.className = `${TOOLBAR_ITEM_CLASS} ${node.className ||
		''} ${propsItem.cssClass || ''}`;
	node.props.style = node.props.style
		? `${node.props.style} ${itemCssStyle}`
		: itemCssStyle;

	return node;
};

const defaultTemplates = {
	root: function(data) {
		const templatesObj = this;
		const leftContainerItems = this.items(
			data.leftContainerItems || [],
			templatesObj,
			data.options,
			data.toolbar
		);
		const rightContainerItems = this.items(
			data.rightContainerItems || [],
			templatesObj,
			data.options,
			data.toolbar
		);

		return (
			<div className={TOOLBAR_CLASS}>
				{[...leftContainerItems]}
				<div className={DIVIDER_CLASS} key="aras_toolbar_dividerNode" />
				{[...rightContainerItems]}
			</div>
		);
	},
	items: function(items, itemTemplates, options, toolbar) {
		const itemNodes = items.reduce(function(acc, item) {
			item = toolbar.data.get(item);
			if (!item.hidden && defaultFormatters[item.type]) {
				acc.push(
					<ItemConstructorWrapper
						key={item.id}
						toolbar={toolbar}
						item={item}
						formatter={defaultFormatters[item.type]}
						focused={toolbar.focusedItemId === item.id}
						itemRefCallback={options.itemRefCallback}
						ref={{ ...lifecycle }}
						isToolbarRole={options.isToolbarRole}
					/>
				);
			}

			return acc;
		}, []);
		return itemNodes;
	}
};

const getDisabledAttributeName = function(data) {
	return data.isToolbarRole ? 'aria-disabled' : 'disabled';
};

function CreateSelectNode(data) {
	return (
		<select
			onChange={data.changeHandler}
			title={data.tooltip_template}
			disabled={data.disabled}
			selectedIndex={data.selectedIndex}
			tabindex={data.tabindex}
		>
			{data.optionNodes}
		</select>
	);
}

const lifecycle = {
	onComponentShouldUpdate: function(lastProps, nextProps) {
		if (lastProps.item.type === 'separator') {
			return false;
		}
		return (
			lastProps.item !== nextProps.item ||
			lastProps.isToolbarRole !== nextProps.isToolbarRole ||
			lastProps.focused !== nextProps.focused
		);
	}
};

const CreateMenu = function(props) {
	const menuItemNodes = props.spinner ? (
		<div className="aras-spinner aras-dropdown-spinner" />
	) : props.data && props.data.size ? (
		renderRecursive(props.data, props.roots)
	) : (
		[]
	);

	return (
		<div
			className={DROPDOWN_LIST_CONTAINER_CLASS}
			onContextMenu={e => e.preventDefault()}
		>
			{menuItemNodes}
		</div>
	);
};

const getItemTabindex = function(data) {
	if (data.isToolbarRole) {
		return data.focused ? '0' : '-1';
	}

	return data.toolbar._isItemFocusable(data.item.id) ? '0' : '-1';
};

const defaultFormatters = {
	corporateTime: function(data) {
		const item = data.item;
		const currentDate = intl.date.parse(item.value);
		const date = intl.date.format(currentDate, 'longDate');
		const time = intl.date.format(currentDate, 'shortTime');
		const dateNodeId = `${item.id}_date`;
		const timeNodeId = `${item.id}_time`;
		const describedByAttribute = `${dateNodeId} ${timeNodeId}`;

		return (
			<span
				aria-label={item.aria_label || item.tooltip_template}
				aria-describedby={describedByAttribute}
				className={CORPORATE_TIME_CLASS}
				data-id={item.id}
				tabindex={getItemTabindex(data)}
				title={item.tooltip_template}
			>
				<span id={dateNodeId}>{date}</span>
				<span id={timeNodeId}>{time}</span>
			</span>
		);
	},
	button: function(data) {
		const contentNodes = [];
		const dataItem = data.item;
		const image = dataItem.image;
		let label = dataItem.label;

		if (image) {
			const iconNode = SvgManager.createInfernoVNode(image);

			iconNode.className =
				(iconNode.className ? iconNode.className + ' ' : '') +
				BUTTON_CLASS +
				'__icon';

			contentNodes.push(iconNode);
		} else if (!label) {
			label = aras.getResource('', 'common.no_label');
		}

		if (label) {
			contentNodes.push(
				<span className={BUTTON_CLASS + '__text'}>{label}</span>
			);
		}

		let nodeClass = BUTTON_CLASS;
		nodeClass += dataItem.state ? ' ' + BUTTON_CLASS + '_toggled-on' : '';
		const disabledAttribute = getDisabledAttributeName(data);
		const props = {
			tabindex: getItemTabindex(data),
			title: dataItem.tooltip_template,
			'data-id': dataItem.id,
			'aria-label': dataItem.aria_label,
			[disabledAttribute]: !!dataItem.disabled
		};
		if (dataItem.state !== undefined) {
			props['aria-pressed'] = Boolean(dataItem.state);
		}

		// Click handler attached only for enabled button with state property
		if (!dataItem.disabled && dataItem.state !== undefined) {
			const toggleButtonClick = function(event) {
				data.toolbar.data.set(
					dataItem.id,
					Object.assign({}, dataItem, { state: !dataItem.state })
				);
				data.toolbar.render();
			};

			if (dataItem.clickEventType === 'mousedown') {
				props.onmousedown = toggleButtonClick;
			} else {
				props.onclick = toggleButtonClick;
			}
		}
		return (
			<button {...props} className={nodeClass}>
				{contentNodes}
			</button>
		);
	},
	separator: function(data) {
		return <span className={SEPARATOR_CLASS} role="separator" />;
	},
	select: function(data) {
		const dataItem = data.item;
		const changeHandler = function(event) {
			data.toolbar.data.set(
				dataItem.id,
				Object.assign({}, data.item, {
					value: event.target.value
				})
			);
			data.toolbar.render();
		};
		const optionNodes = (dataItem.options || []).map(function(option) {
			return (
				<option value={option.value} selected={dataItem.value === option.value}>
					{option.label}
				</option>
			);
		});

		const selectNode = (
			<CreateSelectNode
				tabindex={getItemTabindex(data)}
				tooltip_template={dataItem.tooltip_template}
				selectedIndex={dataItem.selectedIndex}
				disabled={dataItem.disabled}
				optionNodes={optionNodes}
				changeHandler={changeHandler}
				ref={{
					onComponentDidMount: function(dom, data) {
						const selectedIndex = data.selectedIndex;
						if (
							selectedIndex !== undefined &&
							selectedIndex !== dom.selectedIndex
						) {
							dom.selectedIndex = selectedIndex;
						}
					}
				}}
			/>
		);

		return (
			<span
				className={
					SELECT_CLASS +
					(dataItem.disabled ? ' ' + SELECT_CLASS + DISABLED_MODIFIER : '')
				}
				data-id={dataItem.id}
			>
				{dataItem.label && (
					<span className={SELECT_CLASS + '__text'}>{dataItem.label}</span>
				)}
				<span className={SELECT_CONTAINER_CLASS}>
					{selectNode}
					<span className={SELECT_CLASS + '__button'} />
				</span>
			</span>
		);
	},
	date: function(data) {
		const changeHandler = function(event) {
			const id = data.item.id;
			data.toolbar.data.set(
				id,
				Object.assign({}, data.item, { value: event.target.value })
			);
			data.toolbar.render();
		};
		const item = data.item;
		const style = item.size ? { width: item.size } : {};
		Object.assign(style, item.disabled ? {} : { cursor: 'pointer' });

		const nodeClass =
			SINGULAR_CLASS +
			(item.disabled ? ` ${SINGULAR_CLASS + DISABLED_MODIFIER}` : '');
		const buttonClass =
			SINGULAR_BUTTON_CLASS + (item.disabled ? ' aras-icon_grayscale' : '');
		const iconNode = SvgManager.createInfernoVNode(
			'../images/InputCalendar.svg'
		);

		if (iconNode.className) {
			iconNode.className += ' aras-icon';
		} else {
			iconNode.className = 'aras-icon';
		}

		return (
			<span className={nodeClass} data-id={item.id}>
				<span className={SINGULAR_CONTAINER_CLASS}>
					<input
						type="text"
						onInput={changeHandler}
						defaultValue={item.value}
						disabled={item.disabled}
						style={style}
						tabindex={getItemTabindex(data)}
						title={item.tooltip_template}
						className="aras-input"
					/>
					<span className={buttonClass}>{iconNode}</span>
				</span>
			</span>
		);
	},
	singular: function(data) {
		const changeHandler = function(event) {
			const id = data.item.id;
			data.toolbar.data.set(
				id,
				Object.assign({}, data.item, { value: event.target.value })
			);
			data.toolbar.render();
		};

		const nodeClass =
			FORM_CLASS +
			(data.item.disabled ? ' ' + FORM_CLASS + DISABLED_MODIFIER : '');

		return (
			<span className={nodeClass} data-id={data.item.id}>
				<span className={SINGULAR_CLASS}>
					<input
						tabindex={getItemTabindex(data)}
						type="text"
						onInput={changeHandler}
						defaultValue={data.item.value}
						disabled={data.item.disabled}
					/>
					<span />
				</span>
			</span>
		);
	},
	textbox: function(data) {
		const item = data.item;
		const children = [];
		const changeHandler = function(event) {
			const id = item.id;
			data.toolbar.data.set(
				id,
				Object.assign({}, item, { value: event.target.value })
			);
			data.toolbar.render();
		};
		const style = data.item.size ? { width: data.item.size + 'rem' } : {};
		const textboxInput = (
			<input
				type="text"
				autoComplete="off"
				className={
					INPUT_CLASS + (item.invalid ? ' ' + INPUT_INVALID_CLASS : '')
				}
				value={data.item.value}
				title={data.item.tooltip_template}
				disabled={data.item.disabled}
				onInput={changeHandler}
				style={style}
				tabindex={getItemTabindex(data)}
			/>
		);
		const itemLabel = item.label;
		if (itemLabel) {
			children.push(
				<span className="aras-toolbar-component__label">{itemLabel}</span>
			);
		}
		if (item.image) {
			const iconNode = SvgManager.createInfernoVNode(item.image);
			if (iconNode.className) {
				iconNode.className += ' aras-input-icon';
			} else {
				iconNode.className = 'aras-input-icon';
			}
			children.push(
				<span className="aras-input-icon-wrapper">
					{textboxInput}
					{iconNode}
				</span>
			);
		} else {
			children.push(textboxInput);
		}
		const nodeClass = 'aras-toolbar__textbox aras-tooltip';
		const nodeAttributes = {
			'data-tooltip-show': item.invalid ? 'focuswithin' : false,
			'data-tooltip': aras.getResource(
				'',
				item.validationMessageResource || 'components.value_invalid'
			),
			'data-tooltip-pos': item.tooltip_pos || 'right',
			'data-focus-within': ''
		};
		return (
			<span className={nodeClass} data-id={data.item.id} {...nodeAttributes}>
				{children}
			</span>
		);
	},
	dropdownMenu: function(data) {
		const createButtonContent = function(item) {
			const contentNodes = [];

			if (item.image) {
				const iconNode = SvgManager.createInfernoVNode(item.image);

				iconNode.className = `${iconNode.className ||
					''} ${BUTTON_CLASS}__icon`;
				contentNodes.push(iconNode);
			}

			if (item.label) {
				contentNodes.push(
					<span className={BUTTON_CLASS + '__text'}>{item.label}</span>
				);
			}

			// Dropdown arrow node always rendered
			contentNodes.push(
				<span
					className={BUTTON_CLASS + '__icon ' + BUTTON_CLASS + '__menu-arrow'}
				/>
			);

			return contentNodes;
		};

		const item = data.item;
		const buttonFormatter = defaultFormatters[item.buttonFormatter];
		const nodeClass =
			BUTTON_CLASS + (item.buttonCssClass ? ' ' + item.buttonCssClass : '');

		const buttonRenderFunc = buttonFormatter || createButtonContent;
		let buttonContentNodes = buttonRenderFunc(item);
		const disabledAttribute = getDisabledAttributeName(data);
		const noItems = !item.data || !item.data.size;
		const buttonNodeAttributes = {
			'aria-label': item.aria_label || item.tooltip_template,
			tabindex: getItemTabindex(data),
			title: data.item.tooltip_template,
			[disabledAttribute]:
				data.item.disabled === undefined ? noItems : !!data.item.disabled,
			'dropdown-button': '',
			'aria-haspopup': 'menu'
		};

		if (buttonContentNodes) {
			buttonContentNodes = Array.isArray(buttonContentNodes)
				? buttonContentNodes
				: [buttonContentNodes];
		} else {
			buttonContentNodes = [];
		}

		return (
			<aras-dropdown
				className="aras-dropdown-container"
				data-id={item.id}
				position={item.menuPosition}
				closeonclick=""
			>
				<button className={nodeClass} {...buttonNodeAttributes}>
					{buttonContentNodes}
				</button>
				<CreateMenu
					id={item.id}
					data={item.data}
					roots={item.roots}
					spinner={item.spinner}
				/>
			</aras-dropdown>
		);
	},
	dropdownSelector: function(data) {
		const itemClickHandler = function(event) {
			const id = data.item.id;
			data.toolbar.data.set(
				id,
				Object.assign({}, data.item, {
					value: event.currentTarget.dataset.value
				})
			);
			const evt = new CustomEvent('change', { bubbles: true });
			event.currentTarget.dispatchEvent(evt);
			data.toolbar._doUpdateNodesInfo = true;
			data.toolbar.render();
		};
		const keys = Object.keys(data.item.options);
		const itemOptions = data.item.options;
		const checkedItemValue = itemOptions[data.item.value];
		const dropdownItemsNodes = (keys || []).map(function(key) {
			const itemValue = itemOptions[key];
			const iconClass =
				'aras-list-item__icon' +
				(itemValue === checkedItemValue ? ' aras-icon-radio' : '');
			return (
				<li onclick={itemClickHandler} data-value={key}>
					<div className={iconClass} />,
					<span className="aras-list__item-value">{itemValue}</span>
				</li>
			);
		});

		const nodeClass = 'aras-toolbar__dropdown-selector';
		return (
			<span
				className={nodeClass}
				data-id={data.item.id}
				title={data.item.tooltip_template}
			>
				<span className="dropdown-selector__label-wrapper">
					<span>{data.item.text}</span>
					{data.item.value}
				</span>
				<div className={DROPDOWN_CLASS}>
					<span
						className={DROPDOWN_BUTTON_CLASS}
						tabindex={getItemTabindex(data)}
					/>
					<div
						className={DROPDOWN_LIST_CONTAINER_CLASS}
						style={data.dropdownStyle || ''}
					>
						<ul className={DROPDOWN_LIST_CLASS}>{dropdownItemsNodes}</ul>
					</div>
				</div>
			</span>
		);
	},
	htmlSelector: function(data) {
		const itemClickHandler = function(event) {
			const id = data.item.id;
			data.toolbar.data.set(
				id,
				Object.assign({}, data.item, {
					value: event.currentTarget.dataset.value
				})
			);
			const evt = new CustomEvent('change', { bubbles: true });
			event.currentTarget
				.closest('.aras-toolbar__html-selector')
				.dispatchEvent(evt);
			data.toolbar.render();
		};

		const selectedItemValue = data.item.value;
		let selectedItemLabel = '';

		const dropdownItemsNodes = data.item.options.map(function(option) {
			if (option.type === 'separator') {
				return <li className="separator" />;
			}

			const itemLabel = option.label;
			const itemValue = option.value;
			selectedItemLabel =
				itemValue === selectedItemValue ? itemLabel : selectedItemLabel;

			return (
				<li onclick={itemClickHandler} data-value={itemValue}>
					{itemLabel}
				</li>
			);
		});

		return (
			<span className="aras-toolbar__html-selector" data-id={data.item.id}>
				<span className="aras-toolbar-component__label">{data.item.label}</span>
				<span
					className="aras-toolbar__html-selector__wrapper"
					title={data.item.tooltip_template}
				>
					<div className={DROPDOWN_CLASS} tabindex={getItemTabindex(data)}>
						<span className="html-selector__label-wrapper">
							{selectedItemLabel}
						</span>
						<span className={DROPDOWN_BUTTON_CLASS} />
					</div>
					<div
						className={DROPDOWN_LIST_CONTAINER_CLASS}
						style={data.dropdownStyle || ''}
					>
						<ul className={DROPDOWN_LIST_CLASS}>{dropdownItemsNodes}</ul>
					</div>
				</span>
			</span>
		);
	},
	dropdownButton: function(data) {
		const itemClickHandler = function(event) {
			const evt = new CustomEvent('dropDownItemClick', {
				bubbles: true,
				detail: { optionId: event.currentTarget.dataset.optionid }
			});
			event.currentTarget.dispatchEvent(evt);
		};

		const dropdownItemsNodes = data.item.options.map(option => {
			return (
				<li onclick={itemClickHandler} data-optionid={option.value}>
					{option.label}
				</li>
			);
		});

		return (
			<span
				className="aras-toolbar__action-dropdown-button"
				data-id={data.item.id}
			>
				<div className={DROPDOWN_CLASS} tabindex={getItemTabindex(data)}>
					<span className="action-dropdown-button__label-wrapper">
						{data.item.text}
					</span>
					<span className={DROPDOWN_BUTTON_CLASS} />
				</div>
				<div
					className={DROPDOWN_LIST_CONTAINER_CLASS}
					style={data.dropdownStyle || ''}
				>
					<ul className={DROPDOWN_LIST_CLASS}>{dropdownItemsNodes}</ul>
				</div>
			</span>
		);
	},
	image: function(data) {
		const imageClass = IMAGE_CLASS;
		return (
			<span
				className={imageClass}
				data-id={data.item.id}
				tabindex={getItemTabindex(data)}
			>
				{SvgManager.createInfernoVNode(data.item.image, {
					alt: data.item.tooltip_template
				})}
			</span>
		);
	},
	text: function(data) {
		const textClass = TEXT_CLASS;

		return (
			<span
				className={textClass}
				data-id={data.item.id}
				tabindex={getItemTabindex(data)}
				title={data.item.tooltip_template}
			>
				{data.item.label}
			</span>
		);
	}
};

export { defaultTemplates, defaultFormatters as toolbarFormatters };
