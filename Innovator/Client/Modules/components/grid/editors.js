﻿const startInputValidation = function(inputField, checkValidity, grid) {
	inputField.addEventListener('input', () => {
		const validationResult = checkValidity(inputField.value);
		if (validationResult.valid) {
			grid.settings.focusedCell = Object.assign(grid.settings.focusedCell, {
				valid: true,
				toolTipMessage: ''
			});
		} else {
			grid.dom.dispatchEvent(
				new CustomEvent('notValidEdit', {
					detail: { message: validationResult.validationMessage }
				})
			);
		}
	});
};

const getWillValidate = function(inputField, metadata) {
	let validate = metadata && metadata.willValidate;
	if (metadata && metadata.checkValidity) {
		const validationResult = metadata.checkValidity(inputField.value);
		validate = validationResult.valid
			? validate
			: () => Promise.reject(validationResult.validationMessage);
	}
	return validate;
};

const gridEditors = {
	text: function(dom, headId, rowId, value, grid, metadata) {
		const inputWrapper = document.createElement('div');
		inputWrapper.classList.add('aras-form-input__wrapper');

		const inputField = document.createElement('input');
		inputField.classList.add('aras-form-input');
		inputField.type = 'text';
		inputField.value = value || '';

		const checkValidity = metadata && metadata.checkValidity;
		if (checkValidity) {
			startInputValidation(inputField, checkValidity, grid);
		}

		const errorSpan = document.createElement('span');
		errorSpan.classList.add('aras-filter-list-icon');
		errorSpan.classList.add('aras-icon-error');

		dom.classList.add('aras-grid-active-cell__input', 'aras-form');
		inputWrapper.appendChild(inputField);
		inputWrapper.appendChild(errorSpan);
		dom.appendChild(inputWrapper);
		inputField.focus();

		return function() {
			return {
				value: inputField.value,
				willValidate: getWillValidate(inputField, metadata)
			};
		};
	},
	link: function(dom, headId, rowId, value, grid, metadata) {
		const inputField = document.createElement('input');
		inputField.type = 'text';
		inputField.value = value || '';

		const inputWrapper = document.createElement('div');
		inputWrapper.classList.add(
			'aras-form-singular',
			'aras-form-singular_light'
		);
		const errorSpan = document.createElement('span');
		errorSpan.classList.add('aras-filter-list-icon');
		errorSpan.classList.add('aras-icon-error');
		const span = document.createElement('span');
		span.classList.add('singular__button');
		if (metadata && metadata.editorClickHandler) {
			span.addEventListener('click', metadata.editorClickHandler);
			inputField.addEventListener('keydown', function(e) {
				if (e.key === 'F2') {
					metadata.editorClickHandler();
				}
			});
		}
		inputWrapper.appendChild(inputField);
		inputWrapper.appendChild(errorSpan);
		inputWrapper.appendChild(span);

		dom.classList.add('aras-grid-active-cell__singular', 'aras-form');
		dom.appendChild(inputWrapper);
		inputField.focus();

		return function() {
			return {
				value: inputField.value,
				willValidate: metadata && metadata.willValidate
			};
		};
	},
	calendar: function(dom, headId, rowId, value, grid, metadata) {
		const inputField = document.createElement('input');
		inputField.type = 'text';

		let format = metadata ? metadata.format : '';
		format = format.replace(/[A-Z]/g, function(match) {
			return '_' + match[0].toLowerCase();
		});

		const dateFormat = aras.getDateFormatByPattern(format || 'short_date');
		const dotNetPattern = aras.getDotNetDatePattern(dateFormat);

		const formattedDate = aras.convertFromNeutral(value, 'date', dotNetPattern);
		inputField.value = formattedDate || '';

		const inputWrapper = document.createElement('div');
		inputWrapper.classList.add('aras-form-date');
		const errorSpan = document.createElement('span');
		errorSpan.classList.add('aras-filter-list-icon');
		errorSpan.classList.add('aras-icon-error');
		const span = document.createElement('span');
		span.classList.add('date__button');
		if (metadata && metadata.editorClickHandler) {
			span.addEventListener('click', metadata.editorClickHandler);
			inputField.addEventListener('keydown', function(e) {
				if (e.key === 'F2') {
					metadata.editorClickHandler();
				}
			});
		}
		inputWrapper.appendChild(inputField);
		inputWrapper.appendChild(errorSpan);
		inputWrapper.appendChild(span);

		dom.classList.add('aras-grid-active-cell__calendar', 'aras-form');
		dom.appendChild(inputWrapper);
		inputField.focus();

		const checkValidity = metadata && metadata.checkValidity;
		if (checkValidity) {
			startInputValidation(inputField, checkValidity, grid);
		}

		return function() {
			return {
				value: aras.convertToNeutral(inputField.value, 'date', dotNetPattern),
				willValidate: getWillValidate(inputField, metadata)
			};
		};
	},
	select: function(dom, headId, rowId, value, grid, metadata) {
		const filterList = document.createElement('aras-filter-list');
		const validation = metadata && metadata.willValidate;
		filterList.setState({
			list: metadata && (metadata.options || metadata.list),
			value: value,
			validation: !validation,
			focus: true
		});

		dom.classList.add('aras-grid-active-cell__select');
		dom.appendChild(filterList);

		return function() {
			return {
				value: filterList.state.value,
				willValidate:
					validation ||
					(filterList.state.invalid
						? () => Promise.reject()
						: () => Promise.resolve(filterList.state.value))
			};
		};
	}
};

export default gridEditors;
