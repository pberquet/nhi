﻿import gridFormatters from './formatters';
import gridSearch from './search';
import utils from '../../core/utils';

const NULL_RELATED_ROW_ID = '00000000000000000000000000000000';

function gridTemplates(extension) {
	const infernoFlags = utils.infernoFlags;

	const templates = {
		gridHeadTemplate: function(children, style) {
			return Inferno.createVNode(
				Inferno.getFlagsForElementVnode('table'),
				'table',
				'aras-grid-head',
				children,
				infernoFlags.hasNonKeyedChildren,
				{ style: style }
			);
		},
		gridHeadRowTemplate: function(
			children,
			className,
			isKeyedChildren = false
		) {
			return Inferno.createVNode(
				Inferno.getFlagsForElementVnode('tr'),
				'tr',
				className,
				children,
				isKeyedChildren
					? infernoFlags.hasKeyedChildren
					: infernoFlags.hasNonKeyedChildren
			);
		},
		gridHeadCellTemplate: function(children, className, attributes) {
			return Inferno.createVNode(
				Inferno.getFlagsForElementVnode('th'),
				'th',
				'aras-grid-head-cell' + className,
				children,
				infernoFlags.hasNonKeyedChildren,
				attributes
			);
		},
		resizeTemplate: function() {
			return Inferno.createVNode(
				Inferno.getFlagsForElementVnode('div'),
				'div',
				'aras-grid-head-cell-resize'
			);
		},
		labelTemplate: function(children) {
			return Inferno.createVNode(
				Inferno.getFlagsForElementVnode('span'),
				'span',
				'aras-grid-head-cell-label',
				children,
				infernoFlags.hasNonKeyedChildren
			);
		},
		labelTextTemplate: function(children) {
			return Inferno.createVNode(
				Inferno.getFlagsForElementVnode('span'),
				'span',
				'aras-grid-head-cell-label-text',
				children,
				infernoFlags.hasVNodeChildren
			);
		},
		sortIconTemplate: function(className, children) {
			return Inferno.createVNode(
				Inferno.getFlagsForElementVnode('span'),
				'span',
				'aras-grid-head-cell-sort aras-icon-long-arrow ' + className,
				children,
				infernoFlags.hasVNodeChildren
			);
		},
		viewportTemplate: function(children, style) {
			return Inferno.createVNode(
				Inferno.getFlagsForElementVnode('table'),
				'table',
				'aras-grid-viewport',
				children,
				infernoFlags.hasNonKeyedChildren,
				{ style: style }
			);
		},
		colgroupTemplate: function(children) {
			return Inferno.createVNode(
				Inferno.getFlagsForElementVnode('colgroup'),
				'colgroup',
				null,
				children,
				infernoFlags.hasNonKeyedChildren
			);
		},
		colTemplate: function(props) {
			return Inferno.createVNode(
				Inferno.getFlagsForElementVnode('col'),
				'col',
				null,
				null,
				infernoFlags.hasInvalidChildren,
				props
			);
		},
		rowTemplate: function(children, className, index, attrs) {
			return Inferno.createVNode(
				Inferno.getFlagsForElementVnode('tr'),
				'tr',
				className,
				children,
				infernoFlags.hasNonKeyedChildren,
				{ 'data-index': index, 'aria-selected': attrs['aria-selected'] }
			);
		},

		getCellTemplate: function(grid, row, headId, value) {
			const type = grid._getCellType(headId.id, row.id, value, grid);
			const cellMetadata = grid.getCellMetadata(headId.id, row.id, type);
			const formatter = gridFormatters[type];

			return formatter
				? formatter(headId.id, row.id, value, grid, cellMetadata)
				: {};
		},
		buildCell: function(props) {
			const template = Object.assign(
				{
					tag: 'td',
					children: [props.value]
				},
				props.template
			);

			template.className = props.className + ' ' + (template.className || '');

			return utils.templateToVNode(template);
		},
		getRowClasses: function(grid, row) {
			let classes = 'aras-grid-row';
			if (row.selected) {
				classes += ' aras-grid-row_selected';
			}
			if (row.hovered) {
				classes += ' aras-grid-row_hovered';
			}

			const customClasses = grid.getRowClasses(row.id);
			if (customClasses) {
				classes += ' ' + customClasses;
			}

			return classes;
		},
		buildRow: function(data) {
			const grid = data.grid;

			const cellsVN = data.head.map(function(head) {
				const rowInfo = { ...data.row };
				const currentItem = rowInfo.data;
				const linkProperty = head.data.linkProperty;
				if (linkProperty) {
					rowInfo.id = currentItem[linkProperty] || NULL_RELATED_ROW_ID;
					rowInfo.data = grid.rows._store.get(rowInfo.id) || {};
				}
				const rawValue = rowInfo.data[head.data.name || head.id];
				const value =
					rawValue === undefined || rawValue === null ? '' : rawValue;
				const template = templates.getCellTemplate(grid, rowInfo, head, value);
				const cellStyles = grid.getCellStyles(head.id, rowInfo.id);
				template.style = templates.mergeCellStyles(
					template,
					head.data,
					cellStyles
				);

				if (typeof template.style !== 'string') {
					template.style = Object.keys(template.style || {}).reduce(function(
						acc,
						style
					) {
						acc += style + ': ' + template.style[style] + ';';
						return acc;
					},
					'');
				}

				return templates.buildCell({
					value: value,
					template: template,
					className: 'aras-grid-row-cell'
				});
			});
			const attrs = {
				'aria-selected': data.row.selected ? true : null
			};
			return templates.rowTemplate(
				cellsVN,
				templates.getRowClasses(grid, data.row),
				data.row.index,
				attrs
			);
		},
		buildViewport: function(rows, head, style, defaults, grid) {
			const columnsVN = head.map(function(head) {
				return templates.colTemplate({
					width: head.data.width || defaults.headWidth
				});
			});

			const rowsVN = rows.map(function(row) {
				return templates.buildRow({
					row: row,
					head: head,
					defaults: defaults,
					grid: grid
				});
			});

			const viewportChildrenVN = [templates.colgroupTemplate(columnsVN)];
			return templates.viewportTemplate(
				viewportChildrenVN.concat(rowsVN),
				style
			);
		},
		buildHead: function(head, headStyle, defaults, grid) {
			const searchVN = [];
			const headVN = head.map(function(head) {
				const labelChildren = [
					templates.labelTextTemplate(
						Inferno.createTextVNode(head.data.label || '')
					)
				];
				const icon = head.data.icon;

				if (icon) {
					labelChildren.push(ArasModules.SvgManager.createInfernoVNode(icon));
				}
				const classNameSort =
					'aras-icon-long-arrow_' +
					(head.sort && head.sort.desc ? 'down' : 'up');
				labelChildren.push(
					templates.sortIconTemplate(
						classNameSort,
						Inferno.createTextVNode(
							head.sort && grid.settings.orderBy.length > 1
								? head.sort.index
								: ''
						)
					)
				);

				const children = [templates.labelTemplate(labelChildren)];

				if (defaults.resizable && head.data.resizable !== false) {
					children.unshift(templates.resizeTemplate());
				}

				const style = {
					width: `${head.data.width || defaults.headWidth}px`
				};

				if (defaults.search) {
					const search = gridSearch[head.data.searchType || 'text'];
					const cellMetadata = grid.getCellMetadata(
						head.id,
						'searchRow',
						head.data.searchType || 'text'
					);
					const template = search
						? search(
								head.data,
								head.id,
								head.data.searchValue || '',
								grid,
								cellMetadata
						  )
						: {};
					template.key = head.id;
					template.attrs = {
						'data-index': head.index
					};

					searchVN.push(
						templates.buildCell({
							template: template,
							className: 'aras-grid-search-row-cell'
						})
					);
				}

				const className = head.sort ? ' aras-grid-head-cell_selected' : '';
				const attributes = {
					style: style,
					'data-index': head.index
				};
				if (defaults.sortable) {
					let sortAttr = 'none';
					if (head.sort) {
						sortAttr = head.sort.desc ? 'descending' : 'ascending';
					}

					attributes['aria-sort'] = sortAttr;
				}

				return templates.gridHeadCellTemplate(children, className, attributes);
			});

			const headRowsVN = [templates.gridHeadRowTemplate(headVN)];

			if (defaults.search) {
				headRowsVN.push(templates.gridHeadRowTemplate(searchVN, null, true));
			}

			return templates.gridHeadTemplate(headRowsVN, headStyle);
		},
		mergeCellStyles: function(template, head, cellStyles) {
			if (!head.columnCssStyles && !cellStyles) {
				return template.style || {};
			}

			return Object.assign(
				{},
				head.columnCssStyles,
				template.style || {},
				cellStyles
			);
		}
	};

	if (extension) {
		Object.assign(extension, Object.assign(templates, extension));
	}

	return {
		buildViewport: templates.buildViewport,
		buildHead: templates.buildHead
	};
}

export default gridTemplates;
