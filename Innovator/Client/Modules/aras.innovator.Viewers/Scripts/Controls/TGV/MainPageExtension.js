﻿define(['dojo/_base/declare',
		'dojo/_base/connect',
		'./PathManager'],
	function(declare, connect, PathManager) {
		return declare([], {
			_pathManager: new PathManager(),
			_isOneCallOfFillTreeDone: false,

			constructor: function(args) {
				const customObject = parent.customObject;
				const self = this;
				const callback = function() {
					//TODO: change the alert.//AlertError or AlertWarning?
					alert('Select failed. Perhaps some items were deleted/versioned.');
				};
				self.threeDViewerSetOnNotFoundItemCallback(callback);
				if (customObject) {
					let eventHandler = connect.connect(customObject, 'onSelectItemOnModel', function(pathsStr) {
						self.threeDViewerSelectByPaths(pathsStr);
					});
					this._eventHandlers.push(eventHandler);
				}
			},

			threeDViewerSetOnNotFoundItemCallback: function(callback) {
				this._pathManager.setOnNotFoundItemCallback(callback);
			},

			threeDViewerSelectByPaths: function(pathsStr) {
				this._pathManager.selectByPaths(pathsStr);
			},

			threeDViewerGetPathsOfSelectedNode: function() {
				var paths = this._pathManager.getPathsOfSelectedNode();
				return paths;
			},

			threeDViewerSetSpecialQueryItemRefId: function(refId) {
				this._pathManager.setSpecialQueryItemRefId(refId);
			},

			_createEmptyTree: function() {
				this._pathManager.setMainPage(this);
				return this.inherited(arguments);
			},

			_fillTree: function() {
				var self = this;
				if (!this._isOneCallOfFillTreeDone) {
					this._isOneCallOfFillTreeDone = true;
					if (self._grid._customObject) {
						const eventHandler = connect.connect(self._grid, 'onSelectRow', this, function() {
							if (self._pathManager.isShowMoreLoadingInProgress) {
								return;
							}
							var pathsStr = self.threeDViewerGetPathsOfSelectedNode();
							self._grid._customObject.onSelectRowOnTgvTreeGrid(pathsStr);
						});
						self._eventHandlers.push(eventHandler);
					}

					this._pathManager.overrideTreeGridForCancelClick(this);

					this._grid.setPathManager(this._pathManager);
				}

				const promise = this.inherited(arguments);
				promise.then(function() {
					self._pathManager._callSelectByPathIfRequired();
				});
				return promise;
			},

			_onLinkClick: function() {
				const self = this;
				const promise = this.inherited(arguments);
				promise.then(function() {
					self._pathManager._callSelectByPathIfRequired();
				});
				return promise;
			},

			_modifyParameters: function() {
				//it's overriden to do nothing, because the button shouldn't be clickable (it's better disabled/not visible) in 3D Viewer TGV.
				// it was decided to show warning message,
				// because this functionality will be implemented/available only in next PI
				const resourceKey = 'modifyParametersMessage';
				const warningMessage = aras.getResource('../Modules/aras.innovator.Viewers/', resourceKey);
				aras.AlertWarning(warningMessage);
			},

			_getTreeGridControlPath: function() {
				return 'Viewers/TGV/ThreeDViewerTgvTreeGrid';
			},

			reload: function() {
				this._grid._customObject.refresh3DView();

				return this.inherited(arguments);
			},

			_getRequestParametersForTreeGridData: function() {
				const result = this.inherited(arguments);
				if (this._grid._customObject.isToSkipModelLoading()) {
					result.fetch = 1; //TGV doesn't have a logic to get only a header, so, we get minimum data for better performance.
				}
				return result;
			}
		});
	});

