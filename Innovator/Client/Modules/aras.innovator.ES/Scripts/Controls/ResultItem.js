define([
	'dojo',
	'dojo/_base/declare',
	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'dojo/_base/lang',
	'ES/Scripts/Classes/Utils',
	'dojo/text!./../../Views/Templates/ResultItem.html',
	'dojo/text!./../../Views/Templates/ResultItemHighlight.html',
	'dojo/text!./../../Views/Templates/ResultFileHighlight.html',
	'dojo/text!./../../Views/Templates/SubResultItem.html',
	'dojo/text!./../../Views/Templates/ResultItemUIProperty.html'
], function(dojo,
			declare,
			_WidgetBase,
			_TemplatedMixin,
			lang,
			Utils,
			resultItemTemplate,
			resultItemHighlightTemplate,
			resultFileHighlightTemplate,
			subResultItemTemplate,
			resultItemUIPropertyTemplate) {
	return declare([_WidgetBase, _TemplatedMixin], {
		_arasObj: null,
		_utils: null,

		_item: null,

		_modifiedInfoTextResource: null,
		_propertiesTextResource: null,
		_currentTextResource: null,
		_notCurrentTextResource: null,
		_showDetailsTextResource: null,
		_hideDetailsTextResource: null,
		_moreTextResource: null,
		_lessTextResource: null,

		templateString: '',
		baseClass: 'resultItem',

		constructor: function(args) {
			this._arasObj = args.arasObj;
			this._utils = new Utils({
				arasObj: this._arasObj
			});
			this._item = args.item;
			this._modifiedInfoTextResource = args.modifiedInfoTextResource;
			this._propertiesTextResource = args.propertiesTextResource;
			this._currentTextResource = args.currentTextResource;
			this._notCurrentTextResource = args.notCurrentTextResource;
			this._showDetailsTextResource = args.showDetailsTextResource;
			this._hideDetailsTextResource = args.hideDetailsTextResource;
			this._moreTextResource = args.moreTextResource;
			this._lessTextResource = args.lessTextResource;

			var type = this._item.getProperty('aes_doc_type', 'value');
			var keyedName = this._getHighlightContent(this._item, 'keyed_name');
			if (keyedName === '') {
				keyedName = this._item.getProperty('keyed_name', 'label');
				var highlightedFileShortKeyedName = this._getHighlightContent(this._item, 'filename_short');

				if (type === 'File' && highlightedFileShortKeyedName !== '') {
					var fileNameShort = this._item.getProperty('filename_short', 'label'); // Get short name

					if (keyedName.length > fileNameShort.length) {
						var extension = keyedName.substring(fileNameShort.length);
						keyedName = highlightedFileShortKeyedName.trim() + extension;
					}
				}
			}

			var templateProperties = {};
			templateProperties.iconPath = this._utils.getImageUrl(this._item.configurations.iconPath);
			templateProperties.itemTypeColor = this._item.configurations.itemTypeColor;
			templateProperties.type = type;
			templateProperties.id = this._item.getProperty('id', 'value');
			templateProperties.keyedName = keyedName;
			templateProperties.subTitle = this.getSubTitle(this._item);
			templateProperties.propertiesLabel = this._propertiesTextResource;
			templateProperties.highlightsMarkup = this._getHighlightsMarkup(this._item);
			templateProperties.childItemsMarkup = this._getChildItemsMarkup(this._item);
			templateProperties.moreTextResource = this._moreTextResource;

			// Details panel template properties
			templateProperties.toggleDetailsButtonTooltip = this._showDetailsTextResource;
			templateProperties.itemType = this._item.configurations.itemTypeSingularLabel;
			if (type !== 'File') {
				var state = this._item.getProperty('state', 'label');
				templateProperties.itemStateMarkup = lang.replace('<div class="itemState">{0}.{1}{2}</div>', [
					this._item.getProperty('major_rev', 'label'),
					this._item.getProperty('generation', 'label'),
					state ? ' — ' + state : ''
				]);
			} else {
				templateProperties.itemStateMarkup = '';
			}
			templateProperties.modifiedMessage = lang.replace(this._modifiedInfoTextResource, [
				this._utils.convertDateToCustomFormat(this._item.getProperty('modified_on', 'value')),
				this._item.getProperty('modified_by_id', 'label')
			]);
			var propsMarkup = '';
			var uiPropNames = Object.keys(this._item.properties).filter(
				function(propName) {
					return this._item.getProperty(propName, 'is_ui');
				}.bind(this)
			);
			var sortedUiPropNames = uiPropNames.sort(
				function(a, b) {
					var titleA = this._item.getProperty(a, 'title');
					var titleB = this._item.getProperty(b, 'title');
					return titleA.localeCompare(titleB, undefined, {numeric: true});
				}.bind(this)
			);
			sortedUiPropNames.forEach(
				function(propName) {
					propsMarkup += lang.replace(resultItemUIPropertyTemplate, {
						title: this._item.getProperty(propName, 'title'),
						value: this._item.getProperty(propName, 'label')
					});
				}.bind(this)
			);
			templateProperties.propsMarkup = propsMarkup;

			this.templateString = lang.replace(resultItemTemplate, templateProperties);
		},

		postCreate: function() {
			if (this._item.subItems.length > 1) {
				//We need to hide some elements and display 'More' link
				this.childItemsContainer.classList.add('childItemsContainerCollapsed');
				this.moreLinkContainer.classList.remove('hidden');
			}
		},

		/**
		 * Get highlight content by property name
		 *
		 * @param {object} item Item
		 * @param {string} name Property name
		 * @returns {string}
		 * @private
		 */
		_getHighlightContent: function(item, name) {
			var res = '';

			for (var i = 0; i < item.highlights.length; i++) {
				var highlight = item.highlights[i];
				if (highlight.name === name) {
					res = this._prepareHighlightContent(highlight.content);
				}
			}

			return res;
		},

		/**
		 * Get HTML markup of highlights
		 *
		 * @param {object} item
		 * @returns {string}
		 * @private
		 */
		_getHighlightsMarkup: function(item) {
			var self = this;
			var markup = '';

			item.highlights.forEach(function(highlight) {
				var highlightContent = self._prepareHighlightContent(highlight.content);
				var propertyTitle = item.getProperty(highlight.name, 'title');
				var propertyLabel = propertyTitle !== '' ? propertyTitle : highlight.name;

				if (item.getProperty('aes_doc_type', 'value') !== 'File') {
					markup += lang.replace(resultItemHighlightTemplate, [propertyLabel, highlightContent]);
				} else {
					if (propertyLabel === 'content') {
						markup += lang.replace(resultFileHighlightTemplate, [highlightContent]);
					}
				}
			});

			return markup;
		},

		/**
		 * Get subtitle of item
		 *
		 * @param {object} item
		 * @returns {string}
		 * @private
		 */
		getSubTitle: function(item) {
			var isRoot = item.getProperty('aes_root', 'value') === 'true';
			var subtitle = (isRoot) ? this._getHighlightContent(item, 'name') : '';

			if (subtitle === '') {
				subtitle = item.getProperty('name', 'label');
			}

			return subtitle.trim();
		},

		/**
		 * Get HTML markup of child items
		 *
		 * @param {object} item
		 * @returns {string}
		 * @private
		 */
		_getChildItemsMarkup: function(item) {
			var markup = '';
			var self = this;

			item.subItems.forEach(function(childItem) {
				var keyedLabel = childItem.getProperty('keyed_name', 'label');
				var keyedName = self._getHighlightContent(childItem, 'keyed_name');
				if (keyedName === '') { // Search object isn't full file name
					keyedName = self._getHighlightContent(childItem, 'filename_short');
					if (keyedName === '') { // Search object isn't short file name (without extension)
						keyedName = keyedLabel;
					} else {
						var fileShortName = childItem.getProperty('filename_short', 'label'); // Get short name
						if (keyedLabel.length > fileShortName.length) {
							keyedName = keyedName.trim() + keyedLabel.substring(fileShortName.length);
						}
					}
				}

				var itemStateMarkup = '';
				var type = childItem.getProperty('aes_doc_type', 'value');

				if (type !== 'File') {
					var state = childItem.getProperty('state', 'label');
					var isCurrent = childItem.getProperty('is_current', 'label');

					itemStateMarkup = lang.replace('<div class="itemState">{0}.{1}{2} — {3}</div>', [
						childItem.getProperty('major_rev', 'label'),
						childItem.getProperty('generation', 'label'),
						state ? ' — ' + state : '',
						(isCurrent === 'true') ? self._currentTextResource : self._notCurrentTextResource
					]);
				}

				markup += lang.replace(subResultItemTemplate, {
					title: keyedName,
					subTitle: self.getSubTitle(childItem),
					id: childItem.getProperty('id', 'value'),
					type: type,
					highlightsMarkup: self._getHighlightsMarkup(childItem),
					iconPath: self._utils.getImageUrl(childItem.configurations.iconPath),
					itemStateMarkup: itemStateMarkup
				});
			});

			return markup;
		},

		/**
		 * Prepare highlight content:
		 *  - escape special HTML characters
		 *  - replace pre and post highlight strings with &lt;b&gt; and &lt;/b&gt;
		 *
		 * @param {string} highlightContent Content of highlight
		 * @returns {string}
		 * @private
		 */
		_prepareHighlightContent: function(highlightContent) {
			var res = this._escapeHtml(highlightContent);

			res = res.replace(new RegExp(this._escapeRegExp('[es_start_selection]'), 'g'), '<b>');
			res = res.replace(new RegExp(this._escapeRegExp('[es_end_selection]'), 'g'), '</b>');

			return res;
		},

		/**
		 * Escape HTML special characters
		 *
		 * @param {string} htmlMarkup HTML markup
		 * @returns {string}
		 * @private
		 */
		_escapeHtml: function(htmlMarkup) {
			return htmlMarkup.replace(/&/g, '&amp;')
				.replace(/</g, '&lt;')
				.replace(/>/g, '&gt;')
				.replace(/"/g, '&quot;')
				.replace(/'/g, '&#039;');
		},

		/**
		 * Escape regular expression special characters
		 *
		 * @param {string} str Regular expression
		 * @returns {string}
		 * @private
		 */
		_escapeRegExp: function(str) {
			return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
		},

		/*------------------------------------------------------------------------------------------------------------*/
		//Event handlers

		_onResultItemClickEventHandler: function(ev) {
			var itemTypeName = ev.currentTarget.getAttribute('data-item-type');
			var itemId = ev.currentTarget.getAttribute('data-item-id');

			if (itemTypeName === 'File') {
				var fileUrl = this._arasObj.IomInnovator.getFileUrl(itemId, this.arasObj.Enums.UrlType.SecurityToken);

				var wnd = window.open('', '_blank');
				wnd.opener = null;
				wnd.location = fileUrl;
			} else {
				if (!this._utils.isNullOrUndefined(itemTypeName) && !this._utils.isNullOrUndefined(itemId)) {
					this._arasObj.uiShowItem(itemTypeName, itemId);
				}
			}
		},

		_onMoreLinkClickEventHandler: function() {
			if (this.childItemsContainer.classList.contains('childItemsContainerCollapsed')) {
				this.childItemsContainer.classList.remove('childItemsContainerCollapsed');
				this.moreLinkContainer.innerText = this._lessTextResource;
			} else {
				this.childItemsContainer.classList.add('childItemsContainerCollapsed');
				this.moreLinkContainer.innerText = this._moreTextResource;
			}
		},

		_onToggleDetailsButtonClickEventHandler: function() {
			if (this._resultItemContainer.classList.contains('detailsPanelVisible')) {
				this._resultItemContainer.classList.remove('detailsPanelVisible');
				this._toggleDetailsButton.title = this._showDetailsTextResource;
			} else {
				this._resultItemContainer.classList.add('detailsPanelVisible');
				this._toggleDetailsButton.title = this._hideDetailsTextResource;
			}
		}
	});
});
