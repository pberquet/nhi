﻿require([
	'ES/Scripts/Classes/Page/InternalReindex',
	'ES/Scripts/Classes/Utils'
], function(InternalReindex, Utils) {
	var aras = parent.parent.aras;
	var utils = new Utils({
		arasObj: aras
	});

	if (utils.isFeatureActivated()) {
		var InternalReindexPage = new InternalReindex({
			arasObj: aras
		});
		InternalReindexPage.init();
	} else {
		window.location = '../GetLicense.html';
	}
});
