﻿define(['SSVC/Scripts/Classes/ViewSettingsManager'],
	function(ViewSettingsManager) {
		const SSVCDataManager = (function() {
			function SSVCDataManager(aras) {
				this.aras = aras;
				this.defaultFilesForViewing = [];
				this.defaultSsvcFormViewSettings = {};
				// TODO: Hide in closure to have access to them just with getFilesForViewing and getSsvcFormViewSettings
				this.filesForViewing = this.defaultFilesForViewing;
				this.ssvcFormViewSettings = this.defaultSsvcFormViewSettings;
			}
			/**
			 * Update SSVC files for viewing and form view settings.
			 * Use @{link SSVCDataManager#getFilesForViewing} and @{link SSVCDataManager#getSsvcFormViewSettings} to get updated data.
			 * Method will set files for viewing to empty array if update files fails.
			 * Method will set SSVC form view settings to empty object if update settings fails.
			 *
			 * @param {Object} itemNode
			 * @return {Promise}
			 */
			SSVCDataManager.prototype.updateDataForItem = function(itemNode) {
				const _this = this;
				const itemId = itemNode.getAttribute('id');
				const itemTypeName = itemNode.getAttribute('type');
				const configId = this.aras.getItemProperty(itemNode, 'config_id');
				const isNew = this.aras.isNew(itemNode);
				const filesForViewing = Promise.resolve()
					.then(function() {
						return isNew ? _this.defaultFilesForViewing : _this.fetchFilesForViewing(itemTypeName, itemId);
					})
					.then(function(files) {
						return {
							success: true,
							result: files
						};
					})
					.catch(function(error) {
						return {
							success: false,
							result: error
						};
					});
				const ssvcFormViewSettings = this.fetchSsvcFormViewSettings(itemTypeName, itemId, configId)
					.then(function(settings) {
						return {
							success: true,
							result: settings
						};
					})
					.catch(function(error) {
						return {
							success: false,
							result: error
						};
					});
				return Promise.all([filesForViewing, ssvcFormViewSettings])
					.then(function(results) {
						const filesResult = results[0];
						const settingsResult = results[1];
						_this.filesForViewing = filesResult.success ? filesResult.result : _this.defaultFilesForViewing;
						_this.ssvcFormViewSettings = settingsResult.success ? settingsResult.result : _this.defaultSsvcFormViewSettings;
						if (!filesResult.success && !settingsResult.success) {
							throw new Error('Errors occurred during update data for SSVC.');
						} else if (!filesResult.success) {
							return Promise.reject(filesResult.result);
						} else if (!settingsResult.success) {
							return Promise.reject(settingsResult.result);
						} else {
							return;
						}
					});
			};
			SSVCDataManager.prototype.getFilesForViewing = function() {
				return this.filesForViewing;
			};
			SSVCDataManager.prototype.getSsvcFormViewSettings = function() {
				return this.ssvcFormViewSettings;
			};
			SSVCDataManager.prototype.fetchFilesForViewing = function(itemTypeName, itemId) {
				const getFilesForViewingRequestItem = ArasModules.jsonToXml({
					Item: {
						'@attrs': {
							type: 'Method',
							action: 'VC_GetFilesForViewingForItem'
						},
						itemTypeName: itemTypeName,
						itemId: itemId
					}
				});
				return ArasModules.soap(getFilesForViewingRequestItem, {async: true})
					.then(function(resNode) {
						return resNode.childNodes;
					});
			};
			SSVCDataManager.prototype.fetchSsvcFormViewSettings = function(itemTypeName, itemId, configId) {
				const settingsManager = new ViewSettingsManager({aras: this.aras});
				return settingsManager.getSsvcFormViewSettings({itemTypeName: itemTypeName, itemId: itemId, configId: configId});
			};
			return SSVCDataManager;
		}());
		return new SSVCDataManager(window.aras);
	});
