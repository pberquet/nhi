﻿const DEFAULT_COLUMN_WIDTH = 100;
const listSet = new Set(['list', 'filter list', 'color list', 'mv_list']);
const itemDataTypes = ['item', 'file', 'claim by'];
const labelWithPostfixTypes = [
	'date',
	'text',
	'image',
	'formatted text',
	'color'
];

export async function adaptRelationshipGridHeader(
	relationshipItemTypeId,
	userPreferences,
	relatedItemTypeId
) {
	const infoByPreferences = getGridHeaderInfoByPreferences(userPreferences);

	const gridColumsInfoByRelatedPromise = getGridHeaderInfoByItemType(
		relatedItemTypeId,
		{
			criteria: 'is_hidden2',
			postfix: '_R'
		}
	);

	const gridColumsInfoByRelationshipPromise = getGridHeaderInfoByItemType(
		relationshipItemTypeId,
		{
			criteria: 'is_hidden2',
			postfix: '_D'
		}
	);

	const [
		headInfoByRelationshipProperties,
		headInfoByRelatedProperties
	] = await Promise.all([
		gridColumsInfoByRelationshipPromise,
		gridColumsInfoByRelatedPromise
	]);

	let infoByItemType = headInfoByRelationshipProperties || {
		headMap: new Map(),
		columnsOrder: []
	};
	if (headInfoByRelatedProperties) {
		headInfoByRelatedProperties.headMap.forEach(head => {
			head.linkProperty = 'related_id';
		});

		infoByItemType = {
			headMap: new Map([
				...infoByItemType.headMap,
				...headInfoByRelatedProperties.headMap
			]),
			columnsOrder: [
				...infoByItemType.columnsOrder,
				...headInfoByRelatedProperties.columnsOrder
			],
			classStructure: new Map([
				...infoByItemType.classStructure,
				...headInfoByRelatedProperties.classStructure
			]),
			itemTypeName: infoByItemType.itemTypeName,
			linkItemTypeName: {
				related_id: headInfoByRelatedProperties.itemTypeName
			}
		};

		const claimByColumnData = getClaimedByColumnData();
		claimByColumnData.linkProperty = 'related_id';
		infoByItemType.columnsOrder.unshift('L');
		infoByItemType.headMap.set('L', claimByColumnData);
	}

	return await getFullGridHeaderInfo(infoByItemType, infoByPreferences);
};

export async function adaptGridHeader(itemTypeId, userPreferencesItem) {
	const infoByPreferences = getGridHeaderInfoByPreferences(userPreferencesItem);
	const infoByItemType = await getGridHeaderInfoByItemType(itemTypeId, {
		criteria: 'is_hidden',
		postfix: '_D'
	});

	infoByItemType.columnsOrder.unshift('L');
	infoByItemType.headMap.set('L', getClaimedByColumnData());

	return await getFullGridHeaderInfo(infoByItemType, infoByPreferences);
};

export function adaptGridRows(resultNode, options = {}) {
	const items = ArasModules.xmlToODataJsonAsCollection(resultNode);
	const headMap = options.headMap;
	const indexRows = [];

	const rowsMap = items.reduce((map, item) => {
		let currentItem;
		const clonedItem = { ...item };
		const uniqueId = clonedItem.id || aras.generateNewGUID();
		const newRow = {
			id: uniqueId
		};

		options.indexHead.forEach(columnName => {
			const dataType = headMap.get(columnName, 'dataType');
			const linkProperty = headMap.get(columnName, 'linkProperty');
			const propertyName = headMap.get(columnName, 'name') || columnName;
			const linkPropertyValue = clonedItem[linkProperty];
			currentItem = linkProperty
				? map.get(linkPropertyValue) || linkPropertyValue || {}
				: clonedItem;

			const propertyValue = currentItem[propertyName];
			if (
				itemDataTypes.includes(dataType) &&
				propertyValue &&
				typeof propertyValue === 'object'
			) {
				map.set(propertyValue.id, propertyValue);

				const action = propertyValue['@aras.action'];
				const discoverOnly = propertyValue['@aras.discover_only'];

				currentItem[`${propertyName}`] = propertyValue.id;
				if (action) {
					currentItem[`${propertyName}@aras.action`] = action;
				}
				if (discoverOnly) {
					currentItem[`${propertyName}@aras.discover_only`] = discoverOnly;
				}
			}

			const sort = headMap.get(columnName, 'sort');
			let cellValue = currentItem[propertyName] || '';
			if (sort === 'NUMERIC') {
				cellValue = cellValue ? parseFloat(cellValue) : '';
			} else if (sort === 'UBIGINT') {
				cellValue = cellValue ? bigInt(cellValue) : '';
			}
			newRow[columnName] = cellValue;

			if (linkProperty) {
				map.set(currentItem.id, currentItem);
				clonedItem[linkProperty] = currentItem.id;
			}
		});

		indexRows.push(uniqueId);
		return map.set(uniqueId, {
			...clonedItem,
			...newRow
		});
	}, new Map());

	return {
		rowsMap,
		indexRows
	};
};

const getFullGridHeaderInfo = async (infoByItemType, infoByPreferences) => {
	const fullInfo = { ...infoByItemType };

	const lists = await getHeadLists(fullInfo.headMap);
	infoByPreferences.indexHead.forEach(columnName => {
		fullInfo.headMap.get(columnName).width = infoByPreferences.widthMap.get(columnName) || DEFAULT_COLUMN_WIDTH;
	});
	fullInfo.indexHead = infoByPreferences.indexHead;
	fullInfo.metadata = {
		headLists: lists,
		classStructure: fullInfo.classStructure,
		itemTypeName: fullInfo.itemTypeName,
		linkItemTypeName: fullInfo.linkItemTypeName
	};

	return fullInfo;
};

const getClaimedByColumnData = () => {
	return {
		id: 'L',
		label: '',
		field: 'L',
		name: 'locked_by_id',
		columnCssStyles: {
			'text-align': 'center'
		},
		editable: false,
		searchType: 'dropDownIcon',
		icon: '../images/ClaimColumn.svg',
		layoutIndex: 0,
		dataType: 'claim by',
		width: 32,
		options: [
			{
				value: '',
				icon: '',
				label: 'Clear Criteria'
			},
			{
				value:
					'<img src="../images/ClaimOn.svg" style="margin-right: 4px; height: auto; width: auto; max-width: 20px; max-height: 20px;" />',
				icon: 'svg-claimon',
				label: 'Claimed By Me'
			},
			{
				value:
					'<img src="../images/ClaimOther.svg" style="margin-right: 4px; height: auto; width: auto; max-width: 20px; max-height: 20px;" />',
				icon: 'svg-claimother',
				label: 'Claimed By Others'
			},
			{
				value:
					'<img src="../images/ClaimAnyone.svg" style="margin-right: 4px; height: auto; width: auto; max-width: 20px; max-height: 20px;" />',
				icon: 'svg-claimanyone',
				label: 'Claimed By Anyone'
			}
		]
	};
};

const getHeadLists = async headers => {
	const listIds = [];
	const filterListIds = [];

	headers.forEach(head => {
		if (!listSet.has(head.dataType)) {
			return;
		}

		if (head.dataType === 'filter list') {
			filterListIds.push(head.dataSource);
			return;
		}

		listIds.push(head.dataSource);
	});

	return await getSeveralListsValues(listIds, filterListIds);
};

const getSeveralListsValues = async (listIds, filterListIds) => {
	const jsonLists = (await aras.MetadataCacheJson.GetList(
		listIds,
		filterListIds
	))
		.map(list => list.value[0])
		.reduce((acc, list) => {
			acc[list.id] = list.Value || list['Filter Value'] || [];
			return acc;
		}, {});

	return Object.keys(jsonLists).reduce((acc, listId) => {
		const listNodes = jsonLists[listId];
		acc[listId] = listNodes.reduce(
			(listInfo, listNode) => {
				listInfo.push({
					value: listNode.value,
					label: listNode.label || listNode.value,
					filter: listNode.filter || ''
				});
				return listInfo;
			},
			[
				{
					value: '',
					label: '',
					filter: ''
				}
			]
		);

		return acc;
	}, {});
};

const getGridHeaderInfoByItemType = async (itemTypeId, options = {}) => {
	if (!itemTypeId) {
		return null;
	}

	const itemType = await aras.MetadataCacheJson.GetItemType(itemTypeId, 'id');
	if (!itemType) {
		return null;
	}

	const classStructure = getItemTypeClassStrucrute(itemType.class_structure);
	const itemTypeVisibleProps = getVisiblePropsForItemType(
		itemType,
		options.criteria
	);
	const xProperties = getItemTypeXProperties(itemType);
	const visibleProps = itemTypeVisibleProps.concat(xProperties);

	const gridHeader = await buildGridHeader(
		visibleProps,
		itemType,
		options.postfix
	);

	return {
		...gridHeader,
		classStructure: new Map().set(itemType.name, classStructure),
		itemTypeName: itemType.name
	};
};

const getGridHeaderInfoByPreferences = userPreferencesItem => {
	const userPreferences = ArasModules.xmlToJson(userPreferencesItem.xml).Item;
	const columnsOrder = userPreferences['col_order'].split(';');
	const columnsWidth = userPreferences['col_widths'].split(';');

	const widthMap = columnsOrder.reduce(
		(acc, columnName, index) =>
			acc.set(columnName, parseInt(columnsWidth[index])),
		new Map()
	);

	return {
		indexHead: columnsOrder.filter(columnName => widthMap.get(columnName) > 0),
		widthMap: widthMap
	};
};

const parseRecursive = items => {
	items = Array.isArray(items) ? items : [items];

	return items.map(item => {
		return {
			label: item['@attrs'].name,
			children: item.class ? parseRecursive(item.class) : []
		};
	});
};

const getItemTypeClassStrucrute = classStructure => {
	if (!classStructure) {
		return [];
	}

	const rootItem = classStructure
		? ArasModules.xmlToJson(classStructure).class
		: {};
	return parseRecursive(rootItem.class || []);
};

const getLabel = (label, property) => {
	const labelWithPostfix = label + ' [...]';
	const dataType = property.data_type;
	const dataSource = property['data_source@aras.id'];

	if (labelWithPostfixTypes.includes(dataType)) {
		return labelWithPostfix;
	}

	return dataType === 'item' && dataSource ? labelWithPostfix : label;
};

const getEditableType = (dataType, dataSourceName, isForeign) => {
	switch (dataType) {
		case 'date':
			return 'dateTime';
		case 'mv_list':
			return 'CheckedMultiSelect';
		case 'list':
		case 'filter list':
		case 'color list':
			return 'FilterComboBox';
		case 'item': {
			if (dataSourceName === 'File') {
				return 'File';
			} else if (!isForeign) {
				return 'InputHelper';
			}
		}
	}

	return 'FIELD';
};

const getSortFormatLocaleInfo = (dataType, property) => {
	const resultObject = {
		locale: null,
		inputformat: undefined,
		sort: null
	};
	const locale = aras.getSessionContextLocale();
	switch (dataType) {
		case 'date': {
			const defaultPattern = 'MM/dd/yyyy';
			const format =
				aras.getDotNetDatePattern(property.pattern) || defaultPattern;

			resultObject.sort = 'DATE';
			resultObject.inputformat = format;
			resultObject.locale = locale;
			break;
		}
		case 'decimal': {
			const format = aras.getDecimalPattern(property.prec, property.scale);

			resultObject.sort = 'NUMERIC';
			resultObject.inputformat = format || null;
			resultObject.locale = locale;
			break;
		}
		case 'integer':
		case 'float': {
			resultObject.sort = 'NUMERIC';
			resultObject.locale = locale;
			break;
		}
		case 'ubigint':
		case 'global_version': {
			resultObject.sort = 'UBIGINT';
			resultObject.locale = locale;
			break;
		}
	}
	return resultObject;
};

const getDataSourceName = (dataType, dataSource) =>
	dataType === 'item' && dataSource ? aras.getItemTypeName(dataSource) : null;

const getSearchType = (dataType, dataSourceName, isForeign, name) => {
	switch (dataType) {
		case 'item': {
			return !isForeign && dataSourceName !== 'File' ? 'singular' : '';
		}
		case 'date':
			return 'date';
		case 'mv_list':
			return 'multiValueList';
		case 'list':
		case 'filter list':
		case 'color list':
			return 'filterList';
	}
	return name === 'classification' ? 'classification' : '';
};

const getColumnDataType = (type, dataSourceName) => {
	if (dataSourceName === 'File') {
		return 'file';
	}

	return type === 'boolean' ? 'bool' : type;
};

const buildGridHeader = async (visibleProperties, itemType, postfix) => {
	const headMap = new Map();
	const columnsOrder = [];

	const foreignProperties = visibleProperties.filter(
		property => property.data_type === 'foreign'
	);

	const promises = foreignProperties.map(foreignProperty =>
		getRealPropertyForForeignProperty(foreignProperty, itemType)
	);
	const sourceProperties = (await Promise.all(promises)).reduce(
		(acc, sourceProperty, index) =>
			acc.set(foreignProperties[index].id, sourceProperty),
		new Map()
	);

	const head = visibleProperties.reduce((acc, property, index) => {
		const propertyId = property.id;
		const name = property.name;
		const label = property.label;
		const headerLabel = getLabel(label || name, property);
		const fieldKey = name + postfix;
		const columnAlign = property.column_alignment || 'left';

		// we have to save isForeign value since on the next line property we are using can be changed to property of source item
		const isForeign = property.data_type === 'foreign';
		const sourceProperty = isForeign
			? sourceProperties.get(property.id)
			: property;

		const dataSource = sourceProperty['data_source@aras.id'];
		const nativeDataType = sourceProperty.data_type || '';
		const dataSourceName = getDataSourceName(nativeDataType, dataSource);
		const dataType = getColumnDataType(nativeDataType, dataSourceName);
		const editableType = getEditableType(dataType, dataSourceName, isForeign);
		const sortFormatLocaleObject = getSortFormatLocaleInfo(
			dataType,
			sourceProperty
		);

		const header = {
			name,
			id: propertyId,
			field: fieldKey,
			label: headerLabel,
			columnCssStyles: {
				'text-align': columnAlign
			},
			layoutIndex: index + 1,

			sort: sortFormatLocaleObject.sort,
			inputformat: sortFormatLocaleObject.inputformat,
			locale: sortFormatLocaleObject.locale,
			scale: sourceProperty.scale,
			pattern: sourceProperty.pattern,
			precision: sourceProperty.prec,

			editable: false, // L column is never editable; can be true for relship grid
			editableType: editableType,
			dataType: dataType,
			dataSource: dataSource,
			dataSourceName: dataSourceName,
			searchType: getSearchType(dataType, dataSourceName, isForeign, name)
		};

		acc.set(fieldKey, header);
		columnsOrder.push(fieldKey);

		return acc;
	}, headMap);

	return {
		headMap: head,
		columnsOrder: columnsOrder
	};
};

const getItemTypeXProperties = itemType => {
	const xItemTypeAllowedProperty = itemType.xItemTypeAllowedProperty || [];

	return xItemTypeAllowedProperty
		.filter(xProperty => xProperty.inactive !== '1')
		.map(xProperty => xProperty.related_id);
};

const getVisiblePropsForItemType = (currItemType, criteriaName) =>
	currItemType.Property.filter(property => property[criteriaName] === '0').sort(
		sortProperties
	);

const parseNumber = number =>
	number || number === 0 ? number : Number.POSITIVE_INFINITY;

const sortProperties = (propertyNode1, propertyNode2) => {
	let sortOrder1 = parseNumber(propertyNode1.sort_order);
	let sortOrder2 = parseNumber(propertyNode2.sort_order);

	if (sortOrder1 === sortOrder2) {
		sortOrder1 = propertyNode1.name;
		sortOrder2 = propertyNode2.name;
	}

	return sortOrder1 < sortOrder2 ? -1 : 1;
};

const getPropertyByCriteria = (itemType, criteriaName, criteriaValue) => {
	return itemType.Property.find(
		property => property[criteriaName] === criteriaValue
	);
};

const getRealPropertyForForeignProperty = async (
	foreignProperty,
	currentItemType
) => {
	/*
	returns modified version of specified foreignProperty with some properties get from source
	Property
	*/
	const sourceProperty = getPropertyByCriteria(
		currentItemType,
		'id',
		foreignProperty['data_source@aras.id']
	);
	const foreignItemType = await aras.getItemTypeDictionaryJson(
		sourceProperty['data_source@aras.id'],
		'id'
	);
	const resultProperty = getPropertyByCriteria(
		foreignItemType,
		'id',
		foreignProperty['foreign_property@aras.id']
	);

	return resultProperty.data_type === 'foreign'
		? await getRealPropertyForForeignProperty(resultProperty, foreignItemType)
		: resultProperty;
};
