﻿import CuiLayout from './CuiLayout';

export default class SearchViewCuiLayout extends CuiLayout {
	_getCuiControls() {
		return Promise.resolve([
			{
				control_type: 'ToolbarControl',
				id: 'searchViewTitleBar',
				additional_data: {
					cssClass: 'aras-titlebar',
					attributes: {
						role: 'heading',
						'aria-level': '1'
					}
				},
				name: 'SearchView.TitleBar',
				'location@keyed_name': 'SearchView.TitleBar'
			},
			{
				control_type: 'ToolbarControl',
				id: 'searchViewCommandBar',
				additional_data: {
					cssClass: 'aras-commandbar aras-commandbar_bordered'
				},
				name: 'SearchView.CommandBar',
				aria_label: aras.getResource('', 'aria_label.SearchView.CommandBar'),
				'location@keyed_name': 'SearchView.CommandBar'
			}
		]);
	}
}