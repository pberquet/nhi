﻿import CuiLayout from './CuiLayout';

export default class SearchDialogCuiLayout extends CuiLayout {
	_getCuiControls() {
		return Promise.resolve([
			{
				control_type: 'ToolbarControl',
				id: 'searchDialogTitleBar',
				additional_data: {
					cssClass: 'aras-titlebar',
					attributes: {
						role: 'heading',
						'aria-level': '1'
					}
				},
				name: 'SearchDialog.TitleBar',
				'location@keyed_name': 'SearchDialog.TitleBar'
			},
			{
				control_type: 'ToolbarControl',
				id: 'searchDialogCommandBar',
				additional_data: {
					cssClass: 'aras-commandbar'
				},
				name: 'SearchDialog.CommandBar',
				aria_label: aras.getResource('', 'aria_label.SearchDialog.CommandBar'),
				'location@keyed_name': 'SearchDialog.CommandBar'
			}
		]);
	}
}