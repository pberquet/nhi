﻿const effs_cuiFormatters = {
	effs_label: function(data) {
		let labelClass = 'effs-toolbar-label';
		labelClass += data.item.className ? ' ' + data.item.className : '';
		return Inferno.createVNode(
			Inferno.getFlagsForElementVnode('span'),
			'span',
			labelClass,
			Inferno.createTextVNode(data.item.text),
			ArasModules.utils.infernoFlags.hasVNodeChildren
		);
	},

	effs_item_field: function(data) {
		const itemFieldContainerChildren = [];
		const properties = {};
		const metadata = data.item.metadata;

		if (metadata) {
			properties.onchange = metadata.onChangeHandler;
			properties.onclick = metadata.onClickHandler;
			properties.onkeydown = metadata.onKeyDownHandler;
		}

		const addNodeCallback = function(itemPropertyElement) {
			itemPropertyElement.setState({
				itemType: data.item.itemtype
			});

			if (metadata && metadata.onAddElementNodeHandler) {
				metadata.onAddElementNodeHandler(itemPropertyElement);
			}
		};

		if (data.item.label) {
			const labelData = {
				item: {
					text: data.item.label,
					className: data.item.fieldItemLabelClassName
				}
			};
			itemFieldContainerChildren.push(effs_cuiFormatters.effs_label(labelData));
		}

		let itemFieldClass = 'effs-toolbar-item-field';
		itemFieldClass += data.item.fieldItemClassName ? ' ' + data.item.fieldItemClassName : '';
		itemFieldContainerChildren.push(Inferno.createVNode(
			Inferno.getFlagsForElementVnode('aras-item-property'),
			'aras-item-property',
			itemFieldClass,
			null,
			ArasModules.utils.infernoFlags.hasInvalidChildren,
			properties,
			null,
			addNodeCallback
		));

		return Inferno.createVNode(
			Inferno.getFlagsForElementVnode('span'),
			'span',
			data.item.fieldItemContainerClassName,
			itemFieldContainerChildren,
			ArasModules.utils.infernoFlags.hasNonKeyedChildren
		);
	}
};
