﻿import reducer from './reducer';
import initialState from './initialState';
import configureStore from './configureStore';

const store = configureStore(reducer, initialState);

window.store = store;
