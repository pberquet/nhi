﻿(function() {
	if (typeof window.CustomEvent !== 'function') {
		function CustomEvent(event, params) {
			params = params || {bubbles: false, cancelable: false, detail: undefined};
			var evt = document.createEvent('CustomEvent');
			evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
			return evt;
		}

		CustomEvent.prototype = window.Event.prototype;
		window.CustomEvent = CustomEvent;
	}
	if (typeof window.MouseEvent !== 'function') {
		window.MouseEvent = function(type, dict) {
			dict = dict || {};
			const event = document.createEvent('MouseEvents');
			event.initMouseEvent(
				type,
				typeof dict.bubbles == 'undefined' ? true : !!dict.bubbles,
				typeof dict.cancelable == 'undefined' ? false : !!dict.cancelable,
				dict.view || window,
				dict.detail | 0,
				dict.screenX | 0,
				dict.screenY | 0,
				dict.clientX | 0,
				dict.clientY | 0,
				!!dict.ctrlKey,
				!!dict.altKey,
				!!dict.shiftKey,
				!!dict.metaKey,
				dict.button | 0,
				dict.relatedTarget || null
			);
			return event;
		};
	}
})();
