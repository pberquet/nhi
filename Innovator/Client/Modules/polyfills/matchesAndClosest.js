(function() {
	if (!('matches' in Element.prototype)) {
		var div = document.createElement('div');
		var prefix = ['moz', 'webkit', 'ms', 'o'].filter(function(prefix) {
			return prefix + 'MatchesSelector' in div;
		})[0] + 'MatchesSelector';
		Element.prototype.matches = Element.prototype[prefix];
		SVGElement.prototype.matches = SVGElement.prototype[prefix];
		SVGElementInstance.prototype.matches = SVGElementInstance.prototype[prefix];
	}

	if (!('closest' in Element.prototype)) {
		SVGElement.prototype.closest =
		SVGElementInstance.prototype.closest =
		Element.prototype.closest = function(selector) {
			var node = (this.constructor === SVGElementInstance) ? this.correspondingUseElement : this;
			while (node && node.matches) {
				if (node.matches(selector)) {
					return node;
				}
				node = node.parentElement || node.parentNode;
			}
			return null;
		};
	}
}());
