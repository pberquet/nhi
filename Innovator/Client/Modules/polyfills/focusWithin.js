﻿(function() {
	try {
		window.document.querySelector(':focus-within');
	} catch(e) {
		const updateAttribute = function(node, value) {
			if (node) {
				node.setAttribute('data-focus-within', value);
			}
		};

		window.document.addEventListener('focusin', function(event) {
			updateAttribute(event.target.closest('[data-focus-within]'), true);
		}, true);
		window.document.addEventListener('focusout', function(event) {
			updateAttribute(event.target.closest('[data-focus-within="true"]'), '');
		}, true);
	}
})();
