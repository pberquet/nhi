﻿define([
	'dojo/_base/declare',
	'dojo/aspect',
	'dijit/popup',
	'Aras/Client/Controls/Experimental/ContextMenu',
	'GraphView/Scripts/GraphLayer',
	'GraphView/Scripts/GraphLayouts/GraphLayoutFactory',
	'GraphView/Scripts/GraphTooltip',
	'GraphView/Scripts/ExpandSettingsMenu',
	'Vendors/d3.min'
],
function(declare, aspect, popup, ContextMenu, GraphLayer, GraphLayoutFactory, GraphTooltip, ExpandSettingsMenu, d3) {
	return declare('GraphNavigation.GraphControl', null, {
		d3: null,
		expandSettingsMenu: null,
		tooltipControl: null,
		contextItemId: null,
		graphLayer: null,
		layoutFactory: null,

		constructor: function(initialParameters) {
			initialParameters = initialParameters || {};

			if (initialParameters.connectId) {
				this.containerNode = typeof initialParameters.connectId === 'string' ? document.getElementById(initialParameters.connectId) : initialParameters.connectId;

				this.d3 = d3;
				this.arasObject = initialParameters.arasObject;
				this.layoutFactory = new GraphLayoutFactory();
				this.zoomListener = this.d3.zoom()
					.scaleExtent([0.1, 2])
					.on('zoom', this._zoomHandler.bind(this));

				this._createDom();
				this._createContextMenu();
				this._createTooltip();
				this._createExpandSettingsMenu();
				this._attachDomEventListeners();

				this.graphLayer = new GraphLayer({
					d3Framework: this.d3,
					owner: this,
					graphLayout: this.layoutFactory.getLayoutInstance(initialParameters.graphLayout, {d3Framework: this.d3})
				});
			}
		},

		setLayoutType: function(layoutType) {
			let newGraphLayout = this.layoutFactory.getLayoutInstance(layoutType, {
				d3Framework: this.d3
			});

			this.graphLayer.setGraphLayout(newGraphLayout);
			this.graphLayer.invalidateLayer();
		},

		_createDom: function() {
			if (this.containerNode) {
				this.svgNode = this.d3.select(this.containerNode).append('svg');
				this.svgDefs = this.svgNode.append('defs');
				this.domNode = this.svgNode.append('g')
					.attr('class', 'ViewContent')
					.node();
			}
		},

		_attachDomEventListeners: function() {
			this.svgNode.on('mouseover', this.hideTooltip.bind(this));
			this.svgNode.on('click', this.hideExpandSettingsMenu.bind(this));

			this.d3.select(this.svgNode.node()).call(this.zoomListener)
				.on('.zoom',  null);
		},

		_createTooltip: function() {
			this.tooltipControl = new GraphTooltip({containerNode: this.containerNode});
		},

		_createExpandSettingsMenu: function() {
			this.expandSettingsMenu = new ExpandSettingsMenu({containerNode: this.containerNode});

			aspect.after(this.expandSettingsMenu, 'onApplyButtonClick', function(nodeData) {
				this.graphLayer.extendChildNodes(nodeData);
				this.graphLayer.expandChildNodes(nodeData);
				this.graphLayer.invalidateLayer();
			}.bind(this), true);
		},

		_createContextMenu: function() {
			this.contextMenu = new ContextMenu(this.containerNode);
			this.containerNode.oncontextmenu = this._showContextMenu.bind(this);
		},

		_initContextMenu: function(menuModel) {
			if (menuModel && menuModel.length) {
				this.contextMenu.addRange(menuModel);

				aspect.after(this.contextMenu, 'onItemClick', function(commandId, nodeData) {
					this.onMenuItemClick(commandId, nodeData && nodeData.data);
					this._hideContextMenu();
				}.bind(this), true);
				aspect.after(this.contextMenu.menu, 'onBlur', this._hideContextMenu.bind(this), true);
				aspect.after(this.contextMenu.menu, 'onKeyPress', function(keyEvent) {
					if (keyEvent.keyCode === 27) {
						this._hideContextMenu();
					}
				}.bind(this), true);
			}

			this.contextMenu.menuInited = true;
		},

		_showContextMenu: function(menuEvent) {
			let targetNode = menuEvent.target;
			let nodeData;
			do {
				nodeData = this.d3.select(targetNode).datum();
				targetNode = targetNode.parentNode;
			} while (!nodeData && targetNode);

			if (!this.contextMenu.menuInited) {
				let menuModel = this.onMenuInit(nodeData);

				this._initContextMenu(menuModel);
			}

			this.contextMenu.rowId = {data: nodeData};
			this.onMenuSetup(this.contextMenu);

			popup.open({
				popup: this.contextMenu.menu,
				x: menuEvent.clientX,
				y: menuEvent.clientY
			});

			this.hideTooltip();
			this.hideExpandSettingsMenu();
		},

		_hideContextMenu: function() {
			popup.close(this.contextMenu.menu);
		},

		showTooltip: function(targetItem, tooltipItems) {
			if (targetItem && !this.contextMenu.menu.isShowingNow && !this.expandSettingsMenu.isActive) {
				let tooltipData = this.tooltipControl.tooltipData || {};

				if (targetItem !== tooltipData.targetItem) {
					tooltipData.positionX = this.d3.event.pageX - this.containerNode.offsetLeft + this.containerNode.scrollLeft + 15;
					tooltipData.positionY = this.d3.event.pageY - this.containerNode.offsetTop + this.containerNode.scrollTop + 5;
					tooltipData.tooltipItems = tooltipItems;
					tooltipData.targetItem = targetItem;

					return this.tooltipControl.showTooltip(tooltipData);
				}
			} else {
				this.hideTooltip();
			}
		},

		hideTooltip: function() {
			this.tooltipControl.hideTooltip();
		},

		hideExpandSettingsMenu: function() {
			this.expandSettingsMenu.hidePopupWindow();
		},

		_zoomHandler: function() {
			let eventTransform = this.d3.event.transform;

			this.domNode.setAttribute('transform', eventTransform.toString());
			this.currentTransform = eventTransform;
		},

		onMenuItemClick: function(commandId, nodeId) {
		},

		onMenuInit: function(menuEvent, targetLayer, targetDataNode, menuItems) {
		},

		onMenuSetup: function(menuEvent, targetLayer, targetDataNode, menuItems) {
		},

		onItemExtend: function(nodeData) {
		},

		loadGraph: function(graphData) {
			let containerBounds = this.containerNode.getBoundingClientRect();

			this.graphLayer.setData(graphData);
			this.centerView(containerBounds);
		},

		setContextItemId: function(contextItemId) {
			if (contextItemId) {
				this.contextItemId = contextItemId;
			}
		},

		setTransform: function(scale, offsetX, offsetY) {
			let zoomTransform = this.d3.zoomTransform(this.svgNode.node());

			zoomTransform.k = scale;
			zoomTransform.x = offsetX;
			zoomTransform.y = offsetY;

			this.zoomListener.transform(this.svgNode, zoomTransform);
		},

		setGraphScale: function(newScale, containerBounds) {
			this._hideContextMenu();
			this.hideExpandSettingsMenu();

			if (newScale > 0) {
				this.zoomListener.scaleTo(this.svgNode, newScale);
				this.adjustViewBounds(containerBounds);
			}
		},

		getGraphScale: function() {
			let zoomTransform = this.d3.zoomTransform(this.svgNode.node());
			return zoomTransform.k;
		},

		expandNode: function(nodeData) {
			if (nodeData) {
				this.graphLayer.extendChildNodes(nodeData);
				this.graphLayer.expandChildNodes(nodeData);
				this.graphLayer.extendParentNodes(nodeData);
				this.graphLayer.expandParentNodes(nodeData);
				this.graphLayer.invalidateLayer(nodeData);
			}
		},

		collapseNode: function(nodeData) {
			if (nodeData) {
				this.graphLayer.collapseChildNodes(nodeData);
				this.graphLayer.collapseParentNodes(nodeData);
				this.graphLayer.invalidateLayer(nodeData);
			}
		},

		collapseConnector: function(connectorData) {
			if (connectorData) {
				this.graphLayer.collapseConnector(connectorData);
				this.graphLayer.invalidateLayer(connectorData.source);
			}
		},

		centerCINode: function() {
			let rootNode = this.graphLayer.getRootNode();

			this.centerNode(rootNode.domNode);
		},

		centerNode: function(targetNode) {
			if (targetNode) {
				if (this.containerNode.scrollHeight > this.containerNode.offsetHeight && this.containerNode.scrollWidth > this.containerNode.offsetWidth) {
					const boundingRect = targetNode.getBoundingClientRect();
					this.containerNode.scrollLeft += boundingRect.left - (this.containerNode.clientWidth - boundingRect.width) / 2;
					this.containerNode.scrollTop += boundingRect.top - (this.containerNode.clientHeight - boundingRect.height) / 2;
				} else {
					this.adjustViewBounds();
				}
			}
		},

		centerView: function(precalculatedContainerBounds) {
			let containerBounds = precalculatedContainerBounds || this.containerNode.getBoundingClientRect();
			let viewBounds = this.graphLayer.getActualLayerBounds();
			let requiredScale = Math.min(
				(containerBounds.width - 20) / (viewBounds.width + 300),
				(containerBounds.height - 20) / (viewBounds.height + 200)
			);

			this.setGraphScale(requiredScale, containerBounds);
		},

		adjustViewBounds: function(containerBounds) {
			containerBounds = containerBounds || {};

			let currentScale = this.currentTransform.k;
			let svgDomNode = this.svgNode.node();
			let viewBounds = this.graphLayer.getActualLayerBounds();

			let calculatedViewHeight = (viewBounds.height + 200) * currentScale;
			let calculatedViewWidth = (viewBounds.width + 300) * currentScale;
			let calculatedTopPosition = (Math.abs(viewBounds.y) + 100) * currentScale;
			let containerHeight = (containerBounds.height || this.containerNode.offsetHeight) - 20;
			let containerWidth = (containerBounds.width || this.containerNode.offsetWidth) - 20;

			let newWidth = Math.max(calculatedViewWidth, containerWidth);
			let newHeight = Math.max(calculatedViewHeight, containerHeight);

			svgDomNode.setAttribute('width', newWidth);
			svgDomNode.setAttribute('height', newHeight);

			if (calculatedViewHeight < containerHeight) {
				let viewCenterOffset = (calculatedTopPosition * 2 - calculatedViewHeight) / 2;

				this.setTransform(currentScale, (newWidth - (viewBounds.width + 2 * viewBounds.x) * currentScale) / 2, newHeight / 2 + viewCenterOffset);
			} else {
				this.setTransform(currentScale, (newWidth - (viewBounds.width + 2 * viewBounds.x) * currentScale) / 2, calculatedTopPosition);
			}
		}
	});
});
