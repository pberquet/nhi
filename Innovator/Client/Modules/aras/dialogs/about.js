﻿(function(externalParent) {
	if (!window.ArasModules.Dialog) {
		return;
	}

	const about = function() {
		const dialogTitle = aras.getResource(
			'',
			'aras_object.about_aras_innovator'
		);
		const data = aras.aboutData;
		const aboutDialog = new ArasModules.Dialog('html', {
			title: dialogTitle
		});

		const content = document.createElement('div');
		const logoContent = document.createElement('div');
		const logoImage = document.createElement('img');
		const version = document.createElement('p');
		const msBuild = version.cloneNode();
		const supportLink = version.cloneNode();
		const arasLink = version.cloneNode();
		const copyright = version.cloneNode();
		const okBtn = document.createElement('button');
		let link = document.createElement('a');
		logoImage.src = aras.getBaseURL('/images/aras-innovator.svg');
		logoContent.appendChild(logoImage);
		content.appendChild(logoContent);
		content.appendChild(version);
		content.appendChild(msBuild);
		content.appendChild(supportLink);
		content.appendChild(arasLink);
		content.appendChild(copyright);
		content.classList.add('aras-dialog_about__content');

		version.textContent =
			'Aras Innovator Version ' +
			data.version.revision +
			'  Build: ' +
			data.version.build;
		msBuild.textContent = 'MS Build Number: ' + data.msBuildNumber;
		link.href = link.textContent = 'http://www.aras.com/support/';
		link.target = '_blank';
		link.classList.add('aras-link');
		supportLink.textContent = 'For support please go to ';
		supportLink.appendChild(link);

		link = link.cloneNode(true);
		link.href = link.textContent = 'http://www.aras.com';
		arasLink.textContent = 'Visit us on-line at ';
		arasLink.appendChild(link);

		copyright.textContent = data.copyright;

		okBtn.autofocus = true;
		okBtn.classList.add('aras-btn', 'aras-btn-right');
		okBtn.textContent = aras.getResource('', 'common.ok');
		okBtn.addEventListener('click', aboutDialog.close.bind(aboutDialog));

		aboutDialog.dialogNode.appendChild(content);
		aboutDialog.dialogNode.appendChild(okBtn);
		aboutDialog.dialogNode.classList.add('aras-dialog_about');

		aboutDialog.show();

		return aboutDialog.promise;
	};

	externalParent.Dialogs = externalParent.Dialogs || {};
	externalParent.Dialogs.about = about;
	window.ArasCore = window.ArasCore || externalParent;
})(window.ArasCore || {});
