﻿import Dialog from '../../core/Dialog';
import getResourceManager from '../startup/getResourceManager';

const disabledCookies = () => {
	const resourceManager = getResourceManager('core');
	const title = resourceManager.getString(
		'system_requirements.failed_initialize_innovator'
	);
	const dialog = new Dialog('html', { title });

	dialog.dialogNode.classList.add(
		'aras-dialog-alert',
		'aras-dialog_disabled-cookies'
	);

	HyperHTMLElement.bind(dialog.contentNode)`
		<div class="aras-dialog-alert__container">
			<img src="${aras.getBaseURL(
				'/images/Error.svg'
			)}" class="aras-dialog-alert__img">
			<span class="aras-dialog-alert__text">${resourceManager.getString(
				'login.cookies_are_disabled_msg_html'
			)}</span>
		</div>`;

	dialog.dialogNode.addEventListener('cancel', event => {
		event.preventDefault();
	});

	dialog.show();

	return dialog.promise;
};

export default disabledCookies;
