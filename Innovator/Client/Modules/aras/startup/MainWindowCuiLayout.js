﻿import {
	contentTabId,
	getSidebarData,
	updateLockedStatus
} from '../../NavigationPanel/sidebarDataConverter';
import cuiContextMenu from '../../cui/cuiContextMenu';
import CuiLayout from '../../cui/CuiLayout';
import cuiMethods from '../../cui/cuiMethods';
import cuiToc from '../../cui/cuiToc';
import cuiToolbar from '../../cui/cuiToolbar';

const tocContextMenuLocation = 'PopupMenuMainWindowTOC';

export default class MainWindowCuiLayout extends CuiLayout {
	_initTocPromise = null;
	deferredLocations = ['PopupMenuSecondaryMenu'];
	navigationPanel = document.getElementById('navigationPanel');
	headerTabs = window.arasTabs;

	async init() {
		sessionStorage.setItem('defaultDState', 'defaultDState');

		const initHeader = this._initializeHeader();
		const initToc = this._initializeToc();
		const initContextMenu = this._initializeContextMenu(tocContextMenuLocation);
		const initHeaderTabsContextMenu = this._initializeHeaderTabsContextMenu();
		this._preloadDeferredLocations();

		const headerControl = await initHeader;
		this._registerHeaderHandlers(headerControl);

		await initToc;
		window.selectStartPage();
		this._registerTocHandlers();

		const contextMenu = await initContextMenu;
		this._registerNavPanelHandlers(contextMenu);

		await this._initializeSidebar();
		this._registerSidebarHandlers();

		const headerTabsContextMenu = await initHeaderTabsContextMenu;
		this._registerHeaderTabsContextMenuHandlers(headerTabsContextMenu);
	}

	updateCuiLayoutOnItemChange(itemTypeName) {
		const itemTypesRequiringCuiUpdate = aras.getMorphaeList(
			aras.getItemTypeForClient('CuiDependency', 'name').node
		);
		const needToCuiUpgrade = itemTypesRequiringCuiUpdate.some(({ name }) => {
			return name === itemTypeName;
		});
		if (needToCuiUpgrade) {
			this.observer.notify('reInitByCUIDependencies');
		}
	}

	_initializeContextMenu(location) {
		return cuiContextMenu(this.navigationPanel.popupMenu, location, {
			...this.options,
			isDashboard: () => true
		});
	}

	_initializeHeaderTabsContextMenu() {
		return cuiContextMenu(this.headerTabs.popupMenu, 'HeaderTabsContextMenu', {
			...this.options,
			tabs: []
		});
	}

	_registerHeaderTabsContextMenuHandlers(contextMenu) {
		const tabsHandlers = {
			closeTab: id => this.headerTabs.removeTab(id),
			closeTabs: ids => this.headerTabs.closeTabs(ids),
			closeOtherTabs: id => this.headerTabs.closeOtherTabs(id),
			openInTearOff: id => this.headerTabs.clickOpenInTearOff(id)
		};
		this.headerTabs.on('contextmenu', (tabId, event) => {
			event.preventDefault();
			const currentTarget = this.headerTabs.data.get(tabId);
			contextMenu.show(
				{ x: event.clientX, y: event.clientY },
				{
					tabs: [...this.headerTabs.tabs],
					currentTarget: { ...currentTarget, id: tabId },
					...tabsHandlers
				}
			);
		});
	}

	async _initializeHeader() {
		const toolbarComponent = document.getElementById('headerCommandsBar');
		await cuiToolbar(toolbarComponent, 'MainWindowHeader', this.options);
		toolbarComponent.firstChild.classList.add('aras-header');

		return toolbarComponent;
	}

	_initializeSidebar() {
		const tabs = this.navigationPanel.tabs;
		const navData = this.navigationPanel.nav.data;
		const favoriteItemTypes = window.favorites.getDataMap('ItemType');
		const sidebarData = getSidebarData(favoriteItemTypes, navData);
		sidebarData.forEach((item, id) => {
			tabs.addTab(id, item);
		});
		tabs.selectTab(contentTabId);

		return tabs.render();
	}

	async _initializeToc() {
		if (this._initTocPromise) {
			return this._initTocPromise;
		}

		const nav = this.navigationPanel.nav;
		this._initTocPromise = cuiToc(nav, 'TOC', this.options);

		await this._initTocPromise;
		this._initTocPromise = null;

		return this._initTocPromise;
	}

	_registerNavPanelHandlers(contextMenu) {
		let activeLocation = tocContextMenuLocation;
		const contextMenus = {
			[activeLocation]: contextMenu
		};
		this.navigationPanel.addEventListener(
			'navigationPanelContextMenu',
			async event => {
				const { x, y, location, ...detail } = event.detail;
				detail.favorites = window.favorites;

				if (activeLocation !== location) {
					activeLocation = location;
					contextMenus[location] = await this._initializeContextMenu(location);
				}
				contextMenus[location].show({ x, y }, detail);
			}
		);
	}

	_registerHeaderHandlers(headerControl) {
		this.observer.subscribe(event => {
			switch (event) {
				case 'reInitByCUIDependencies':
					this._initializeHeader();
					break;
				case 'UpdatePreferences':
					cuiMethods.reinitControlItems(headerControl, event, this.options);
					break;
			}
		});
		aras.registerEventHandler('PreferenceValueChanged', window, () => {
			this.observer.notify('UpdatePreferences');
		});
	}

	_registerSidebarHandlers() {
		this.observer.subscribe(async event => {
			if (event !== 'UpdateTOC') {
				return;
			}

			await this._initializeToc();
			const nav = this.navigationPanel.nav;
			const tabs = this.navigationPanel.tabs;
			updateLockedStatus(tabs.data, nav.data);
			tabs.render();
		});
	}

	_registerTocHandlers() {
		this.observer.subscribe(async event => {
			if (event !== 'UpdateTOC') {
				return;
			}

			aras.MetadataCache.DeleteConfigurableUiDatesFromCache();
			await this._initializeToc();
			this.navigationPanel.render();
		});
	}
}
