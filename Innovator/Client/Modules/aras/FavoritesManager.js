﻿const soapConfig = {
	method: 'ApplyMethod',
	async: true
};
const cacheFavoriteItems = (dataMap, favoriteItemJson) => {
	const favoriteItems = JSON.parse(favoriteItemJson);
	favoriteItems.forEach(favoriteItem => {
		if (favoriteItem.id) {
			dataMap.set(favoriteItem.id, favoriteItem);
		}
	});
};
const sortFavoriteItems = ({ label = '' }, a) =>
	label.localeCompare(a.label || '');
const refreshUIComponents = () => {
	window.mainLayout.navigationPanel.render();
};
const sendSoap = (ArasModules, action, favorite) => {
	const item = { ...favorite };
	if (item.savedSearchItemAml) {
		item.savedSearchItem = ArasModules.xmlToJson(item.savedSearchItemAml);
		delete item.savedSearchItemAml;
	}

	if (item.additionalData) {
		item.additionalData = JSON.stringify(item.additionalData);
	}

	const requestXml = ArasModules.jsonToXml({
		Item: {
			'@attrs': {
				type: 'Method',
				action
			},
			...item
		}
	});

	return ArasModules.soap(requestXml, soapConfig);
};
const errorHandler = (ArasModules, error) => {
	const faultObj = ArasModules.aml.faultToJSON(error.responseXML);
	return ArasModules.notify(faultObj.faultstring, { type: 'error' });
};

class FavoritesManager {
	constructor(ArasModules, arasMainWindowInfo) {
		this.ArasModules = ArasModules;
		this.data = new Map();

		cacheFavoriteItems(this.data, arasMainWindowInfo.favoriteItems);
		cacheFavoriteItems(this.data, arasMainWindowInfo.favoriteItemTypes);
		cacheFavoriteItems(this.data, arasMainWindowInfo.favoriteSearches);
	}

	async add(category, favoriteData) {
		if (!category) {
			return null;
		}

		const item = {
			...favoriteData,
			category
		};

		const soapRequest = sendSoap(ArasModules, 'Fav_AddFavoriteItem', item);
		item.additional_data = item.additionalData || {};
		delete item.additionalData;

		const tempId = Symbol('favoriteId');
		this.data.set(tempId, item);
		refreshUIComponents();

		let favorite;
		try {
			const soapResult = await soapRequest;
			favorite = JSON.parse(soapResult.text);
			this.data.set(favorite.id, favorite);
		} catch (error) {
			favorite = null;
			errorHandler(this.ArasModules, error);
		} finally {
			this.data.delete(tempId);
			refreshUIComponents();
		}

		return favorite;
	}
	async delete(favoriteId) {
		if (!favoriteId) {
			return false;
		}
		const soapRequest = sendSoap(ArasModules, 'Fav_DeleteFavoriteItem', {
			id: favoriteId
		});
		const item = this.data.get(favoriteId);
		this.data.delete(favoriteId);
		refreshUIComponents();

		try {
			await soapRequest;
			return true;
		} catch (error) {
			errorHandler(this.ArasModules, error);
			this.data.set(favoriteId, item);
			refreshUIComponents();
			return false;
		}
	}
	async removeFromQuickAccess(favoriteId) {
		if (!favoriteId) {
			return null;
		}

		const storedFavoriteItem = this.data.get(favoriteId);
		if (storedFavoriteItem && storedFavoriteItem.quickAccessFlag === '0') {
			return null;
		}

		const soapRequest = this.ArasModules.soap(
			`<Item type="Favorite" action="edit" id="${favoriteId}">
				<quick_access_flag>0</quick_access_flag>
			</Item>`,
			{ async: true }
		);
		const updatedFavoriteItem = {
			...storedFavoriteItem,
			quickAccessFlag: '0'
		};
		this.data.set(favoriteId, updatedFavoriteItem);
		refreshUIComponents();

		try {
			await soapRequest;
			return updatedFavoriteItem;
		} catch (error) {
			errorHandler(this.ArasModules, error);
			this.data.set(favoriteId, storedFavoriteItem);
			refreshUIComponents();
			return null;
		}
	}
	get(favoriteId) {
		if (!favoriteId) {
			return null;
		}

		return this.data.get(favoriteId) || null;
	}
	getDataMap(favoriteCategory, filterCriteria) {
		if (!favoriteCategory) {
			return this.data;
		}

		filterCriteria = {
			category: favoriteCategory,
			...filterCriteria
		};

		const sortedItems = [];
		this.data.forEach(favoriteItem => {
			for (const criterion in filterCriteria) {
				if (favoriteItem[criterion] !== filterCriteria[criterion]) {
					return;
				}
			}

			sortedItems.push(favoriteItem);
		});

		const resultMap = new Map();
		sortedItems.sort(sortFavoriteItems).forEach(favoriteItem => {
			resultMap.set(favoriteItem.id, favoriteItem);
		});

		return resultMap;
	}
}

export default FavoritesManager;
