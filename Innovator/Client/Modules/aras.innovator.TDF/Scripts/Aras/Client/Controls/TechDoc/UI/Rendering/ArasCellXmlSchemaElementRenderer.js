﻿define([
	'dojo/_base/declare',
	'TechDoc/Aras/Client/Controls/TechDoc/UI/Rendering/XmlSchemaElementRenderer'
],
function(declare, XmlSchemaElementRenderer) {
	return declare('Aras.Client.Controls.TechDoc.UI.Rendering.ArasCellXmlSchemaElementRenderer', XmlSchemaElementRenderer, {

		RenderInnerContent: function(drawingItemsList, elementState) {
			var out = '';

			if (!drawingItemsList.length) {
				out += '\u0081';
			} else {
				var childItem;
				var rowItem;
				var blockRenderer;
				var blockList;
				var block;
				var startTagsString;
				var endTagsString;
				var i;
				var j;

				for (i = 0; i < drawingItemsList.length; i++) {
					childItem = drawingItemsList[i];
					rowItem = childItem.Parent.Parent;
					blockList = this._GetBlocksBetweenTableAndRow(rowItem);

					if (blockList.length) {
						blockRenderer = blockRenderer || this.factory.CreateRenderer(blockList[0]);
						startTagsString = '';
						endTagsString = '';

						for (j = 0; j < blockList.length; j++) {
							block = blockList[j];

							startTagsString += blockRenderer.RenderStartHtmlElement(block, elementState);
							endTagsString += blockRenderer.RenderEndHtmlElement(block, elementState);
						}

						out +=
							startTagsString +
							this.factory.CreateRenderer(childItem).RenderHtml(childItem, elementState) +
							endTagsString;
					} else {
						out += this.factory.CreateRenderer(childItem).RenderHtml(childItem, elementState);
					}
				}
			}

			return out;
		},

		RenderHtml: function(schemaElement, parentState) {
			var elementState = this.prepareElementState(schemaElement, parentState);
			var tableItem = schemaElement.GetTable();
			var cellAttributes = this.GetAttributes(schemaElement, elementState);
			var additionsClasses = '';
			var out = '';
			var isValidParent = schemaElement.Parent && schemaElement.Parent.is('ArasRowXmlSchemaElement');
			var childList;

			if (tableItem) {
				var drawingItemsContainer = tableItem.GetChildsOfCellForDrawing(schemaElement);
				var valign = schemaElement.Attribute('valign');
				var align = schemaElement.Attribute('align');

				if (drawingItemsContainer.height > 1) {
					cellAttributes.rowspan = drawingItemsContainer.height;
				}

				if (drawingItemsContainer.width > 1) {
					cellAttributes.colspan = drawingItemsContainer.width;
				}

				additionsClasses += valign ? ' valign_' + valign : '';
				additionsClasses += align ? ' align_' + align : '';
				childList = drawingItemsContainer.list;
			} else {
				childList = schemaElement.ChildItems().List();
				cellAttributes.width = (isValidParent ? (100 / schemaElement.Parent.ChildItems().length() | 0) : '100') + '%' ;
			}

			if (!this._IsHiddenCell(schemaElement)) {
				var tagName = isValidParent ? 'td' : 'div';
				// Validate if cell is merged to up or left cell
				out +=
					'<' + tagName + ' id="' + schemaElement.Id() + '" class="' +
					this.GetClassList(schemaElement, elementState).join(' ') + additionsClasses + '" ' +
					this._getAttributesStringArray(cellAttributes).join(' ') + '>' +
						this.RenderInnerContent(childList, elementState) +
					'</' + tagName + '>';
			}

			return out;
		},

		_IsHiddenCell: function(cellObject) {
			var tableObject = cellObject.GetTable();

			if (tableObject) {
				var rowObject = cellObject.Parent;
				var cellList = rowObject.ChildItems();
				var rowIndex = tableObject._GetRowIndex(rowObject);
				var cellIndex = cellList.index(cellObject);

				return (rowIndex !== null) ? tableObject._GetMergeMatrix().ValidateMergeDraw(rowIndex, cellIndex) : false;
			}

			return false;
		},

		_GetBlocksBetweenTableAndRow: function(rowObject) {
			var blocks = [];
			var parent = rowObject.Parent;

			while (parent && !parent.is('ArasTableXmlSchemaElement')) {
				blocks.push(parent);
				parent = parent.Parent;
			}

			return blocks.reverse();
		},

		GetTreeType: function(schemaElement, elementState) {
			return 'ArasCellXmlSchemaElementTreeNode';
		}
	});
});
