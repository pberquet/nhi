﻿define([
	'dojo/_base/declare'
],
function(declare) {
	return declare('Aras.Client.Controls.TechDoc.Action.ActionBase', null, {
		actionsHelper: null,
		_viewmodel: null,

		constructor: function(args) {
			this.actionsHelper = args.actionsHelper;
			this._viewmodel = this.actionsHelper.viewmodel;
			this.aras = this.actionsHelper.aras;
		},

		Execute: function(/*Object*/args) {

		},

		Validate: function(/*Object*/args) {
			return true;
		}
	});
});
