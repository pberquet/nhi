﻿define([
	'dojo/_base/declare',
	'TechDoc/Aras/Client/Controls/TechDoc/Action/ActionBase'
],
function(declare, ActionBase) {
	return declare('Aras.Client.Controls.TechDoc.Action.UngroupAction', ActionBase, {
		constructor: function(args) {
		},

		Execute: function(/*Object*/context) {
			var selectedBlock = context.selectedItems[0];
			var parentItem = selectedBlock.Parent;

			if (parentItem) {
				var childList = parentItem.ChildItems();
				var index = childList.index(selectedBlock);

				if (index > -1) {
					var selectedList = [];
					var removedBlock;
					var inseredList;
					var inseredItem;
					var itemsCount;
					var i;

					this._viewmodel.SuspendInvalidation();

					removedBlock = childList.splice(index, 1).get(0);
					inseredList = removedBlock.ChildItems();
					itemsCount = inseredList.length();

					for (i = 0; i < itemsCount; i++) {
						inseredItem = inseredList.get(i);

						childList.insertAt(i + index, inseredItem);
						selectedList.push(inseredItem);
					}

					this._viewmodel.SetSelectedItems(selectedList);
					this._viewmodel.ResumeInvalidation();
				}
			} else {
				this.aras.AlertError(this.aras.getResource('../Modules/aras.innovator.TDF', 'action.couldntapplyforrootblock'));
			}
		}
	});
});
