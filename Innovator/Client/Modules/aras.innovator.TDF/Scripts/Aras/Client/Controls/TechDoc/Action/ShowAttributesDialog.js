﻿define([
	'dojo/_base/declare',
	'TechDoc/Aras/Client/Controls/TechDoc/Action/ActionBase',
	'TechDoc/Aras/Client/Controls/TechDoc/ViewModel/DocumentationEnums'
],
function(declare, ActionBase, Enums) {
	return declare('Aras.Client.Controls.TechDoc.Action.ShowAttributesDialog', ActionBase, {
		typeSystemAttributes: null,

		constructor: function(args) {
			this.typeSystemAttributes = [
				{
					type: 'ArasImageXmlSchemaElement',
					systemAttributes: ['id', 'src', 'ref-id']
				},
				{
					type: 'ArasItemXmlSchemaElement',
					systemAttributes: ['id', 'ref-id', 'typeId']
				},
				{
					type: 'ArasBlockXmlSchemaElement',
					systemAttributes: ['by-reference', 'condition', 'id', 'ref-id']
				},
				{
					type: 'ArasListXmlSchemaElement',
					systemAttributes: ['type']
				},
				{
					type: 'ArasTableXmlSchemaElement',
					systemAttributes: ['MergeMatrix', 'ColWidth']
				},
				{
					type: 'ArasCellXmlSchemaElement',
					systemAttributes: ['valign', 'align']
				}
			];
		},

		isSystemAttibute: function(/*WrappedObject*/ targetElement, attributeName) {
			if (attributeName) {
				var typeData;
				var i;
				var j;

				for (i = 0; i < this.typeSystemAttributes.length; i++) {
					typeData = this.typeSystemAttributes[i];

					if (targetElement.is(typeData.type)) {
						for (j = 0; j < typeData.systemAttributes.length; j++) {
							if (typeData.systemAttributes[j] == attributeName) {
								return true;
							}
						}
					}
				}
			}

			return false;
		},

		Validate: function(/*Object*/context) {
			var targetElement = context.selectedItem;
			var attributesList = this._viewmodel.Schema().GetSchemaElementAttributes(targetElement);

			if (attributesList && attributesList.length) {
				var attribute;
				var i;

				for (i = 0; i < attributesList.length; i++) {
					attribute = attributesList[i];

					if (!this.isSystemAttibute(targetElement, attribute.Name)) {
						return true;
					}
				}
			}

			return false;
		},

		Execute: function(/*Object*/context) {
			var targetElement = context.selectedItem;
			var attributesList = this._viewmodel.Schema().GetSchemaElementAttributes(targetElement);

			if (attributesList) {
				var attributesListWithoutSystem = [];
				var param = {
					title: this.aras.getResource('../Modules/aras.innovator.TDF', 'action.attributesdialog'),
					aras: this.aras,
					isEditMode: true,
					isDisabled: !this._viewmodel.IsEqualEditableLevel(Enums.EditLevels.FullAllow),
					formId: '2B586AB158144FEB999E6812C9657647', // tp_EditAttributesDialog Form
					attrlist: attributesListWithoutSystem,
					wrappedObj: targetElement,
					dialogWidth: 375,
					dialogHeight: 410,
					content: 'ShowFormAsADialog.html',
				};
				var attribute;
				var i;

				for (i = 0; i < attributesList.length; i++) {
					attribute = attributesList[i];

					if (!this.isSystemAttibute(targetElement, attribute.Name)) {
						attributesListWithoutSystem.push(attribute);
					}
				}

				this.actionsHelper.topWindow.ArasModules.Dialog.show('iframe', param).promise
				.then(function(selectedValues) {
					if (selectedValues) {
						this._viewmodel.SuspendInvalidation();

						for (let attributeName in selectedValues) {
							targetElement.Attribute(attributeName, selectedValues[attributeName] || null);
						}

						this._viewmodel.ResumeInvalidation();
					}
				}.bind(this))
				.catch(function(exception) {
					this.aras.AlertError(exception);
				});
			}
		}
	});
});
