﻿define([
	'dojo/_base/declare',
	'TechDoc/Aras/Client/Controls/TechDoc/ViewModel/XmlSchemaExternalElement'
],
function(declare, XmlSchemaExternalElement) {
	return declare('Aras.Client.Controls.TechDoc.ViewModel.Aras.ArasItemXmlSchemaElement', XmlSchemaExternalElement, {
		_aras: null,

		constructor: function(args) {
			this._aras = this.ownerDocument._aras;
			this.referenceNodeName = 'aras:item';

			this.registerType('ArasItemXmlSchemaElement');
		},

		_parseOriginInternal: function() {
			this.inherited(arguments);

			this.internal.itemNode = this.OriginExternal().firstChild;
			this.Attribute('itemId', this.ItemId());
		},

		Item: function(value) {
			if (value === undefined) {
				return this.internal.itemNode;
			} else {
				var aras = this._aras;
				var referenceNode = this.OriginExternal();
				var isDiscoverOnly = value.getAttribute('discover_only') == '1';
				var itemNode = this.OriginExternal().ownerDocument.importNode(value, true);
				itemNode.setAttribute('xmlns', '');
				var referenceProperties = {
					isCurrent: aras.getItemProperty(value, 'is_current'),
					majorVersion: aras.getItemProperty(value, 'major_rev'),
					minorVersion: aras.getItemProperty(value, 'generation')
				};

				this.internal.itemNode = !isDiscoverOnly ? itemNode : itemNode.cloneNode(false);
				referenceNode.appendChild(this.internal.itemNode);
				this.Attribute('itemId', this.ItemId());

				if (referenceProperties.isCurrent !== '1') {
					// search for latest item version
					var latestItemVersion = aras.newIOMItem(aras.getItemProperty(value, 'type'), 'getItemLastVersion');

					latestItemVersion.setID(aras.getItemProperty(value, 'id'));
					latestItemVersion = latestItemVersion.apply();

					if (!latestItemVersion.isError()) {
						referenceProperties.latestVersionId = latestItemVersion.getID();
					}
				}

				this.internal.referenceProperties = referenceProperties;
				this.AttributeExternal('referenceProperties', JSON.stringify(referenceProperties));

				if (isDiscoverOnly) {
					this.AttributeExternal('isBlocked', 'true');
				}
			}
		},

		ItemId: function() {
			return !this.isEmpty() ? this.Item().getAttribute('id') : '';
		},

		ItemType: function() {
			return !this.isEmpty() ? this.Item().getAttribute('type') : '';
		},

		ItemTypeId: function() {
			return !this.isEmpty() ? this.Item().getAttribute('typeId') : '';
		},

		GetProperty: function(propname, defaultValue) {
			return !this.isEmpty() ? this._aras.getItemProperty(this.Item(), propname, defaultValue) : '';
		},

		isEmpty: function() {
			return !Boolean(this.internal.itemNode);
		}
	});
});
