﻿function BaseRelationshipsGridWrapper() {
}

BaseRelationshipsGridWrapper.prototype.createGridContainer = function() {
	var self = this;
	return new Promise(function(resolve) {
		clientControlsFactory.createControl('Aras.Client.Controls.Public.GridContainerWrapper', {
			onStartSearch_Experimental: doSearch,
			canEdit_Experimental: canEditCell,
			validateCell_Experimental: validateCell,
			customRowHeight: 32
		}, function(control) {
			grid = gridApplet = control;
			grid.setColumnTypeManager_Experimental('File', FilePropertyManager);
			gridReady = true;

			clientControlsFactory.on(grid.grid_Experimental, {
				onHeaderCellContextMenu: function(e) {
					onRelshipsHeaderCellContextMenu(e);
					grid.headerContexMenu_Experimental.show({
						x: e.clientX,
						y: e.clientY
					});
				},
				onHeaderContextMenu: onRelshipsHeaderContextMenu,
				onRowContextMenu: function(e) {
					const selectedRow = grid.grid_Experimental.getItem(e.rowIndex);
					const rowId = grid.grid_Experimental.store.getIdentity(selectedRow);
					grid.contexMenu_Experimental.show({
						x: e.clientX,
						y: e.clientY
					}, {
						selectedRow: rowId,
						colIndex: grid.contexMenu_Experimental.columnIndex,
						favorites: aras.getMainWindow().favorites
					});
				}
			});

			clientControlsFactory.on(grid, {
				'gridHeaderMenuClick_Experimental': onRelshipsHeaderMenuClicked,
				'gridDoubleClick': onDoubleClick,
				'gridXmlLoaded': onXmlLoaded,
				'gridClick': onSelectItem,
				'gridLinkClick': function(linkVal) {
					if (linkVal.length) {
						linkVal = linkVal.replace(/'/g, '');
						var typeName = linkVal.split(',')[0];
						var id = linkVal.split(',')[1];
						onLink(typeName, id);
					}
				},
				'onInputHelperShow_Experimental': relshipsGrid_showInputHelperDialog,
				'onStartEdit_Experimental': startCellEditRG,
				'onApplyEdit_Experimental': applyCellEditRG,
				'onCancelEdit_Experimental': onCancelEditHandler,
				'gridMenuInit': self.gridMenuInit,
				'gridMenuClick': function(cmdId, rowId, col) {
					return onRelationshipPopupMenuClicked(cmdId, rowId, col);
				},
				'gridKeyPress': onKeyPressed
			});

			clientControlsFactory.on(grid.items_Experimental, {
				'onNew': addNewRowEvent
			});

			require(['dojo/_base/connect'], function(connect) {
				const relationshipItemTypeId = aras.getItemProperty(RelType_Nd, 'relationship_id');
				const itemTypeDescriptor = aras.getItemTypeForClient(relationshipItemTypeId, 'id');
				window.cuiContextMenu(
					new ArasModules.ContextMenu(document.body),
					'ItemView.RelationshipsGridContextMenu',
					{
						itemTypeId: relationshipItemTypeId,
						itemTypeName: itemTypeDescriptor.getProperty('name'),
						item_classification: aras.getItemProperty(window.item, 'classification')
					}).
					then(function(contextMenu) {
						grid.contexMenu_Experimental = contextMenu;
					});

				grid.headerContexMenu_Experimental = new ContextMenuWrapper(document.body, true);
				grid.grid_Experimental.headerMenu = grid.headerContexMenu_Experimental.menu;
				cui.initPopupMenu(grid.headerContexMenu_Experimental);
				connect.connect(grid.headerContexMenu_Experimental, 'onItemClick', grid, function(command, rowID, columnIndex) {
					this.gridHeaderMenuClick_Experimental(command, rowID, columnIndex);
				});
				grid.headerContexMenu_Experimental.menu.on('click', function(commandId, e) {
					const colIndex = grid.headerContexMenu_Experimental.columnIndex;
					grid.headerContexMenu_Experimental.onItemClick(commandId, 'header_row', colIndex);
				}.bind(grid));
			});

			scriptInit();
			initSearch().then(function() {
				refreshGridSize();
				aras.registerEventHandler('ItemLock', window, relatedItemLockListener);
				aras.registerEventHandler('ItemSave', window, ItemSaveListener);

				//For Advanced search grid
				require(['Aras/Client/Controls/Public/GridContainer']);

				resolve();
			});
		});
	});
};

BaseRelationshipsGridWrapper.prototype.addRow = function(relationshipNode, relatedItemNode, markDirty) {
	if (!relationshipNode) {
		return;
	}

	if (relatedItemNode) {
		aras.setItemProperty(relationshipNode, 'related_id', relatedItemNode, false);
	}

	const itemId = relationshipNode.getAttribute('id');
	const rowsInfo = window.adaptGridRows('<Result>' + relationshipNode.xml + '</Result>', {
		headMap: grid._grid.head,
		indexHead: grid._grid.settings.indexHead
	});
	rowsInfo.rowsMap.forEach(function(row, key) {
		grid._grid.rows._store.set(key, row);
	});
	grid._grid.settings.indexRows.push(itemId);
	grid._grid.render();
	grid.items_Experimental.onNew(itemId);

	xml_ready_flag = false;
	addRowInProgress_Number++;
	const skipImmediateUpdate = true;

	//to support removeRelationship in onInsertRow event of RelationshipType
	if (!item.selectSingleNode('Relationships/Item[@id="' + itemId + '"]')) {
		grid.deleteRow(itemId, skipImmediateUpdate);
		if (editWait) {
			clearTimeout(editWait);
		}
	}
};
