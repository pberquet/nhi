﻿<!-- (c) Copyright by Aras Corporation, 2006-2011. -->
<!-- #INCLUDE FILE="utils.aspx" -->
<%
	Dim qs As String = Request.ServerVariables("QUERY_STRING")
	If Not String.IsNullOrEmpty(qs) Then
		qs = "?" + qs
	End If
	Dim clConfig As ClientConfig = ClientConfig.GetServerConfig()

	Dim actionNamespace As String = clConfig.ActionNamespace
	Dim serviceName As String = clConfig.ServiceName
	Dim serviceUrl As String = clConfig.ServiceUrl
	Dim oauthClientId As String = clConfig.oauthClientId

	Dim requestPath As String = Request.Url.AbsolutePath
	Dim requestDir As String = requestPath.Substring(0, requestPath.LastIndexOf("/"))
%>
	<script type="text/javascript">
		window.__staticVariablesStorage = true; //this is a flag for StaticVariablesStorage to initialize static variables storage here in this window.
	</script>
	<script defer type="text/javascript" src="../javascript/include.aspx?classes=StaticVariablesStorage"></script>
	<%
		Dim iomScriptSharp As String = If(HttpContext.Current.IsDebuggingEnabled, "IOM.ScriptSharp.debug", "IOM.ScriptSharp")
	%>
	<script type="text/javascript" src="../javascript/include.aspx?classes=ArasModulesWithoutCore"></script>
	<script src="../jsBundles/core.js"></script>
	<script type="text/javascript" src="../javascript/include.aspx?classes=MainWindow,pageReturnBlocker&files=<%=iomScriptSharp%>"></script>
	<script src="../javascript/include.aspx?classes=arasStub/dojo.js" data-dojo-config="async: true, isDebug: false, baseUrl:'../javascript/dojo'"></script>
	<script type="text/javascript" src="../javascript/include.aspx?classes=arasInnovatorCUI"></script>
	<script type="text/javascript">
		//these global variable is required because may be overwritten on pages that include this file.
		var documentTitle = '<%=Client_Config("product_name")%>';//document title

		function _createArasObject()
		{
			var aras = new Aras();
			window.aras = aras;
			window.aras.Browser = new BrowserInfo();

			aras.aboutData = {
				version: {
					revision: '<!-- #include file = "rev.inc"-->',
					build: '<!-- #include file = "build.inc"-->'
				},
				msBuildNumber: '<!-- #include file = "MSBuildNumber.inc"-->',
				copyright: '<!-- #include file = "copyright.inc"-->'
			};

			aras.setCommonPropertyValue('clientRevision', "<%=ClientHttpApplication.ClientVersion%>");
		}

		function login() {
			showSpinner();
			return Promise.all([
				deepLinking.beforeInitializeLoginForm().then(function(deepLinkingResult) {
					if (deepLinkingResult.startItemHandled === true) {
						removeTimeZoneSelectFrame();
					}
					return deepLinkingResult;
				}),
				aras.OAuthServerDiscovery.discover(aras.getServerBaseURL())
			]).then(function(result) {
				const deepLinkingResult = result[0];
				if (deepLinkingResult.startItemHandled === true) {
					return;
				}
				const oauthServerConfiguration = result[1];
				const clientBaseUrl = aras.getInnovatorUrl() + 'Client/';
				return aras.OAuthClient.init('<%=oauthClientId%>', clientBaseUrl, oauthServerConfiguration)
					.then(function()  {
						const params = new URLSearchParams(location.search.substr(1, location.search.length));
						const options = {
							prompt: params.get('prompt'),
							database: params.get('db'),
							authentication_type: params.get('auth'),
							state: {}
						};
						// Remove prompt to do not have it in returnUrl to avoid recursive
						// login in case when prompt equals to 'login' or 'select-account'
						params.delete('prompt');
						let search = params.toString();
						if (search) {
							search = '?' + search;
						}
						const returnUrl = [
							location.origin, location.pathname, search, location.hash
						].join('');
						options.state.returnUrl = returnUrl;
						return aras.OAuthClient.login(options);
					}).then(function (result) {
						if (result.status == 'logged-in') {
							return aras.login()
								.then(function() {
									hideSpinner();
									onSuccessfulLogin();
								});
						}
					});
				}).catch(function(err) {
					hideSpinner();
					return aras.AlertError(err).then(function() {
						if (aras.OAuthClient.isLogged()) {
							return aras.OAuthClient.logout();
						}
					});
				});
		}

		function showTimeZoneSelectFrame(tzName, winTzNames, onSelect) {
			const timeZoneFrame = frames.tz;
			timeZoneFrame.initialize(tzName, winTzNames, onSelect);
			timeZoneFrame.show();
		}

		function hideTimeZoneSelectFrame() {
			const timeZoneFrame = frames.tz;
			timeZoneFrame.hide();
		}

		function removeTimeZoneSelectFrame() {
			const timeZoneFrame = frames.tz;
			if (timeZoneFrame) {
				document.body.removeChild(timeZoneFrame.frameElement);
			}
		}

		function hideSpinner() {
			document.querySelector('#dimmer_spinner').classList.add('disabled-spinner');
			document.querySelector('#dimmer_spinner').classList.remove('behind-error-dialogs');
		}

		function showSpinner() {
			document.querySelector('#dimmer_spinner').classList.add('behind-error-dialogs');
			document.querySelector('#dimmer_spinner').classList.remove('disabled-spinner');
		}

		function onSuccessfulLogin() {
			removeTimeZoneSelectFrame();

			ArasModules.intl.locale = aras.getSessionContextLocale();
			resetSelfServiceReportingSession();
			initalizeArasSetupQuery();
			document.title = documentTitle;
			deepLinking.onSuccessfulLogin();
			aras.setCommonPropertyValue("systemInfo_CurrentLocale", aras.IomInnovator.getI18NSessionContext().getLocale());
			window.Cache = null;
			const navPanel = document.getElementById('navigationPanel');
			navPanel.render();
			window.cui = new CUI_ConfigurableUI();
			Promise.all([
				prepareMainWindowInfo(),
			])
				.then(function() {
					window.main = window;
					window.initialize();
					window.mainLayout = new MainWindowCuiLayout(document, 'MainWindowView', {item_classification: '%all_grouped_by_classification%'});
					return window.mainLayout.init();
				})
				.then(function() {
					window.aras.browserHelper.toggleSpinner(document, false);
				})
				.catch(function (e) {
					console.error('An error has occurred during login', e);
					window.aras.browserHelper.toggleSpinner(document, false);
				})
				.then(function() {
					return Promise.all(['List', 'Form', 'ContentTypeByDocumentItemType'].map(function(name) {
						return aras.MetadataCache.RefreshMetadata(name);
					}));
				})
				.then(function() {
					arasMainWindowInfo.setProvider(new SyncMainWindowInfoProvider());
				});
		}

		function initalizeArasSetupQuery() {
			aras.setup_query = {};

			<% Dim key As String
		For Each key In Request.QueryString.AllKeys
			Response.Write("aras.setup_query[""" +
			Server.HtmlEncode(key) + """]=""" +
			Server.HtmlEncode(Request.QueryString.Item(key)) +
			""";" + vbCrLf)
		Next %>
		}

		function prepareMainWindowInfo() {
			var asyncProvider = new AsyncCachedMainWindowInfoProvider();

			return asyncProvider.fetch()
				.then(function(provider) {
					window.arasMainWindowInfo = new ArasMainWindowInfo(aras);
					window.arasMainWindowInfo.setProvider(provider);

					// load metadataDates
					var metadataDates = arasMainWindowInfo.GetAllMetadataDates;
					aras.MetadataCache.UpdatePreloadDates(metadataDates);

					aras.buildIdentityList(window.arasMainWindowInfo.getIdentityListResult);
					aras.setCommonPropertyValue('IsSSVCLicenseOk', arasMainWindowInfo.IsSSVCLicenseOk);
					aras.setCommonPropertyValue('MessageCheckInterval', arasMainWindowInfo.MessageCheckInterval);
					aras.setCommonPropertyValue('SearchCountModeException', arasMainWindowInfo.Core_SearchCountModeException);
					aras.setCommonPropertyValue('SearchCountMode', arasMainWindowInfo.SearchCountMode);
				});
		}



		function resetSelfServiceReportingSession() {
			var xmlHttp = new XMLHttpRequest();
			// SSR is placed near Inn. Server
			var url = aras.getServerBaseURL() + "../SelfServiceReporting/SessionAbandon.aspx?_random=" + Math.random();
			xmlHttp.open("POST", url);
			xmlHttp.send();
		}

		function initArasObject() {
			_createArasObject();
			var aras = window.aras;

			aras.browserHelper = new BrowserHelper(aras, window);

			//++++++++ Initialize aras components
			/// this function is called from scrits/nash.aspx and scripts/login.aspx
			aras.IomFactory = new Aras.IOM.IomFactory();
			aras.IomInnovator = aras.newIOMInnovator();
			aras.InnovatorUser = new Aras.IOM.InnovatorUser();
			aras.GUIDManager = new GUIDManager(aras);
			const cache = aras.IomFactory.CreateItemCache();
			aras.MetadataCache = new MetadataCache(aras, cache);// aras.IomFactory.CreateItemCache();
			aras.MetadataCacheJson = new MetadataCache(aras, cache, true);
			aras.setCommonPropertyValue("mainWindow", window);
			aras.browserHelper.initComponents();
			aras.vault = controlWrapperFactory.createVaultWrapper(aras);
			aras.utils = controlWrapperFactory.createUtilsWrapper(aras);
			aras.XmlHttpRequestManager = new XmlHttpRequestManager(aras.utils);

			aras.arasService = aras.newObject();
			aras.arasService.actionNamespace = "<%=actionNamespace%>";
			aras.arasService.serviceName = "<%=serviceName%>";
			aras.arasService.serviceUrl = "<%=serviceUrl%>";
			CultureInfo.initClass();
		}

		window.addEventListener('DOMContentLoaded', function() {
			window.__staticVariablesStorage["XmlHttpRequestManager"] = new XmlHttpRequestManager();
			makeItCaseInsensetive(
				Object,
				Array,
				Aras.IOME.CacheableContainer,
				Aras.IOME.ItemCache,
				Aras.IOM.I18NSessionContext,
				Aras.IOM.Innovator,
				Aras.IOM.IomFactory,
				Aras.IOM.Item,
				Aras.IOM.InnovatorUser,
				Aras.IOM.InnovatorServerConnector);
		});

		initArasObject();
	</script>
